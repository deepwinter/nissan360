package com.amci.nissan360.services;

import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgressListener;

public interface UpdateMediaListener extends RequestListener<String>, RequestProgressListener{}

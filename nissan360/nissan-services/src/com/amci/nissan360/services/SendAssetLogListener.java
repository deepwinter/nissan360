package com.amci.nissan360.services;

public interface SendAssetLogListener {

	public void assetLogCached();

	public void assetLogSend();

}

package com.amci.nissan360.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import net.osmand.plus.OsmandApplication;
import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;

import org.codehaus.jackson.JsonProcessingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONException;

import android.app.Notification;
import android.app.Service;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.activities.admin.LoadingDoneListener;
import com.amci.nissan360.activities.drive.DriveReminder;
import com.amci.nissan360.activities.drive.ReservationAddedMessage;
import com.amci.nissan360.api.AgendaItem;
import com.amci.nissan360.api.AllUsersRequest;
import com.amci.nissan360.api.AllUsersResponse;
import com.amci.nissan360.api.Asset;
import com.amci.nissan360.api.AssetLog;
import com.amci.nissan360.api.AssetLogPost;
import com.amci.nissan360.api.AssetsRequest;
import com.amci.nissan360.api.AssetsResponse;
import com.amci.nissan360.api.ContentStatus;
import com.amci.nissan360.api.Device;
import com.amci.nissan360.api.DevicePut;
import com.amci.nissan360.api.DownloadFileRequest;
import com.amci.nissan360.api.EmptyResponse;
import com.amci.nissan360.api.LoginUser;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.amci.nissan360.api.Vehicle;
import com.amci.nissan360.api.VehiclesRequest;
import com.amci.nissan360.api.VehiclesResponse;
import com.amci.nissan360.api.configuration.AgendaDay;
import com.amci.nissan360.api.configuration.Colors;
import com.amci.nissan360.api.configuration.ConfigurationRequest;
import com.amci.nissan360.api.configuration.ConfigurationResponse;
import com.amci.nissan360.api.configuration.Contact;
import com.amci.nissan360.api.configuration.ContactList;
import com.amci.nissan360.api.configuration.Coordinates;
import com.amci.nissan360.api.configuration.Help;
import com.amci.nissan360.api.configuration.PointOfInterest;
import com.amci.nissan360.api.configuration.SmartPoster;
import com.amci.nissan360.api.configuration.TrackDescriptions;
import com.amci.nissan360.api.reservation.ReservationCancelRequest;
import com.amci.nissan360.api.reservation.ReservationCheckoutRequest;
import com.amci.nissan360.api.reservation.ReservationIdentity;
import com.amci.nissan360.api.reservation.ReservationInfo;
import com.amci.nissan360.api.reservation.ReservationOptions;
import com.amci.nissan360.api.reservation.ReservationTracking;
import com.amci.nissan360.api.reservation.ReservationTrackingPost;
import com.amci.nissan360.api.reservation.ReservationsRequest;
import com.amci.nissan360.api.reservation.ReservationsResponse;
import com.amci.nissan360.api.reservation.StreetDriveQueueRequest;
import com.amci.nissan360.api.reservation.StreetDriveQueueResponse;
import com.amci.nissan360.database.AgendaContract;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.AgendaProvider;
import com.amci.nissan360.provider.AssetsLogProvider;
import com.amci.nissan360.provider.AssetsProvider;
import com.amci.nissan360.provider.PointsOfInterestProvider;
import com.amci.nissan360.provider.ReservationsProvider;
import com.amci.nissan360.provider.ReservationsQueueProvider;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.state.ApplicationState;
import com.amci.nissan360.state.ReservationState;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;


public class NissanApiService extends Service implements LocationListener {

	public static final int SERVICE_ID = 1001;

	private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);

	String mediaHashDownloadInProgress = "";

	Timer beaconTimer;
	int beaconDelayPeriod = 1000 * 15 * 1; // Every 15 seconds.

	public class LocalBinder extends Binder {
		public NissanApiService getService() {
			return NissanApiService.this;
		}
	}

	private final IBinder mBinder = new LocalBinder();


	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	// flag for GPS status
	static boolean canGetLocation = false;

	static Location location; // location
	static double latitude; // latitude
	static double longitude; // longitude

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;


	// Temporary State
	ReservationIdentity pendingCompletedReservation = null;


	@Override
	public void onCreate() {
		super.onCreate();
		spiceManager.start(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int rval = super.onStartCommand(intent, flags, startId);
		getLocation();
		return rval;
	}



	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);

		// Put the service into the foreground so it doesn't sleep
		Notification n = new NotificationCompat.Builder(getApplicationContext())
		.setContentTitle("Nissan 360 Background Service")
		.setContentText("Nissan360 is Running in the Background")
		.build();
		startForeground(SERVICE_ID, n);

		// Start the timer to phone home
		class BeaconTimerTask extends TimerTask {

			@Override
			public void run() {

				sendBeacon();

			}

		}

		beaconTimer = new Timer();
		beaconTimer.schedule(new BeaconTimerTask(), 0, beaconDelayPeriod);

	}

	public void sendBeacon(){

		Device d = new Device();
		LoginUser l = Nissan360.getLoggedInUser(true);
		if(l != null){
			d.AttendeeId = l.LoginId;
		} else {
			d.AttendeeId = null;
		}
		d.Phone = Nissan360.application.getPhoneNumber();
		d.deviceId = Nissan360.application.getDeviceId();
		d.LocationLat = getLatitude();
		d.LocationLong = getLongitude();
		d.PushToken = Nissan360.application.getPushToken();
		
		class DevicePutListener implements RequestListener< ContentStatus > {
			
			@Override
			public void onRequestFailure(SpiceException arg0) {
				Toast.makeText(getApplicationContext(), "Error sending beacon to server", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onRequestSuccess(ContentStatus contentStatus) {
				// The server is reachable, and the API is up

				// Look for queued asset log items and send them to the server
				sendAllAssetLogItems();

				// Upload any pending reservations
				sendPendingReservationRequests();
				
				// Send pending reservation completed if exists
				if(pendingCompletedReservation != null){
					sendDriveCompleted(pendingCompletedReservation);
				}


				// Check for config refresh
				SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				String lastHash = settings.getString(Nissan360.CONTENT_HASH_PREFERENCE, null);
				if(lastHash == null || !contentStatus.content_status_hash.equals(lastHash)){
					reloadAllConfigurationData(null);
					settings.edit().putString(Nissan360.CONTENT_HASH_PREFERENCE, contentStatus.content_status_hash).commit();
				}

				int currentAssetsVersion = settings.getInt(Nissan360.NISSAN360_APK_ASSETS_VERSION_PREFERENCE, 0);
				boolean initialMediaLoaded = settings.getBoolean(Nissan360.NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE, false);
				if(currentAssetsVersion == Nissan360.NISSAN360_APK_ASSETS_VERSION 
						&& initialMediaLoaded == true){

					// update media if there are changes
					String lastMediaHash =  settings.getString(Nissan360.MEDIA_DOWNLOAD_HASH_PREFERENCE, null);
					if(lastMediaHash == null || ! contentStatus.media_hash.equals(lastMediaHash)){
						String mediaUrl = contentStatus.media_zip;
						if(mediaUrl != null && mediaUrl != ""){
							updateMediaAssets(mediaUrl, contentStatus.media_hash);
						}
					}
				}

			}

		}

		spiceManager.execute(new DevicePut(d), null, DurationInMillis.NEVER, new DevicePutListener());	
		
		
		if(ApplicationState.isInDriveMode(this)){
			ReservationTracking rTracking = new ReservationTracking();
			rTracking.AttendeeId = d.AttendeeId;
			rTracking.ReservationId = ReservationState.getReservationId(this);
			rTracking.VehicleId = ReservationState.getVin(this);
			rTracking.Status = ReservationTracking.RESERVATION_TRACKING_STATE_ON_COURSE;
			rTracking.Details = String.valueOf( ((OsmandApplication) getApplication()  ).getRoutingHelper().getLeftTime() );

			class ReservationTrackingListener implements RequestListener< Boolean > {
				
				@Override
				public void onRequestFailure(SpiceException arg0) {
					Toast.makeText(getApplicationContext(), "Error sending reservation tracking", Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onRequestSuccess(Boolean success) {
					// The server is reachable, and the API is up
				}

			}
			spiceManager.execute(new ReservationTrackingPost(rTracking), null, DurationInMillis.NEVER, new ReservationTrackingListener());	

			
		}
	}



	public void sendPendingReservationRequests() {
		
		if(Nissan360.getLoggedInUser(true) != null){

			Cursor pendingReservations = getContentResolver().query(ReservationsQueueProvider.CONTENT_URI, ReservationsQueueProvider.getDefaultProjection(), "Uploaded = 'NO'", null, null);

			for(int i = 0; i<pendingReservations.getCount(); i++){
				pendingReservations.moveToPosition(i);
				ReservationOptions reservationOptions = ReservationOptions.loadFromCursor(pendingReservations);
				sendPendingReservationRequest(reservationOptions);
			}
		}
	}

	public void sendPendingReservationRequest(final ReservationOptions reservationOptions) {
		class PendingReservationRequestListener implements RequestListener< StreetDriveQueueResponse > {

			@Override
			public void onRequestFailure(SpiceException exception) {

				Log.d("API SERVICE", "Failed to send registration");
				// Just do nothing and keep trying.
				// What if it's a client error issue though?  Guess it's just going to get stuck?

			}

			@Override
			public void onRequestSuccess(
					StreetDriveQueueResponse response) {

				ContentValues cv = new ContentValues();
				cv.put(AssetsLogContract.UPLOADED_FIELD, "YES");
				getContentResolver().update( Uri.withAppendedPath( ReservationsQueueProvider.CONTENT_URI, reservationOptions.ProviderId), cv, null, null);

				// Launch a new activity
				// http://stackoverflow.com/questions/3606596/android-start-activity-from-service
				Intent dialogIntent = new Intent(getBaseContext(), ReservationAddedMessage.class);
				dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplication().startActivity(dialogIntent);
			}

		}

		spiceManager.execute( new StreetDriveQueueRequest(reservationOptions), null, DurationInMillis.NEVER, new PendingReservationRequestListener() );

	}

	public void sendAssetLogItem(final AssetLog assetLog, final SendAssetLogListener listener) {
		class AssetLogPostListener implements RequestListener< EmptyResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				// Not a big deal, we just wait until things are working and try again
				e.printStackTrace();
				if(listener != null){
					listener.assetLogCached();
				}
			}

			@Override
			public void onRequestSuccess(EmptyResponse arg0) {
				// Update the asset in the database
				ContentValues cv = new ContentValues();
				cv.put(AssetsLogContract.UPLOADED_FIELD, "YES");
				getContentResolver().update( Uri.withAppendedPath( AssetsLogProvider.CONTENT_URI_BY_ROW_ID, assetLog.ProviderId), cv, null, null);
				if(listener != null){

					listener.assetLogSend();
				}
			}

		}

		spiceManager.execute(new AssetLogPost(assetLog), null, DurationInMillis.NEVER, new AssetLogPostListener());	
	}

	@Override
	public void onDestroy() {
		spiceManager.shouldStop();
		stopUsingGPS();
		beaconTimer.cancel();
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	public void reloadAllConfigurationData(final LoadingDoneListener loadingDoneListener) {


		class VehiclesRequestListener implements RequestListener< VehiclesResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				//showProgress(false);
				if(e instanceof NoNetworkException){
					Toast.makeText(getApplicationContext(), "Network connection is unavailable", Toast.LENGTH_LONG).show();
					spiceManager.cancelAllRequests();
				} else {
					Toast.makeText(getApplicationContext(), "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}				
			}

			@Override
			public void onRequestSuccess(VehiclesResponse Vehicles) {

				if(Vehicles == null){
					return;
				}

				// Remove all cars from the database ?
				getContentResolver().delete(
						VehiclesProvider.CONTENT_URI,
						null,
						null
						);

				Iterator<Vehicle> it = Vehicles.iterator();
				while (it.hasNext()) {
					Vehicle vehicle = it.next();
					Uri newUri = getContentResolver().insert(
							VehiclesProvider.CONTENT_URI,   // the user dictionary content URI
							vehicle.getContentValues()      // the values to insert
							);
					//Toast.makeText(getApplicationContext(), "Added Vehicle "+ vehicle.Model, Toast.LENGTH_SHORT).show();

				}
				Toast.makeText(getApplicationContext(), "Loaded Vehicles", Toast.LENGTH_SHORT).show();

			}

		}

		spiceManager.execute( new VehiclesRequest(), null, DurationInMillis.NEVER, new VehiclesRequestListener() );



		class AssetsRequestListener implements RequestListener< AssetsResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				if(e instanceof NoNetworkException){
					Toast.makeText(getApplicationContext(), "Network connection is unavailable", Toast.LENGTH_LONG).show();
					spiceManager.cancelAllRequests();
				} else {
					Toast.makeText(getApplicationContext(), "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}				
			}

			@Override
			public void onRequestSuccess(AssetsResponse assets) {

				if(assets == null){
					return;
				}

				// Remove all cars from the database ?
				getContentResolver().delete(
						AssetsProvider.ASSETS_CONTENT_URI,
						null,
						null
						);

				Iterator<Asset> it = assets.iterator();
				while (it.hasNext()) {
					Asset asset = it.next();
					getContentResolver().insert(
							AssetsProvider.ASSETS_CONTENT_URI,   // the user dictionary content URI
							asset.getContentValues()      // the values to insert
							);
					//Toast.makeText(getApplicationContext(), "Added Asset "+ asset.Name, Toast.LENGTH_SHORT).show();

				}
				//Toast.makeText(getApplicationContext(), "Done with Assets", Toast.LENGTH_SHORT).show();

			}

		}

		spiceManager.execute( new AssetsRequest(), null, DurationInMillis.NEVER, new AssetsRequestListener() );



		class ConfigurationRequestListener implements RequestListener< ConfigurationResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				if(e instanceof NoNetworkException){
					Toast.makeText(getApplicationContext(), "Network connection is unavailable", Toast.LENGTH_LONG).show();
					spiceManager.cancelAllRequests();
				} else {
					Toast.makeText(getApplicationContext(), "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}			
			}

			@Override
			public void onRequestSuccess(ConfigurationResponse configuration) {

				if(configuration == null)
					return;

				deleteAllContacts();
				if(configuration.contact_list != null && configuration.contact_list.contacts != null){
					Iterator<Contact> i = configuration.contact_list.contacts.iterator();
					while(i.hasNext()){
						Contact c = i.next();
						insertContact(c.name, c.phone_number );
					}
				}

				getContentResolver().delete(
						PointsOfInterestProvider.CONTENT_URI,
						null,
						null
						);

				if(configuration.points_of_interest != null){
					Iterator<PointOfInterest> poii = configuration.points_of_interest.iterator();
					while(poii.hasNext()){
						PointOfInterest poi = poii.next();

						Uri newUri = getContentResolver().insert(
								PointsOfInterestProvider.CONTENT_URI,   // the user dictionary content URI
								poi.getContentValues()      // the values to insert
								);
						//Toast.makeText(getApplicationContext(), "Added POI "+ poi.title, Toast.LENGTH_SHORT).show();

					}
					//Toast.makeText(getApplicationContext(), "Done with POIs ", Toast.LENGTH_SHORT).show();

				}

				getContentResolver().delete(
						AssetsProvider.SMART_POSTERS_CONTENT_URI,
						null,
						null
						);

				if(configuration.smart_posters != null){
					Iterator<SmartPoster> spi = configuration.smart_posters.iterator();
					while(spi.hasNext()){
						SmartPoster s = spi.next();
						Uri newUri = getContentResolver().insert(
								AssetsProvider.SMART_POSTERS_CONTENT_URI,
								s.getContentValues()
								);
						//Toast.makeText(getApplicationContext(), "Added Smart Poster " + s.name, Toast.LENGTH_SHORT).show();
					}
					//Toast.makeText(getApplicationContext(), "Done with Smart Posters ", Toast.LENGTH_SHORT).show();
				}

				// Agenda


				getContentResolver().delete(AgendaProvider.CONTENT_URI, null, null);

				if(configuration.agenda != null){
					Iterator<AgendaDay> adi = configuration.agenda.iterator();
					while(adi.hasNext()){
						AgendaDay day = adi.next();
						Iterator<AgendaItem> ai = day.Events.iterator();
						while(ai.hasNext()){
							AgendaItem item = ai.next();
							ContentValues cv = item.getContentValues();
							cv.put(AgendaContract.DAY_OF_EVENT_FIELD, day.DayOfEvent);
							cv.put(AgendaContract.DATE_FIELD, day.Date);
							getContentResolver().insert(
									AgendaProvider.CONTENT_URI,
									cv);
						}
					}
				}


				// Json Configuration Stores

				JSONDatabase configDatabase;
				try {
					configDatabase = JSONDatabase.GetDatabase(getApplicationContext(), "ConfigurationJSONDatabaseConfiguration");
					configDatabase.deleteAll();

					if(configuration.track_descriptions != null){
						configDatabase.insert(configuration.track_descriptions, TrackDescriptions.class);	
					}

					/*
					if(configuration.coordinates != null){
						configDatabase.insert(configuration.coordinates, Coordinates.class);
					}
					*/
					
					/*
					if(configuration.help != null){
						configDatabase.insert(configuration.help, Help.class);
					}
					*/
					
					if(configuration.colors != null){
						configDatabase.insert(configuration.colors, Colors.class);
					}
					
		
					if(configuration.contact_list != null){
						configDatabase.insert(configuration.contact_list, ContactList.class);
					}

				} catch (InitJSONDatabaseExcepiton e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



				Toast.makeText(getApplicationContext(), "Configuration Updated", Toast.LENGTH_SHORT).show();
				if(loadingDoneListener != null){
					loadingDoneListener.done();
				}
				
				sendBeacon();
			}

		}
		spiceManager.execute( new ConfigurationRequest(), null, DurationInMillis.NEVER, new ConfigurationRequestListener() );


	}


	public void deleteAllContacts(){
		ContentResolver contentResolver = getContentResolver();
		Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		while (cursor.moveToNext()) {
			String lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
			Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
			contentResolver.delete(uri, null, null);
		}
	}

	public void insertContact(String name, String phoneNumber){

		ArrayList<ContentProviderOperation> op_list = new ArrayList<ContentProviderOperation>(); 
		op_list.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI) 
				.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null) 
				.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null) 
				//.withValue(RawContacts.AGGREGATION_MODE, RawContacts.AGGREGATION_MODE_DEFAULT) 
				.build()); 

		// first and last names 
		op_list.add(ContentProviderOperation.newInsert(Data.CONTENT_URI) 
				.withValueBackReference(Data.RAW_CONTACT_ID, 0) 
				.withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE) 
				.withValue(StructuredName.GIVEN_NAME, "") 
				.withValue(StructuredName.FAMILY_NAME, name) 
				.build()); 

		op_list.add(ContentProviderOperation.newInsert(Data.CONTENT_URI) 
				.withValueBackReference(Data.RAW_CONTACT_ID, 0) 
				.withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
				.withValue(ContactsContract.CommonDataKinds.Phone.TYPE, Phone.TYPE_ASSISTANT)
				.build());
		op_list.add(ContentProviderOperation.newInsert(Data.CONTENT_URI) 
				.withValueBackReference(Data.RAW_CONTACT_ID, 0)

				.withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Email.DATA, "abc@xyz.com")
				.withValue(ContactsContract.CommonDataKinds.Email.TYPE, Email.TYPE_WORK)
				.build());

		try{ 
			ContentProviderResult[] results = getContentResolver().applyBatch(ContactsContract.AUTHORITY, op_list); 
		}catch(Exception e){ 
			e.printStackTrace(); 
		} 

	}


	public void refreshReservations(final RefreshReservationsListener listener){

		if(Nissan360.getLoggedInUser(true) == null){
			return;
		}
		
		class ReservationsRequestListener implements RequestListener<ReservationsResponse> {

			@Override
			public void onRequestFailure(SpiceException arg0) {
				// Do Nothing
				if(listener != null){
					listener.refreshFailed();
				}
			}

			@Override
			public void onRequestSuccess(ReservationsResponse Reservations) {

				// Remove all reservations from the database
				getContentResolver().delete(
						ReservationsProvider.CONTENT_URI,
						null,
						null
						);

				if(Reservations != null){
					Iterator<ReservationInfo> it = Reservations.iterator();
					boolean error = false;
					while (it.hasNext()) {
						ReservationInfo reservation = it.next();
						Uri newUri = getContentResolver().insert(
								ReservationsProvider.CONTENT_URI,   // the user dictionary content URI
								reservation.getContentValues()      // the values to insert
								);
						if(newUri != null){
							//Toast.makeText(getBaseContext(), "Found Vehicle Reservation "+ reservation.BeginTime, Toast.LENGTH_SHORT).show();
						} else {
							//Toast.makeText(getBaseContext(), "Error Loading Reservation!", Toast.LENGTH_SHORT).show();
							error = true;
						}

					}
					if(!error){
						//Toast.makeText(getBaseContext(), "Loaded all Vehicle Reservations", Toast.LENGTH_SHORT).show();
						if(listener != null){
							listener.refreshSucceeded();
						}
					} else {
						if(listener != null){
							listener.refreshFailed();
						}
					}
				}

			}

		}

		spiceManager.execute( new ReservationsRequest(Nissan360.getLoggedInUser().LoginId ), null, DurationInMillis.NEVER, new ReservationsRequestListener() );				
	}
	
	public void cancelReservation(String reservationId) {
		// Delete res. locally
		getContentResolver().delete(
				Uri.withAppendedPath(ReservationsProvider.CONTENT_URI, reservationId),
				null, null);
		
		// Clear application state
		ReservationState.clearActiveReservation(getBaseContext());
		
		// Send delete the server
		class ReservationDeleteListener implements RequestListener < Boolean> {

			@Override
			public void onRequestFailure(
					SpiceException arg0) {
				// As an enhancement, we could add a record to the queue table indicating a delete here
				// But the then queue table MUST send the queued requests asynchronously
				Toast.makeText(getBaseContext(), "Could not confirm your cancellation at this time", Toast.LENGTH_SHORT).show();
				
			}

			@Override
			public void onRequestSuccess(
					Boolean arg0) {
				// Success!  Nothing to do!
				
			}
			
			
		}
		spiceManager.execute( new ReservationCancelRequest(reservationId), null, DurationInMillis.NEVER, new ReservationDeleteListener() );
	}


	// GPS
	public Location getLocation() {
		try {
			locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
					MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
					MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

			if (locationManager != null) {
				location = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (location != null) {
					latitude = location.getLatitude();
					longitude = location.getLongitude();
				}
			}

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
				canGetLocation = false;
			} else {
				canGetLocation = true;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 * */
	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(NissanApiService.this);
		}
	}

	/**
	 * Function to get latitude
	 * */
	public static double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}

		// return latitude
		return latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public static double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}

		// return longitude
		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 * 
	 * @return boolean
	 * */
	public static boolean canGetLocation() {
		return canGetLocation;
	}

	@Override
	public void onLocationChanged(Location loc) {
		if (loc != null) {
			location = loc;
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		isGPSEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// getting network status
		isNetworkEnabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (!isGPSEnabled && !isNetworkEnabled) {
			// no network provider is enabled
			canGetLocation = false;
		} else {
			canGetLocation = true;
		}
	}

	@Override
	public void onProviderEnabled(String provider) {
		isGPSEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// getting network status
		isNetworkEnabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (!isGPSEnabled && !isNetworkEnabled) {
			// no network provider is enabled
			canGetLocation = false;
		} else {
			canGetLocation = true;
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public void logAssetTap(String type, String assetId, SendAssetLogListener listener, String interaction) {
		AssetLog assetLog = new AssetLog();
		assetLog.AssetId = assetId;
		assetLog.Type = type;
		assetLog.deviceId = Nissan360.application.getDeviceId();
		assetLog.Interaction = interaction;
		LoginUser user = Nissan360.getLoggedInUser();
		if(user != null){
			assetLog.AttendeeId = user.LoginId;
		}
		DateTime dt = new DateTime();
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		assetLog.Timestamp =  fmt.print(dt);

		if(type.equals(AssetsContract.SMART_POSTER_TYPE)){
			// get Smart Poster Name
			String[] projection = {
					AssetsContract.SMART_POSTER_NAME_FIELD
			};
			Cursor c = getContentResolver().query( Uri.withAppendedPath(AssetsProvider.SMART_POSTERS_CONTENT_URI,assetId),
					projection, null, null, null);
			if(c.getCount() > 0){
				c.moveToPosition(0);
				assetLog.MediaName = c.getString(0);
			}

		} else if (type.equals(AssetsContract.VEHICLE_TYPE)) {
			// get Vehicle name and Track
			Cursor c = getContentResolver().query(Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, assetId),
					VehiclesProvider.getDefaultProjection(), null, null, null);
			if(c.getCount() > 0){
				c.moveToPosition(0);
				assetLog.MediaName = c.getString(c.getColumnIndex(VehiclesContract.MODEL_FIELD));

				/*
				 * This isn't desired anymore.
				String track = c.getString(c.getColumnIndex(VehiclesContract.TRACK_FIELD));
				if(track.equals(VehiclesContract.TRACK_TYPE_PERFORMANCE)){
					assetLog.MediaName += " - Performance Track";
				} else if(VehiclesContract.TRACK_TYPES_STREET.contains(track)){
					assetLog.MediaName += " - Street Drive";
				} else if(track.equals(VehiclesContract.TRACK_TYPE_HIGH_PERFORMANCE) ) {
					assetLog.MediaName += " - High Performance Drive";
				} else if(track.equals(VehiclesContract.TRACK_TYPE_LCV_DRIVE) ) {
					assetLog.MediaName += " - LCV Drive";
				} else if(track.equals(VehiclesContract.TRACK_TYPE_ADVENTURE) ) {
					assetLog.MediaName += " - Adventure Drive";
				} else if(track.equals(VehiclesContract.TRACK_TYPE_ALEAF_DRIVE) ) {
					assetLog.MediaName += " - ALeaf Drive";
				} else if(track.equals(VehiclesContract.TRACK_TYPE_WORLD_DRIVE) ) {
					assetLog.MediaName += " - World Drive";
				} 
				*/


			}
		}

		Uri uri = getContentResolver().insert(AssetsLogProvider.CONTENT_URI, assetLog.getContentValues());
		assetLog.ProviderId = uri.getLastPathSegment();

		// attempt if we have network
		sendAssetLogItem(assetLog, listener);
	}

	public void updateMediaAssets(){
		sendBeacon();
	}

	public  void updateMediaAssets(String url, String new_hash) {
		updateMediaAssets(url, new_hash, null);
	}

	public  void updateMediaAssets(String url, final String newHash, final UpdateMediaListener updateMediaListener) {
		Toast.makeText(getApplicationContext(), "Patching Media", Toast.LENGTH_SHORT).show();

		if(mediaHashDownloadInProgress.equals(newHash)){
			return;
		}
		class DownloadMediaAssetsRequestListener implements RequestListener<String>, RequestProgressListener {

			@Override
			public void onRequestFailure(SpiceException e) {
				// Do Nothing
				e.printStackTrace();
				mediaHashDownloadInProgress = "";
			}

			@Override
			public void onRequestSuccess(String tempFile) {
				boolean rval = Utilities.unpackZip(tempFile, getApplicationContext().getFilesDir().getPath() + "/nissan360/" );
				if(rval){
					SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					settings.edit().putString(Nissan360.MEDIA_DOWNLOAD_HASH_PREFERENCE, newHash).commit();
					DateTime dt = new DateTime();
					DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
					String time = fmt.print(dt);
					settings.edit().putString(Nissan360.MEDIA_DOWNLOAD_HASH_TIMESTAMP, time).commit();

					Toast.makeText(getApplicationContext(), "Done Patching Media", Toast.LENGTH_SHORT).show();
				}
				mediaHashDownloadInProgress = "";
			}

			@Override
			public void onRequestProgressUpdate(RequestProgress progress) {
				if(updateMediaListener != null){

				}
			}

		}

		spiceManager.execute( new DownloadFileRequest(url), null, DurationInMillis.NEVER, new DownloadMediaAssetsRequestListener() );				


	}



	public void sendAllAssetLogItems() {
		Cursor assetLogs = getContentResolver().query(AssetsLogProvider.CONTENT_URI, AssetsLogProvider.getDefaultProjection(), "Uploaded = 'NO'", null, null);
		for(int i = 0; i<assetLogs.getCount(); i++){
			assetLogs.moveToPosition(i);
			AssetLog assetLog = new AssetLog();
			assetLog.ProviderId = assetLogs.getString(assetLogs.getColumnIndex(AssetsLogContract.ID_FIELD));
			assetLog.AssetId = assetLogs.getString(assetLogs.getColumnIndex(AssetsLogContract.ASSET_ID_FIELD));
			assetLog.AttendeeId = assetLogs.getString(assetLogs.getColumnIndex(AssetsLogContract.ATTENDEE_ID_FIELD));
			assetLog.deviceId = assetLogs.getString(assetLogs.getColumnIndex(AssetsLogContract.DEVICE_ID_FIELD));
			assetLog.Timestamp = assetLogs.getString(assetLogs.getColumnIndex(AssetsLogContract.TIMESTAMP_FIELD));
			NissanApiService.this.sendAssetLogItem(assetLog, null);

		}
	}

	public void sendRequest(
			DownloadFileRequest downloadFileRequest,
			RequestListener<String> downloadMediaAssetsRequestListener) {
		// TODO Auto-generated method stub
		spiceManager.execute( downloadFileRequest, null, DurationInMillis.NEVER, downloadMediaAssetsRequestListener );

		
	}

	public void sendDriveCompleted(final ReservationIdentity reservation) {
		
		class CheckoutRequestListener implements RequestListener<EmptyResponse> {

			@Override
			public void onRequestFailure(SpiceException e) {
				// Do Queue this for later!
				pendingCompletedReservation = reservation;
			}

			@Override
			public void onRequestSuccess(EmptyResponse empty) {
				// Good to go
				// Unqueue the cancellation
				pendingCompletedReservation = null;
				
			}


		}

		spiceManager.execute( new ReservationCheckoutRequest(reservation), null, DurationInMillis.NEVER, new CheckoutRequestListener() );
		
	}

	public void execute(AllUsersRequest allUsersRequest,
			RequestListener< AllUsersResponse > allUsersRequestListener) {
		
		spiceManager.execute(  allUsersRequest, null, DurationInMillis.NEVER, allUsersRequestListener );

		
	}

	

}
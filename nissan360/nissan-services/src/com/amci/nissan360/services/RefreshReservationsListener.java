package com.amci.nissan360.services;

public interface RefreshReservationsListener {

	void refreshSucceeded();

	void refreshFailed();

}

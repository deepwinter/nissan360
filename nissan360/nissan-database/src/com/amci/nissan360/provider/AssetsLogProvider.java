package com.amci.nissan360.provider;

import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.database.Nissan360Database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class AssetsLogProvider extends ContentProvider {

	private static final String AUTHORITY = "com.amci.nissan360.provider.AssetsLogProvider";
	public static final int ASSET_LOGS = 100;
	public static final int ASSET_LOG_ITEM_BY_ROW_ID = 110;
	public static final int ASSET_LOG_ITEM_BY_ASSET_ID = 120;

	
	private static final String ASSETS_BASE_PATH = "nfc";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + ASSETS_BASE_PATH);
	public static final Uri CONTENT_URI_BY_ROW_ID = Uri.parse("content://" + AUTHORITY
			+ "/" + ASSETS_BASE_PATH + "/row");
	public static final Uri CONTENT_URI_BY_ASSET_ID = Uri.parse("content://" + AUTHORITY
			+ "/" + ASSETS_BASE_PATH + "/search");
	
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/assetsLog";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/assetsLog";



	private Nissan360Database mDB;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH, ASSET_LOGS);
		sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/search/*", ASSET_LOG_ITEM_BY_ASSET_ID);
		sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/row/#", ASSET_LOG_ITEM_BY_ROW_ID);

	}


	/*
	 * The asset log is very important
	 * hence, we don't even support deleting from it.
	 * 
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

		// Validate the requested Uri
		if (sURIMatcher.match(uri) != ASSET_LOGS) {
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		SQLiteDatabase db = mDB.getWritableDatabase();
		long rowID = db.insert(Nissan360Database.TABLE_ASSETS_LOG, null, values);

		if (rowID > 0) {
			// Return a URI to the newly created row on success
			result = ContentUris.withAppendedId(CONTENT_URI, rowID);

			// Notify the Context's ContentResolver of the change
			getContext().getContentResolver().notifyChange(result, null);
		}
		return result;		
	}

	@Override
	public boolean onCreate() {
		mDB = new Nissan360Database(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(Nissan360Database.TABLE_ASSETS_LOG);
		int uriType = sURIMatcher.match(uri);
		Cursor cursor;
	
		switch (uriType) {
		case ASSET_LOG_ITEM_BY_ASSET_ID:
			cursor = queryBuilder.query(mDB.getReadableDatabase(),
					projection, AssetsLogContract.ASSET_ID_FIELD + "= '" + uri.getLastPathSegment() + "'",
					null, null, null, null);
			break;
		
		case ASSET_LOGS:
			cursor = queryBuilder.query(mDB.getReadableDatabase(),
					projection, selection, selectionArgs, null, null, sortOrder);
			break;

		default:
			throw new IllegalArgumentException("Unknown URI " + uri.toString());
		}
		
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {

		case ASSET_LOG_ITEM_BY_ROW_ID:
			SQLiteDatabase db = mDB.getWritableDatabase();
			String id = uri.getLastPathSegment();
			int update = db.update(Nissan360Database.TABLE_ASSETS_LOG, values, AssetsContract.ID_FIELD+"="+id, null);
			return update;

		default:
			throw new IllegalArgumentException("Unknown URI " + uri.toString());
		}
	}


	public static String[] getDefaultProjection(){
		String[] projection = {
				AssetsLogContract.ID_FIELD,
				AssetsLogContract.DEVICE_ID_FIELD ,
				AssetsLogContract.ASSET_ID_FIELD ,
				AssetsLogContract.ATTENDEE_ID_FIELD ,
				AssetsLogContract.TIMESTAMP_FIELD,
				AssetsLogContract.TYPE_FIELD,
				AssetsLogContract.UPLOADED_FIELD,
				AssetsLogContract.MEDIA_NAME_FIELD
		};
		return projection;
		
	
	}





}

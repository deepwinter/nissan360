package com.amci.nissan360.provider;

import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.Nissan360Database;
import com.amci.nissan360.database.PointsOfInterestContract;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class PointsOfInterestProvider extends ContentProvider {

	private static final String AUTHORITY = "com.amci.nissan360.provider.PointsOfInterestProvider";
	public static final int POIS = 100;
	public static final int POI = 110;
	public static final int POIS_TYPE = 120;
	public static final int POI_GEO_ASSETS = 130;

	private static final String POIS_BASE_PATH = "pois";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + POIS_BASE_PATH);
	public static final Uri CONTENT_URI_BY_TYPE = Uri.parse("content://" + AUTHORITY
	        + "/" + POIS_BASE_PATH + "/type" );
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
	        + "/poi";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
	        + "/poi";
	
	public static final String POIS_GEO_ASSETS_PATH = POIS_BASE_PATH + "/geo";
	public static final Uri CONTENT_URI_GEO_ASSETS = Uri.parse("content://" + AUTHORITY
			+ "/" + POIS_GEO_ASSETS_PATH);

	private Nissan360Database mDB;
	
	private static final UriMatcher sURIMatcher = new UriMatcher(
	        UriMatcher.NO_MATCH);
	static {
	    sURIMatcher.addURI(AUTHORITY, POIS_BASE_PATH, POIS);
	    sURIMatcher.addURI(AUTHORITY, POIS_BASE_PATH + "/#", POI);
	    sURIMatcher.addURI(AUTHORITY, POIS_BASE_PATH + "/type/*", POIS_TYPE);
	    sURIMatcher.addURI(AUTHORITY, POIS_GEO_ASSETS_PATH, POI_GEO_ASSETS);

	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
	    int uriType = sURIMatcher.match(uri);
	    SQLiteDatabase sqlDB = mDB.getWritableDatabase();
	    int rowsAffected = 0;
	    switch (uriType) {
	    case POIS:
	        rowsAffected = sqlDB.delete(Nissan360Database.TABLE_POIS,
	                selection, selectionArgs);
	        break;
	    default:
	        throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
	    }
	    getContext().getContentResolver().notifyChange(uri, null);
	    return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

		// Validate the requested Uri
		if (sURIMatcher.match(uri) != POIS) {
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		SQLiteDatabase db = mDB.getWritableDatabase();
		long rowID = db.insert(Nissan360Database.TABLE_POIS, null, values);

		if (rowID > 0) {
			// Return a URI to the newly created row on success
			result = ContentUris.withAppendedId(CONTENT_URI, rowID);

			// Notify the Context's ContentResolver of the change
			getContext().getContentResolver().notifyChange(result, null);
		}
		return result;		
	}

	@Override
	public boolean onCreate() {
		mDB = new Nissan360Database(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    queryBuilder.setTables(Nissan360Database.TABLE_POIS);
	    int uriType = sURIMatcher.match(uri);
	    switch (uriType) {
	    case POI:
	        queryBuilder.appendWhere(PointsOfInterestContract.ID_FIELD + "="
	                + uri.getLastPathSegment());
	        break;
	    case POIS:
	        // no filter
	        break;
	    case POIS_TYPE:
	        queryBuilder.appendWhere(PointsOfInterestContract.TYPE_FIELD + "="
	                + "'" + uri.getLastPathSegment() + "'");
	    	break;
	    case POI_GEO_ASSETS:
	        queryBuilder.setTables(
		    		Nissan360Database.TABLE_POIS 
		    		+ " JOIN "
		    		+ Nissan360Database.TABLE_ASSETS 
		    		+ " ON ( "
		    		+ Nissan360Database.TABLE_POIS + "." + PointsOfInterestContract.IDENTIFIER_FIELD
		    		+ " = " 
		    		+ Nissan360Database.TABLE_ASSETS + "." + AssetsContract.ASSET_ID_FIELD
		    		+ " ) ");
	        queryBuilder.appendWhere(Nissan360Database.TABLE_ASSETS+"."+AssetsContract.TYPE_FIELD + "='" + AssetsContract.POINT_OF_INTEREST_TYPE +"'" );
	    	break;
	    default:
	        throw new IllegalArgumentException("Unknown URI " + uri.toString());
	    }
	    Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
	            projection, selection, selectionArgs, null, null, sortOrder);
	    cursor.setNotificationUri(getContext().getContentResolver(), uri);
	    return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static String[] getDefaultProjection() {
		String[] projection = {
				PointsOfInterestContract.IDENTIFIER_FIELD,
				Nissan360Database.TABLE_POIS+"."+PointsOfInterestContract.ID_FIELD,
				Nissan360Database.TABLE_POIS+"."+PointsOfInterestContract.TYPE_FIELD,
				PointsOfInterestContract.SUBTYPE_FIELD,
				PointsOfInterestContract.TITLE_FIELD,
				PointsOfInterestContract.DESCRIPTION_FIELD,
				Nissan360Database.TABLE_POIS+"."+PointsOfInterestContract.LATITUDE_FIELD,
				Nissan360Database.TABLE_POIS+"."+PointsOfInterestContract.LONGITUDE_FIELD
		};
		return projection;
	}

	public static String[] getGeoAssetsProjection() {
		String[] projection = {
				PointsOfInterestContract.IDENTIFIER_FIELD,
				Nissan360Database.TABLE_POIS+"."+PointsOfInterestContract.ID_FIELD,
				Nissan360Database.TABLE_POIS+"."+PointsOfInterestContract.TYPE_FIELD,
				PointsOfInterestContract.SUBTYPE_FIELD,
				PointsOfInterestContract.TITLE_FIELD,
				PointsOfInterestContract.DESCRIPTION_FIELD,
				Nissan360Database.TABLE_ASSETS+"."+AssetsContract.LATITUDE_FIELD,
				Nissan360Database.TABLE_ASSETS+"."+AssetsContract.LONGITUDE_FIELD
		};
		return projection;
	}


	
}

package com.amci.nissan360.provider;

import com.amci.nissan360.database.AgendaContract;
import com.amci.nissan360.database.Nissan360Database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class AgendaProvider extends ContentProvider {

	private static final String AUTHORITY = "com.amci.nissan360.provider.AgendaProvider";
	public static final int AGENDA = 100;
	public static final int AGENDA_ITEM = 110;
	public static final int AGENDA_BY_DAY = 120;
	private static final String AGENDA_BASE_PATH = "agenda";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + AGENDA_BASE_PATH);
	public static final Uri CONTENT_URI_BY_DAY =  Uri.parse("content://" + AUTHORITY
			+ "/" + AGENDA_BASE_PATH + "/day");

	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/agenda";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/agenda";



	private Nissan360Database mDB;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, AGENDA_BASE_PATH, AGENDA);
		sURIMatcher.addURI(AUTHORITY, AGENDA_BASE_PATH + "/#", AGENDA_ITEM);
		sURIMatcher.addURI(AUTHORITY, AGENDA_BASE_PATH + "/day/#", AGENDA_BY_DAY);

	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case AGENDA:
			rowsAffected = sqlDB.delete(Nissan360Database.TABLE_AGENDA,
					selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

		// Validate the requested Uri
		if (sURIMatcher.match(uri) != AGENDA) {
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		SQLiteDatabase db = mDB.getWritableDatabase();
		long rowID = db.insert(Nissan360Database.TABLE_AGENDA, null, values);

		if (rowID > 0) {
			// Return a URI to the newly created row on success
			result = ContentUris.withAppendedId(CONTENT_URI, rowID);

			// Notify the Context's ContentResolver of the change
			getContext().getContentResolver().notifyChange(result, null);
		}
		return result;		
	}

	@Override
	public boolean onCreate() {
		mDB = new Nissan360Database(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(Nissan360Database.TABLE_AGENDA);
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case AGENDA_ITEM:
			queryBuilder.appendWhere(AgendaContract.ID_FIELD + "="
					+ uri.getLastPathSegment());
			break;
		case AGENDA:
			// no filter
			break;
		case AGENDA_BY_DAY:
			queryBuilder.appendWhere(AgendaContract.DAY_OF_EVENT_FIELD + "="
					+ uri.getLastPathSegment());
			break;

		default:
			throw new IllegalArgumentException("Unknown URI " + uri.toString());
		}
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}





}

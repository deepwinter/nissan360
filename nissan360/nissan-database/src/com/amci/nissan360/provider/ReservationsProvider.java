package com.amci.nissan360.provider;

import com.amci.nissan360.database.Nissan360Database;
import com.amci.nissan360.database.ReservationsContract;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class ReservationsProvider extends ContentProvider {

	private static final String AUTHORITY = "com.amci.nissan360.provider.ReservationsProvider";
	public static final int RESERVATIONS = 100;
	public static final int RESERVATION = 110;
	private static final String RESERVATIONS_BASE_PATH = "reservations";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + RESERVATIONS_BASE_PATH);
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/reservation";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/reservation";


	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, RESERVATIONS_BASE_PATH, RESERVATIONS);
		sURIMatcher.addURI(AUTHORITY, RESERVATIONS_BASE_PATH + "/#", RESERVATION);
	}

	private Nissan360Database mDB;

	@Override
	public boolean onCreate() {
		mDB = new Nissan360Database(getContext());
		return true;
	}


	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(Nissan360Database.TABLE_RESERVATIONS);
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case RESERVATION:
			queryBuilder.appendWhere(ReservationsContract.RESERVATION_ID_FIELD + "="
					+ uri.getLastPathSegment());
			break;
		case RESERVATIONS:
			// no filter
			break;
		default:
			throw new IllegalArgumentException("Unknown URI");
		}
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;

	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case RESERVATIONS:
			rowsAffected = sqlDB.delete(Nissan360Database.TABLE_RESERVATIONS,
					selection, selectionArgs);
			break;
		case RESERVATION:
			rowsAffected = sqlDB.delete(Nissan360Database.TABLE_RESERVATIONS,
					ReservationsContract.RESERVATION_ID_FIELD +"='" + uri.getLastPathSegment() + "'", null);

			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

		// Validate the requested Uri
		if (sURIMatcher.match(uri) != RESERVATIONS) {
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		SQLiteDatabase db = mDB.getWritableDatabase();
		long rowID = db.insert(Nissan360Database.TABLE_RESERVATIONS, null, values);

		if (rowID > 0) {
			// Return a URI to the newly created row on success
			result = ContentUris.withAppendedId(CONTENT_URI, rowID);

			// Notify the Context's ContentResolver of the change
			getContext().getContentResolver().notifyChange(result, null);
		}
		return result;		
	}


	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case RESERVATION:

			break;
		case RESERVATIONS:
			break;
		default:
			throw new IllegalArgumentException("Unknown URI");
		}

		return 0;
	}


	public static String[] getDefaultProjection() {
		String[] projection = {
				ReservationsContract.ID_FIELD,
				ReservationsContract.RESERVATION_ID_FIELD,
				ReservationsContract.ATTENDEE_ID_FIELD,
				ReservationsContract.GUEST_FIELD,
				ReservationsContract.PHONE_FIELD,
				ReservationsContract.VEHICLE_FIELD,
				ReservationsContract.VEHICLE_ID_FIELD,
				ReservationsContract.TYPE_FIELD,
				ReservationsContract.TRACK_FIELD,
				ReservationsContract.BEGIN_TIME_FIELD,
				ReservationsContract.END_TIME_FIELD,
				ReservationsContract.QUEUE_POSITION_FIELD,
				ReservationsContract.LATITUDE_FIELD,
				ReservationsContract.LONGITUDE_FIELD
		};
		return projection;
	}



}

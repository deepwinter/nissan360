package com.amci.nissan360.provider;

import java.util.List;

import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.Nissan360Database;
import com.amci.nissan360.database.PointsOfInterestContract;
import com.amci.nissan360.database.VehiclesContract;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class VehiclesProvider extends ContentProvider {

	private static final String AUTHORITY = "com.amci.nissan360.provider.VehiclesProvider";
	public static final int VEHICLES = 100;
	public static final int VEHICLE_VIN = 110;
	public static final int VEHICLES_IN_BRAND = 120;
	public static final int VEHICLES_IN_BRAND_AND_TYPE = 130;
	public static final int VEHICLES_GEO_ASSETS = 140;


	private static final String VEHICLES_BASE_PATH = "vehicles";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + VEHICLES_BASE_PATH);
	public static final Uri CONTENT_URI_BY_BRAND = Uri.parse("content://" + AUTHORITY
			+ "/" + VEHICLES_BASE_PATH + "/brand" );
	public static final Uri CONTENT_URI_BY_VIN = Uri.parse("content://" + AUTHORITY
			+ "/" + VEHICLES_BASE_PATH + "/vin" );
	public static final Uri CONTENT_URI_BY_TYPE_AND_BRAND = Uri.parse("content://" + AUTHORITY
			+ "/" + VEHICLES_BASE_PATH + "/types/brand" );
	public static final Uri CONTENT_URI_GEO_ASSETS = Uri.parse("content://" + AUTHORITY
			+ "/" + VEHICLES_BASE_PATH + "/geo" );

	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/vehicle";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/vehicle";


	private Nissan360Database mDB;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, VEHICLES_BASE_PATH, VEHICLES);
		sURIMatcher.addURI(AUTHORITY, VEHICLES_BASE_PATH + "/vin/*", VEHICLE_VIN);
		sURIMatcher.addURI(AUTHORITY, VEHICLES_BASE_PATH + "/brand/*", VEHICLES_IN_BRAND);
		sURIMatcher.addURI(AUTHORITY, VEHICLES_BASE_PATH + "/types/brand/*/*", VEHICLES_IN_BRAND_AND_TYPE);
		sURIMatcher.addURI(AUTHORITY, VEHICLES_BASE_PATH + "/geo", VEHICLES_GEO_ASSETS);

	}


	public static final String BRAND_PARTNER_TYPE = "Partners";
	public static final String BRAND_DATSUN = "Datsun";
	public static final String BRAND_NISSAN = "Nissan";
	public static final String BRAND_INFINITI = "Infiniti";


	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case VEHICLES:
			rowsAffected = sqlDB.delete(Nissan360Database.TABLE_VEHICLES,
					selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

		// Validate the requested Uri
		if (sURIMatcher.match(uri) != VEHICLES) {
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		SQLiteDatabase db = mDB.getWritableDatabase();
		long rowID = db.insert(Nissan360Database.TABLE_VEHICLES, null, values);

		if (rowID > 0) {
			// Return a URI to the newly created row on success
			result = ContentUris.withAppendedId(CONTENT_URI, rowID);

			// Notify the Context's ContentResolver of the change
			getContext().getContentResolver().notifyChange(result, null);
		}
		return result;		
	}

	@Override
	public boolean onCreate() {
		mDB = new Nissan360Database(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(Nissan360Database.TABLE_VEHICLES);
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case VEHICLE_VIN:
			queryBuilder.appendWhere(VehiclesContract.VIN_FIELD + "= '"
					+ uri.getLastPathSegment() +"'");
			break;
		case VEHICLES:
			// no filter
			break;
		case VEHICLES_IN_BRAND:
			String where;
			if(uri.getLastPathSegment().equals(VehiclesProvider.BRAND_PARTNER_TYPE)){
				where =   VehiclesContract.BRAND_FIELD + "!=" + "'" + VehiclesProvider.BRAND_DATSUN  + "' AND " 
						+ VehiclesContract.BRAND_FIELD + "!=" + "'" + VehiclesProvider.BRAND_INFINITI  + "' AND " 
						+ VehiclesContract.BRAND_FIELD + "!=" + "'" + VehiclesProvider.BRAND_NISSAN + "'";

			} else {
				where = VehiclesContract.BRAND_FIELD + "=" + "'" + uri.getLastPathSegment() + "'";
			}
			queryBuilder.appendWhere(where);
			break;
		case VEHICLES_IN_BRAND_AND_TYPE:
			List<String> segments = uri.getPathSegments();
			String brand = segments.get(3);
			String type = segments.get(4);
			type = type.replace("#SLASH#", "/");
			String where2;
			if(brand.equals(VehiclesProvider.BRAND_PARTNER_TYPE)){
				where2 =  VehiclesContract.BRAND_FIELD + "!=" + "'" + VehiclesProvider.BRAND_DATSUN  + "' AND " 
						+ VehiclesContract.BRAND_FIELD + "!=" + "'" + VehiclesProvider.BRAND_INFINITI  + "' AND " 
						+ VehiclesContract.BRAND_FIELD + "!=" + "'" + VehiclesProvider.BRAND_NISSAN + "' AND "
						+ VehiclesContract.CATEGORY_FIELD + "=" + "'" + type + "'";
			} else {
				where2 =  VehiclesContract.BRAND_FIELD + "=" + "'" + brand + "' AND "
						+ VehiclesContract.CATEGORY_FIELD + "=" + "'" + type + "'";
			}
			queryBuilder.appendWhere(where2);
			break;
		case VEHICLES_GEO_ASSETS:
			queryBuilder.setTables(
					Nissan360Database.TABLE_VEHICLES 
					+ " JOIN "
					+ Nissan360Database.TABLE_ASSETS 
					+ " ON ( "
					+ Nissan360Database.TABLE_VEHICLES + "." + VehiclesContract.VIN_FIELD
					+ " = " 
					+ Nissan360Database.TABLE_ASSETS + "." + AssetsContract.ASSET_ID_FIELD
					+ " ) ");
			queryBuilder.appendWhere(Nissan360Database.TABLE_ASSETS+"."+AssetsContract.TYPE_FIELD + "='" + AssetsContract.VEHICLE_TYPE +"'" );

			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri.toString());
		}
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static String[] getDefaultProjection() {

		String [] projection = {
				VehiclesContract.ID_FIELD,
				VehiclesContract.VIN_FIELD,
				VehiclesContract.NAME_FIELD,
				VehiclesContract.MODEL_FIELD,
				VehiclesContract.BRAND_FIELD,
				VehiclesContract.HERO_IMAGE_FIELD,
				VehiclesContract.DESCRIPTION_FIELD,
				VehiclesContract.TRACK_FIELD,
				VehiclesContract.CATEGORY_FIELD,
				VehiclesContract.SPEC_SHEET_FIELD,
				VehiclesContract.SPEC_SHEET_IMAGE_FIELD,
				VehiclesContract.STORY1_NAME_FIELD,
				VehiclesContract.STORY1_TEXT_FIELD,
				VehiclesContract.STORY1_IMAGE_FIELD,
				VehiclesContract.STORY2_NAME_FIELD,
				VehiclesContract.STORY2_TEXT_FIELD,
				VehiclesContract.STORY2_IMAGE_FIELD,
				VehiclesContract.STORY3_NAME_FIELD,
				VehiclesContract.STORY3_TEXT_FIELD,
				VehiclesContract.STORY3_IMAGE_FIELD,
				VehiclesContract.THUMBNAIL_FIELD,
				VehiclesContract.BUCKET_FIELD,
				VehiclesContract.LATITUDE_FIELD,
				VehiclesContract.LONGITUDE_FIELD
		};
		return projection;
	}

	public static String[] getGeoAssetsProjection() {
		String[] projection = {
				Nissan360Database.TABLE_VEHICLES+"."+VehiclesContract.ID_FIELD,
				VehiclesContract.VIN_FIELD,
				Nissan360Database.TABLE_VEHICLES+"."+VehiclesContract.NAME_FIELD,
				VehiclesContract.MODEL_FIELD,
				VehiclesContract.BRAND_FIELD,
				VehiclesContract.HERO_IMAGE_FIELD,
				VehiclesContract.DESCRIPTION_FIELD,
				VehiclesContract.TRACK_FIELD,
				VehiclesContract.CATEGORY_FIELD,
				VehiclesContract.SPEC_SHEET_FIELD,
				VehiclesContract.SPEC_SHEET_IMAGE_FIELD,
				VehiclesContract.STORY1_NAME_FIELD,
				VehiclesContract.STORY1_TEXT_FIELD,
				VehiclesContract.STORY1_IMAGE_FIELD,
				VehiclesContract.STORY2_NAME_FIELD,
				VehiclesContract.STORY2_TEXT_FIELD,
				VehiclesContract.STORY2_IMAGE_FIELD,
				VehiclesContract.STORY3_NAME_FIELD,
				VehiclesContract.STORY3_TEXT_FIELD,
				VehiclesContract.STORY3_IMAGE_FIELD,
				VehiclesContract.THUMBNAIL_FIELD,
				VehiclesContract.BUCKET_FIELD,
				Nissan360Database.TABLE_ASSETS+"."+AssetsContract.LATITUDE_FIELD,
				Nissan360Database.TABLE_ASSETS+"."+AssetsContract.LONGITUDE_FIELD
		};
		return projection;
	}





}

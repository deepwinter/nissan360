package com.amci.nissan360.provider;

import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.Nissan360Database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class AssetsProvider extends ContentProvider {

	private static final String AUTHORITY = "com.amci.nissan360.provider.AssetsProvider";
	public static final int ASSETS = 100;
	public static final int ASSET = 110;
	public static final int ASSET_BY_ID = 120;
	public static final int SMART_POSTERS = 200;
	public static final int SMART_POSTER = 210;
	private static final String ASSETS_BASE_PATH = "assets";
	private static final String ASSETS_SEGMENT = "assets";
	private static final String ASSET_BY_ID_SEGMENT = "assets_by_id";	
	private static final String SMART_POSTERS_SEGMENT = "smart_posters";
	private static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + ASSETS_BASE_PATH);
	public static final Uri ASSETS_CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + ASSETS_BASE_PATH + "/" + ASSETS_SEGMENT);
	public static final Uri SMART_POSTERS_CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + ASSETS_BASE_PATH + "/" + SMART_POSTERS_SEGMENT);
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
	        + "/asset";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
	        + "/asset";
	

	
	private Nissan360Database mDB;
	
	private static final UriMatcher sURIMatcher = new UriMatcher(
	        UriMatcher.NO_MATCH);
	static {
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + SMART_POSTERS_SEGMENT , SMART_POSTERS);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + SMART_POSTERS_SEGMENT + "/*", SMART_POSTER);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + ASSETS_SEGMENT, ASSETS);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + ASSETS_SEGMENT + "/*", ASSET);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + ASSET_BY_ID_SEGMENT + "/*", ASSET_BY_ID);
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
	    int uriType = sURIMatcher.match(uri);
	    SQLiteDatabase sqlDB = mDB.getWritableDatabase();
	    int rowsAffected = 0;
	    switch (uriType) {
	    case ASSETS:
	        rowsAffected = sqlDB.delete(Nissan360Database.TABLE_ASSETS,
	                selection, selectionArgs);
	        break;
	        
	    case ASSET:
			String id = uri.getLastPathSegment();
			rowsAffected = sqlDB.delete(Nissan360Database.TABLE_ASSETS,
	                "_id = "+id, null);
	        break;
	        
	    case SMART_POSTERS:
	        rowsAffected = sqlDB.delete(Nissan360Database.TABLE_SMART_POSTERS,
	                selection, selectionArgs);
	        break;
	    default:
	        throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
	    }
	    getContext().getContentResolver().notifyChange(uri, null);
	    return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

	    int uriType = sURIMatcher.match(uri);
	    SQLiteDatabase db = mDB.getWritableDatabase();
	    int rowsAffected = 0;
	    long rowID;
	    switch (uriType) {
	    case ASSETS:
	    	rowID = db.insert(Nissan360Database.TABLE_ASSETS, null, values);

			if (rowID > 0) {
				// Return a URI to the newly created row on success
				result = ContentUris.withAppendedId(ASSETS_CONTENT_URI, rowID);

				// Notify the Context's ContentResolver of the change
				getContext().getContentResolver().notifyChange(result, null);
			}
			return result;		
	    case SMART_POSTERS:
	    	rowID = db.insert(Nissan360Database.TABLE_SMART_POSTERS, null, values);

			if (rowID > 0) {
				// Return a URI to the newly created row on success
				result = ContentUris.withAppendedId(SMART_POSTERS_CONTENT_URI, rowID);

				// Notify the Context's ContentResolver of the change
				getContext().getContentResolver().notifyChange(result, null);
			}
			return result;		
	    default:
	        throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
	    }	
	    	

		
	}

	@Override
	public boolean onCreate() {
		mDB = new Nissan360Database(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    int uriType = sURIMatcher.match(uri);
	    Cursor cursor = null;
	    switch (uriType) {
	    case ASSET:
		    queryBuilder.setTables(Nissan360Database.TABLE_ASSETS);
	        queryBuilder.appendWhere(AssetsContract.TAG_ID_FIELD + "= '"
	                + uri.getLastPathSegment() + "'");
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
		    break;
	    case ASSETS:
		    queryBuilder.setTables(Nissan360Database.TABLE_ASSETS);
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
		    break;
	    case ASSET_BY_ID:
		    queryBuilder.setTables(Nissan360Database.TABLE_ASSETS);
		    cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, "asset_id = '" + uri.getLastPathSegment() +"'", null, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
	    	break;
	    case SMART_POSTER:
		    queryBuilder.setTables(Nissan360Database.TABLE_SMART_POSTERS);
	        queryBuilder.appendWhere(AssetsContract.SMART_POSTER_IDENTIFIER + "="
	                + uri.getLastPathSegment());
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	
	        break;
	    case SMART_POSTERS:
		    queryBuilder.setTables(Nissan360Database.TABLE_SMART_POSTERS);
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	        
		    break;
		    
	    default:
	        throw new IllegalArgumentException("Unknown URI " + uri.toString());
	    }
	    return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static String[] getDefaultProjection() {
		String[] projection = {
				AssetsContract.ID_FIELD,
				AssetsContract.NAME_FIELD,
				AssetsContract.TYPE_FIELD,
				AssetsContract.LOCATION_FIELD,
				AssetsContract.TAG_ID_FIELD,
				AssetsContract.ASSET_ID_FIELD,
				AssetsContract.LONGITUDE_FIELD,
				AssetsContract.LATITUDE_FIELD
		};
		return projection;
	}


	
}

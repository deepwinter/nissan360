package com.amci.nissan360.database;

public class PointsOfInterestContract {
	public static final String ID_FIELD = "_id";
	public static final String TYPE_FIELD = "Type";
	public static final String SUBTYPE_FIELD = "Subtype";
	public static final String TITLE_FIELD = "Title";
	public static final String DESCRIPTION_FIELD = "Description";
	public static final String LATITUDE_FIELD = "Latitude";
	public static final String LONGITUDE_FIELD = "Longitude";
	public static final String IDENTIFIER_FIELD = "Identifier";
	
}

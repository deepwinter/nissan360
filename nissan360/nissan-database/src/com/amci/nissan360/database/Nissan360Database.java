package com.amci.nissan360.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Nissan360Database extends SQLiteOpenHelper {

	private static final String DEBUG_TAG = "Nissan360Database";
	private static final int DB_VERSION = 23;
	private static final String DB_NAME = "nissan360";

	public static final String TABLE_VEHICLES = "vehicles";
	public static final String ID = "_id";
	private static final String CREATE_TABLE_VEHICLES = " create table " + TABLE_VEHICLES
			+ " (" + ID + " integer primary key autoincrement, " 
			+ VehiclesContract.VIN_FIELD + " text not null, " 
			+ VehiclesContract.NAME_FIELD + " text not null, " 
			+ VehiclesContract.BRAND_FIELD + " text not null, " 
			+ VehiclesContract.MODEL_FIELD + " text not null, " 
			+ VehiclesContract.HERO_IMAGE_FIELD + " text not null, " 
			+ VehiclesContract.DESCRIPTION_FIELD + " text not null, " 
			+ VehiclesContract.TRACK_FIELD + " text not null, "
			+ VehiclesContract.CATEGORY_FIELD + " text not null, "
			+ VehiclesContract.SPEC_SHEET_FIELD + " text, "
			+ VehiclesContract.SPEC_SHEET_IMAGE_FIELD + " text, "
			+ VehiclesContract.STORY1_NAME_FIELD + " text, "
			+ VehiclesContract.STORY1_TEXT_FIELD + " text, "
			+ VehiclesContract.STORY1_IMAGE_FIELD + " text, "
			+ VehiclesContract.STORY2_NAME_FIELD + " text, "
			+ VehiclesContract.STORY2_TEXT_FIELD + " text, "
			+ VehiclesContract.STORY2_IMAGE_FIELD + " text, "
			+ VehiclesContract.STORY3_NAME_FIELD + " text, "
			+ VehiclesContract.STORY3_TEXT_FIELD + " text, "
			+ VehiclesContract.STORY3_IMAGE_FIELD + " text, "
			+ VehiclesContract.THUMBNAIL_FIELD + " text,"
			+ VehiclesContract.BUCKET_FIELD + " text,"
			+ VehiclesContract.LATITUDE_FIELD + " float, "
			+ VehiclesContract.LONGITUDE_FIELD + " float "
			+ " );";

	public static final String TABLE_RESERVATIONS = "reservations";

	public static final String CREATE_TABLE_RESERVATIONS = " create table " + TABLE_RESERVATIONS
			+ " (" + ID + " integer primary key autoincrement, " 
			+ ReservationsContract.RESERVATION_ID_FIELD  + " text not null, "
			+ ReservationsContract.ATTENDEE_ID_FIELD  + " text not null, "
			+ ReservationsContract.GUEST_FIELD  + " text not null, "
			+ ReservationsContract.PHONE_FIELD  + " text not null, "
			+ ReservationsContract.VEHICLE_FIELD  + " text not null, "
			+ ReservationsContract.VEHICLE_ID_FIELD  + " text not null, "
			+ ReservationsContract.TYPE_FIELD  + " text not null, "
			+ ReservationsContract.TRACK_FIELD  + " text not null, "
			+ ReservationsContract.BEGIN_TIME_FIELD  + " text not null, "
			+ ReservationsContract.END_TIME_FIELD  + " text not null, "
			+ ReservationsContract.QUEUE_POSITION_FIELD  + " int, "
			+ ReservationsContract.LATITUDE_FIELD  + " double, "
			+ ReservationsContract.LONGITUDE_FIELD  + " double "
			+" ); ";

	public static final String TABLE_RESERVATIONS_QUEUE = "reservations_queue";
	public static final String CREATE_TABLE_RESERVATIONS_QUEUE = " create table " + TABLE_RESERVATIONS_QUEUE
			+ " (" + ReservationsQueueContract.ID_FIELD + " integer primary key autoincrement, " 
			+ ReservationsQueueContract.ATTENDEE_ID_FIELD  + " text not null, "
			+ ReservationsQueueContract.VEHICLE_ID_FIELD  + " text not null, "
			+ ReservationsQueueContract.TRACK_FIELD  + " text not null, "
			+ ReservationsQueueContract.TIME_FIELD  + " text not null, "
			+ ReservationsQueueContract.UPLOADED_FIELD  + " text not null "
			+" ); ";

	public static final String TABLE_ASSETS = "assets";
	public static final String CREATE_TABLE_ASSETS = " create table " + TABLE_ASSETS
			+ " (" + ID + " integer primary key autoincrement, " 
			+ AssetsContract.NAME_FIELD  + " text, "
			+ AssetsContract.LOCATION_FIELD + " text, "
			+ AssetsContract.LATITUDE_FIELD + " double, "
			+ AssetsContract.LONGITUDE_FIELD + " double, "
			+ AssetsContract.TAG_ID_FIELD + " text not null, "
			+ AssetsContract.TYPE_FIELD + " text not null, "
			+ AssetsContract.ASSET_ID_FIELD + " text not null);";

	public static final String TABLE_ASSETS_LOG = "assets_log";
	public static final String CREATE_TABLE_ASSETS_LOG = " create table " + TABLE_ASSETS_LOG
			+ " (" + ID + " integer primary key autoincrement, " 
			+ AssetsLogContract.DEVICE_ID_FIELD  + " text not null, "
			+ AssetsLogContract.ASSET_ID_FIELD + " text not null, "
			+ AssetsLogContract.ATTENDEE_ID_FIELD + " text not null, "
			+ AssetsLogContract.TIMESTAMP_FIELD + " text not null, "
			+ AssetsLogContract.TYPE_FIELD + " text, "
			+ AssetsLogContract.INTERACTION_FIELD + " text, "
			+ AssetsLogContract.MEDIA_NAME_FIELD + " text, "
			+ AssetsLogContract.UPLOADED_FIELD + " text );";

	public static final String TABLE_SMART_POSTERS = "smart_posters";
	public static final String CREATE_TABLE_SMART_POSTERS = " create table " + TABLE_SMART_POSTERS
			+ " (" + ID + " integer primary key autoincrement, " 
			+ AssetsContract.SMART_POSTER_NAME_FIELD  + " text, "
			+ AssetsContract.SMART_POSTER_IDENTIFIER  + " text, "
			+ AssetsContract.SMART_POSTER_FILE_FIELD  + " text );";

	public static final String TABLE_AGENDA = "agenda";
	public static final String CREATE_TABLE_AGENDA = " create table " + TABLE_AGENDA
			+ " (" + ID + " integer primary key autoincrement, " 
			+ AgendaContract.NAME_FIELD  + " text not null, "
			+ AgendaContract.DAY_OF_EVENT_FIELD  + " int, "
			+ AgendaContract.DATE_FIELD  + " text not null, "
			+ AgendaContract.TIME_FIELD  + " text not null); ";

	public static final String TABLE_POIS = "pois";
	public static final String CREATE_TABLE_POIS = " create table " + TABLE_POIS
			+ " (" + ID + " integer primary key autoincrement, " 
			+ PointsOfInterestContract.IDENTIFIER_FIELD + " text not null, "
			+ PointsOfInterestContract.TYPE_FIELD  + " text not null, "
			+ PointsOfInterestContract.SUBTYPE_FIELD  + " text not null, "
			+ PointsOfInterestContract.TITLE_FIELD  + " text not null, "
			+ PointsOfInterestContract.DESCRIPTION_FIELD  + " text, "
			+ PointsOfInterestContract.LATITUDE_FIELD  + " float, "
			+ PointsOfInterestContract.LONGITUDE_FIELD  + " float ); ";

	public Nissan360Database(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_VEHICLES);
		db.execSQL(CREATE_TABLE_RESERVATIONS);
		db.execSQL(CREATE_TABLE_RESERVATIONS_QUEUE);
		db.execSQL(CREATE_TABLE_AGENDA);
		db.execSQL(CREATE_TABLE_POIS);
		db.execSQL(CREATE_TABLE_ASSETS);
		db.execSQL(CREATE_TABLE_SMART_POSTERS);
		db.execSQL(CREATE_TABLE_ASSETS_LOG);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DEBUG_TAG, "Upgrading database. Existing contents will be lost. ["
				+ oldVersion + "]->[" + newVersion + "]");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VEHICLES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESERVATIONS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESERVATIONS_QUEUE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POIS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_AGENDA);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSETS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SMART_POSTERS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSETS_LOG);
		onCreate(db);
	}
}



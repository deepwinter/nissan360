package com.amci.nissan360.database;

public class ReservationsQueueContract {
	
	public static final String ID_FIELD = "_id";
	public static final String ATTENDEE_ID_FIELD = "AttendeeId";
	public static final String VEHICLE_ID_FIELD = "VehicleId";
	public static final String TRACK_FIELD = "Track";
	public static final String TIME_FIELD = "Time";
	public static final String UPLOADED_FIELD = "Uploaded";

	
}

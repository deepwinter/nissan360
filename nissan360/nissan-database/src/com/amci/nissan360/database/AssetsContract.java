package com.amci.nissan360.database;

public class AssetsContract {

	public static final String ID_FIELD = "_id";
	public static final String NAME_FIELD = "Name";
	public static final String TYPE_FIELD = "Type";
	public static final String LOCATION_FIELD = "Location";
	public static final String TAG_ID_FIELD = "TagId";
	public static final String ASSET_ID_FIELD = "AssetId";
	public static final String LONGITUDE_FIELD = "Longitude";
	public static final String LATITUDE_FIELD = "Latitude";

	public static final String SMART_POSTER_IDENTIFIER = "Identifier";
	public static final String SMART_POSTER_NAME_FIELD = "Name";
	public static final String SMART_POSTER_FILE_FIELD = "File";
	
	public static final String VEHICLE_TYPE = "Vehicle";
	public static final String SMART_POSTER_TYPE = "SmartPoster";
	public static final String POINT_OF_INTEREST_TYPE = "PointOfInterest";
	public static final String PERFORMANCE_TRACK_SHUTTLE_TYPE = "PerformanceTrackShuttle";
	public static final String ADMIN_OVERRIDE_TYPE = "AdminOverride";

}

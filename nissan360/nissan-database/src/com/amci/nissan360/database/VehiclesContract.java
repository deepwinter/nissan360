package com.amci.nissan360.database;

import java.util.ArrayList;

public class VehiclesContract {

	public static final String ID_FIELD = "_id";
	public static final String VIN_FIELD = "VIN";
	public static final String NAME_FIELD = "Name";
	public static final String BRAND_FIELD = "Brand";
	public static final String MODEL_FIELD = "Model";
	public static final String HERO_IMAGE_FIELD = "HeroImage";
	public static final String DESCRIPTION_FIELD = "Description";
	public static final String TRACK_FIELD = "TRACK";
	public static final String CATEGORY_FIELD = "Category";
	public static final String SPEC_SHEET_FIELD = "SpecSheet";
	public static final String SPEC_SHEET_IMAGE_FIELD = "SpecSheetImage";
	public static final String STORY1_NAME_FIELD = "Story1Name";
	public static final String STORY1_TEXT_FIELD = "Story1Text";
	public static final String STORY1_IMAGE_FIELD = "Story1Image";
	public static final String STORY2_NAME_FIELD = "Story2Name";
	public static final String STORY2_TEXT_FIELD = "Story2Text";
	public static final String STORY2_IMAGE_FIELD = "Story2Image";
	public static final String STORY3_NAME_FIELD = "Story3Name";
	public static final String STORY3_TEXT_FIELD = "Story3Text";
	public static final String STORY3_IMAGE_FIELD = "Story3Image";
	public static final String THUMBNAIL_FIELD = "Thumbnail";
	public static final String BUCKET_FIELD = "Bucket";
	public static final String LATITUDE_FIELD = "Latitude";
	public static final String LONGITUDE_FIELD = "Longitude";

	
	public static final String TRACK_TYPE_HIGH_PERFORMANCE = "High Performance Drive";
	
	public static final String TRACK_TYPE_STREET_URBAN = "West/Urban Drive";
	public static final String TRACK_TYPE_STREET_RURAL = "East/Rural";
	public static final String TRACK_TYPE_STREET_TEST = "Street Course One";
	public static ArrayList<String> TRACK_TYPES_STREET = null;
	
	public static final String TRACK_NAME_STREET_URBAN = "Urban Route (30 minutes)";
	public static final String TRACK_NAME_STREET_RURAL = "Rural Route (45 minutes)";

	public static final String TRACK_TYPE_PERFORMANCE = "Performance Drive";
	public static final String TRACK_TYPE_WORLD_DRIVE = "World Drive";
	public static final String TRACK_TYPE_ADVENTURE = "Adventure Drive";
	public static final String TRACK_TYPE_LCV_DRIVE = "LCV Drive";
	public static final String TRACK_TYPE_ALEAF_DRIVE = "ALEAF Drive";
	public static ArrayList<String> TRACK_TYPES_WALKUP = null;
	
	public static final String TRACK_TYPE_DISPLAY = "Display";
	
	public static final String GPX_FILE_WEST_URBAN = "west_urban.gpx";
	public static final String GPX_FILE_EAST_RURAL = "east_rural.gpx";
	public static final String GPX_FILE_DEBUG = "debug.gpx";

	
	public static final String BUCKET_DISPLAY_TYPE = "Display";
	public static final String BUCKET_DRIVE_TYPE = "Drive";
	
	
	static {
		TRACK_TYPES_STREET = new ArrayList<String>();
		TRACK_TYPES_STREET.add(TRACK_TYPE_STREET_URBAN);
		TRACK_TYPES_STREET.add(TRACK_TYPE_STREET_RURAL);
		TRACK_TYPES_STREET.add(TRACK_TYPE_STREET_TEST);
		
		TRACK_TYPES_WALKUP = new ArrayList<String>();
		TRACK_TYPES_WALKUP.add(TRACK_TYPE_PERFORMANCE);
		TRACK_TYPES_WALKUP.add(TRACK_TYPE_WORLD_DRIVE);
		TRACK_TYPES_WALKUP.add(TRACK_TYPE_ADVENTURE);
		TRACK_TYPES_WALKUP.add(TRACK_TYPE_LCV_DRIVE);
		TRACK_TYPES_WALKUP.add(TRACK_TYPE_ALEAF_DRIVE);
	}
	
	public static String getStreetTrackDisplayName(String track){
		if(track.equals(VehiclesContract.TRACK_TYPE_STREET_RURAL)){
			return TRACK_NAME_STREET_RURAL;
		} else if(track.equals(VehiclesContract.TRACK_TYPE_STREET_URBAN)){
			return TRACK_NAME_STREET_URBAN;
		} else {
			return "";
		}

	}

}

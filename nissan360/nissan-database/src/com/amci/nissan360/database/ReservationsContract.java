package com.amci.nissan360.database;

public class ReservationsContract {
	
	public static final String ID_FIELD = "_id";
	public static final String RESERVATION_ID_FIELD = "ReservationId";
	public static final String ATTENDEE_ID_FIELD = "AttendeeId";
	public static final String GUEST_FIELD = "Guest";
	public static final String PHONE_FIELD = "Phone";
	public static final String VEHICLE_FIELD = "Vehicle";
	public static final String VEHICLE_ID_FIELD = "VehicleId";
	public static final String TRACK_FIELD = "Track";
	public static final String BEGIN_TIME_FIELD = "BeginTime";
	public static final String END_TIME_FIELD = "EndTime";
	public static final String TYPE_FIELD = "Type";
	public static final String QUEUE_POSITION_FIELD = "QueuePos";
	public static final String LATITUDE_FIELD = "Latitude";
	public static final String LONGITUDE_FIELD = "Longitude";
	
	public static final String RURAL_DRIVE = "East/Rural";
	public static final String URBAN_DRIVE = "West/Urban Drive";
	
}

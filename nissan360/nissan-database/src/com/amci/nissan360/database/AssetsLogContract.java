package com.amci.nissan360.database;

public class AssetsLogContract {
	public static final String ID_FIELD = "_id";
	public static final String DEVICE_ID_FIELD = "deviceId";
	public static final String ATTENDEE_ID_FIELD = "AttendeeId";
	public static final String ASSET_ID_FIELD = "AssetId";
	public static final String TIMESTAMP_FIELD = "Timestamp";
	public static final String UPLOADED_FIELD = "Uploaded";
	public static final String TYPE_FIELD = "Type";
	public static final String MEDIA_NAME_FIELD = "MediaName";
	public static final String INTERACTION_FIELD = "Interaction";
	
	
	public static final String INTERACTION_TYPE_TAP = "Tap";
	public static final String INTERACTION_TYPE_BRIEFCASE = "Briefcase";

	
}

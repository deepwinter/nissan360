package com.amci.nissan360.database;

public class AgendaContract {
	public static final String ID_FIELD = "_id";
	public static final String NAME_FIELD = "Name";
	public static final String DAY_OF_EVENT_FIELD = "DayOfEvent";
	public static final String DATE_FIELD = "Date";
	public static final String TIME_FIELD = "Time";
}

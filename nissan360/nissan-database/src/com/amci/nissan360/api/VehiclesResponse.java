package com.amci.nissan360.api;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VehiclesResponse extends ArrayList<Vehicle> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

package com.amci.nissan360.api;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiError {

	public static final String ERROR_ResErrConflict = "R116:0";
	public static final String ERROR_ResErrInvalid = "R116:1"; // Request had invalid attributes
	public static final String ERROR_ResErrNotAvailable = "R116:2"; // Vehicle is  out of service
	public static final String ERROR_ResErrThrottled = "R116:3"; //Too many request for same vehicle
	public static final String ERROR_ResErrDuplicate = "R116:4"; // Duplicate reservation request
	public static final String ERROR_ResErrOther = "R116:5"; // General error, DB error, etc

	public static final String ERROR_TEXT_UNDEFINED = "Undefined Error";
	public static final String ERROR_TEXT_CONFLICT = "Conflict: ";
	public static final String ERROR_OUT_OF_SERVICE = "Vehicle is Out of Service";
	public static final String ERROR_DUPLICATE_RESERVATION = "You have already reserved this vehicle";
	public static final String ERROR_DUPLICATE_RESERVATION_HIGH_PERFORMANCE = "You have already made a high performance track reservation";

	
	public String Error;
	public String ErrorCode;

	@JsonIgnore
	public String getErrorText(){
		String[] array = ErrorCode.split(":");
		if(array.length < 2){
			return ERROR_TEXT_UNDEFINED;
		}
		String subCode = array[1];
		int intCode = Integer.valueOf(subCode);
		switch(intCode){
		case 0:
			return Error;
		case 1:
			return Error;
		case 2:
			return ERROR_OUT_OF_SERVICE;
		case 3:
			return Error;
		case 4:
			return ERROR_DUPLICATE_RESERVATION;
		case 5:
			return Error;
		default:
			return ERROR_TEXT_UNDEFINED;
		}
	}
}

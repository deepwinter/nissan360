package com.amci.nissan360.api;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class AllUsersRequest extends Nissan360SpiceRequest< AllUsersResponse > {

	String uri;
	
	public AllUsersRequest() {
		super(AllUsersResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "attendees/";
		
	}

	@Override
	public AllUsersResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, AllUsersResponse.class  );	
	}

}

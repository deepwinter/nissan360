package com.amci.nissan360.api;


import org.codehaus.jackson.annotate.JsonIgnore;

import com.amci.nissan360.database.AssetsLogContract;

import android.content.ContentValues;

public class AssetLog {

	public String deviceId;
	public String AssetId;
	public String AttendeeId;
	public String Timestamp;
	public String Interaction; // Briefcase or Tap	
	public String Type;
	
	@JsonIgnore // Just for local use
	public String MediaName;
	
	@JsonIgnore // ignore this
	public String Uploaded = "NO";
	@JsonIgnore // ignore this
	public String ProviderId;
	
	@JsonIgnore // ignore this
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(AssetsLogContract.DEVICE_ID_FIELD, deviceId);
		values.put(AssetsLogContract.ASSET_ID_FIELD, AssetId);
		values.put(AssetsLogContract.TIMESTAMP_FIELD, Timestamp);
		values.put(AssetsLogContract.ATTENDEE_ID_FIELD, AttendeeId);
		values.put(AssetsLogContract.UPLOADED_FIELD, Uploaded );
		values.put(AssetsLogContract.TYPE_FIELD, Type);
		values.put(AssetsLogContract.INTERACTION_FIELD, Interaction);
		values.put(AssetsLogContract.MEDIA_NAME_FIELD, MediaName);
		return values;
	}
}

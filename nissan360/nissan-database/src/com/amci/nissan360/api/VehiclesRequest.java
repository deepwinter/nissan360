package com.amci.nissan360.api;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class VehiclesRequest extends Nissan360SpiceRequest< VehiclesResponse > {

	String uri;
	
	public VehiclesRequest() {
		super(VehiclesResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "vehicles/";
		
	}

	@Override
	public VehiclesResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, VehiclesResponse.class  );	
	}

}

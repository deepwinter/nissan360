package com.amci.nissan360.api;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.Environment;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;


public class DownloadFileRequest extends SpringAndroidSpiceRequest<String> {

	String url;

	public DownloadFileRequest(String url) {
		super(String.class);
		if(!( url.startsWith("http://") || url.startsWith("https://") )){
			url = "http://" + url;
		}
		this.url = url;
	}

	int progressTrack = 0;
	
	@Override
	public String loadDataFromNetwork() throws Exception {
		URL url = new URL(this.url);
		URLConnection connection = url.openConnection();
		connection.connect();
		// this will be useful so that you can show a typical 0-100% progress bar
		int fileLength = connection.getContentLength();

		// download the file
		InputStream input = new BufferedInputStream(url.openStream());
		String outFile = "/tmp.download";
		// Just put the file somewhere and pass back the String file location?
		OutputStream output = new FileOutputStream(  Environment.getExternalStorageDirectory() + outFile ); 
		byte data[] = new byte[1024];
		long total = 0;
		int count;
		while ((count = input.read(data)) != -1) {
			total += count;
			// publishing the progress....
			if( (total % 10000) > 9000){
				int p = (int) (total * 100 / fileLength);
				if(p > progressTrack){
					progressTrack = p;
					publishProgress(progressTrack);
				}
			}
			//publishProgress((int) (total * 100 / fileLength));
			output.write(data, 0, count);
		}

		output.flush();
		output.close();
		input.close();
		return Environment.getExternalStorageDirectory() + outFile;
	}

}


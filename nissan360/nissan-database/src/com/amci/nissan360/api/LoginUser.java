package com.amci.nissan360.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginUser {

	public String ShowUserKey;
	public String LoginId; 				// Used
	public String TestUser;
	public String Name;  				// Used
	public String Email;
	public String Active;
	public String Phone1;
	public String Phone2;
	public String RegDateTime;
	public String Address;
	public String City;
	public String State;
	public String PostalCode;
	public String Country;  			// Used
	public String Company;
	public String AttendeeType;
	public String UserType;
	
}
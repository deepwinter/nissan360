package com.amci.nissan360.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpMessageConverterExtractor;

public class ErrorHandler extends DefaultResponseErrorHandler {

	@Override
	public void handleError(ClientHttpResponse response) throws IOException, Nissan360ApiErrorException {
		// check for expected status codes
		if (isExpected(response.getStatusCode())) {
			// check right here..
			// throw custom exceptions to catch up in lala land ?

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			messageConverters.add(MappingJacksonHttpMessageConverter() );
			HttpMessageConverterExtractor<Error> extractor = new HttpMessageConverterExtractor<Error>(Error.class, messageConverters);

			Error error = null;
			try {
				error = extractor.extractData(response);	
			} catch (Exception e){
				e.printStackTrace();
				super.handleError(response);    
				return;
			}

			// if this works
			// bundle the error up into a custom Nissan exception
			// and throw it.
			Nissan360ApiErrorException exception = new Nissan360ApiErrorException(error);
			throw(exception);


		}
		super.handleError(response);    
	}


	private HttpMessageConverter<?> MappingJacksonHttpMessageConverter() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		// check for expected status codes
		/*
		if (isExpected(response.getStatusCode())) {

			// watch out now..
			// need to return true here for expected errors
			return false;
		}
		 */
		return super.hasError(response);
	}


	private boolean isExpected(HttpStatus statusCode) {
		return (statusCode == HttpStatus.BAD_REQUEST);
	}
}

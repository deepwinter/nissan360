package com.amci.nissan360.api.configuration;

import java.util.ArrayList;


import com.amci.nissan360.api.AgendaItem;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgendaDay {
	public String Date;
	public int DayOfEvent;
	public ArrayList<AgendaItem> Events;
}

package com.amci.nissan360.api.configuration;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactList {

	static public String CONTACTS_TYPE = "Contacts";
	public String Type = CONTACTS_TYPE;
	
	public ArrayList<Contact> contacts;

}

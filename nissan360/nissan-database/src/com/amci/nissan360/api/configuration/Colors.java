package com.amci.nissan360.api.configuration;

import java.util.ArrayList;
import java.util.Iterator;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Colors {
	
	public static final String COLOR_MAP_KIND = "COLOR_MAP_KIND";
	static public String COLORS_TYPE = "Colors";
	public String Type = COLORS_TYPE; 
	
	static public String DEFAULT_COLOR = "FE7569"; // default red color
	
	public ArrayList<MapColor> map_colors;
	
	@JsonIgnore
	public String getColor(String kind, String identifier){
		if(map_colors == null || map_colors.size() < 1){
			return DEFAULT_COLOR;
		}
		Iterator<MapColor> i = map_colors.iterator();
		while(i.hasNext()){
			MapColor color = i.next();
			if(color.subtype.equals(identifier)){
				return color.color_hex;
			}
		}
		return DEFAULT_COLOR;
	}
}

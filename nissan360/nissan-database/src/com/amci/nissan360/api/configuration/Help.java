package com.amci.nissan360.api.configuration;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Help {
	
	static public String HELP_TYPE = "Help";
	public String Type = HELP_TYPE;
	
	public ArrayList<HelpCategory> help_categories;
}

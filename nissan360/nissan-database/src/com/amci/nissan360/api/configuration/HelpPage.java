package com.amci.nissan360.api.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HelpPage {
	String Body;
	String Image;
	String Title;
	
}

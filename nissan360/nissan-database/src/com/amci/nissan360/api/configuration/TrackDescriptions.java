package com.amci.nissan360.api.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackDescriptions {
	
	static public String TRACK_DESCRIPTIONS_TYPE = "TrackDescriptions";
	
	public String Type = TRACK_DESCRIPTIONS_TYPE;
	
	public String street_drive;
	public String performance_drive;
	public String world_drive;
	public String high_performance_drive;
	public String aleaf_drive;
	public String lcv_drive;
	public String adventure_drive;
	
}

package com.amci.nissan360.api.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.content.ContentValues;

import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.PointsOfInterestContract;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PointOfInterest {
	
	public int id;
	public String type;
	public String subtype;
	public String title;
	public double latitude;
	public double longitude;
	
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(PointsOfInterestContract.IDENTIFIER_FIELD, String.valueOf(id));
		values.put(PointsOfInterestContract.TYPE_FIELD, type);
		values.put(PointsOfInterestContract.SUBTYPE_FIELD, subtype);
		values.put(PointsOfInterestContract.TITLE_FIELD, title);
		values.put(PointsOfInterestContract.LATITUDE_FIELD, latitude);
		values.put(PointsOfInterestContract.LONGITUDE_FIELD, longitude);
		return values;
	}
}

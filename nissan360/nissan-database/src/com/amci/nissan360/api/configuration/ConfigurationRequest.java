package com.amci.nissan360.api.configuration;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class ConfigurationRequest extends Nissan360SpiceRequest< ConfigurationResponse > {

	String uri;
	
	public ConfigurationRequest() {
		super(ConfigurationResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "config/";
		
	}

	@Override
	public ConfigurationResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, ConfigurationResponse.class  );	
	}

}

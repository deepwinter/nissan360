package com.amci.nissan360.api.configuration;

import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Coordinates {
	
	static public String COORDINATES_TYPE = "Coordinates";
	static public final String COORINDATE_STREET_DRIVE_PICKUP = "street_drive_pickup";
	static public final String COORINDATE_PERFORMANCE_TRACK_SHUTTLE_PICKUP = "performance_track_shuttle_pickup";

	public String Type = COORDINATES_TYPE;
	public HashMap<String, Coordinate> points;
	
}

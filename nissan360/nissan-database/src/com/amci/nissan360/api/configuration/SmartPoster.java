package com.amci.nissan360.api.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.content.ContentValues;

import com.amci.nissan360.database.AssetsContract;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SmartPoster {

	public int id;
	public String file;
	public String name;
	
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(AssetsContract.SMART_POSTER_IDENTIFIER, id);
		values.put(AssetsContract.SMART_POSTER_FILE_FIELD, file);
		values.put(AssetsContract.SMART_POSTER_NAME_FIELD, name);
		return values;
	}
}

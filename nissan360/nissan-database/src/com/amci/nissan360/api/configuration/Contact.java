package com.amci.nissan360.api.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {
	
	public static final String STREET_DRIVE_HELP_TYPE = "Street Drive Support";
	
	public String name;
	public String phone_number;
}

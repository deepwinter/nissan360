package com.amci.nissan360.api.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Coordinate {
	public double latitude;
	public double longitude;
}

package com.amci.nissan360.api.configuration;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigurationResponse {

	public ArrayList<PointOfInterest> points_of_interest;
	public ArrayList<AgendaDay> agenda;
	public ArrayList<SmartPoster> smart_posters;
	public ContactList contact_list;
	//public Help help;
	public TrackDescriptions track_descriptions;
	public Coordinates coordinates;
	public Colors colors;
}

package com.amci.nissan360.api.configuration;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HelpCategory {

	String Icon;
	ArrayList<HelpPage> Pages;
	
}

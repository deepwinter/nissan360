package com.amci.nissan360.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class DevicePut extends Nissan360SpiceRequest< ContentStatus >  {

	String uri;
	Device device;
	
	public DevicePut(Device device) {
		super(ContentStatus.class);
		
		uri = ApiSettings.API_BASE_URI + "devices/"+device.deviceId;
		this.device = device;
		
	}

	@Override
	public ContentStatus loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();

		/*
		 * 
		 * PUT logic
		 */
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Device> requestEntity = new HttpEntity<Device>(device, headers);
		
		ResponseEntity<ContentStatus> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, requestEntity,ContentStatus.class);
		return responseEntity.getBody();
	}

}

package com.amci.nissan360.api;

import java.io.IOException;

import com.octo.android.robospice.persistence.exception.SpiceException;

public class Nissan360ApiErrorException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7469213868562784245L;
	
	public Error error;
	
	public Nissan360ApiErrorException(Error error){
		super("Api Process Error In Process");
		this.error = error;
	}
	
}

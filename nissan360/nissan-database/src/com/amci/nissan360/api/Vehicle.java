package com.amci.nissan360.api;

import com.amci.nissan360.database.VehiclesContract;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


import android.content.ContentValues;
import android.database.Cursor;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicle {
	
	public int IdentityId;
	public String VIN;
	public String Brand;
	public String Model;
	public String HeroImage;
	public String Description;
	public String Track;
	public String Bucket;
	public boolean Active;
	public String Category;
	public String SpaceID;
	public String SpecSheet;
	public String SpecSheetImage;
	public String Story1Name;
	public String Story1Text;
	public String Story1Image;
	public String Story2Name;
	public String Story2Text;
	public String Story2Image;
	public String Story3Name;
	public String Story3Image;
	public String Story3Text;
	public String Thumbnail;
	public double Latitude;
	public double Longitude;
	
	public Vehicle(){
		super();
	}
	
	public Vehicle(Cursor vehicle) {
		VIN = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.VIN_FIELD));
		Brand = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.BRAND_FIELD));
		Model = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.MODEL_FIELD));
		HeroImage = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.HERO_IMAGE_FIELD));
		Description = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.DESCRIPTION_FIELD));
		Track = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.TRACK_FIELD));
		Category = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.CATEGORY_FIELD));
		SpecSheet = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.SPEC_SHEET_FIELD));
		//Active = vehicle.getBoolean(vehicle.getColumnIndex(VehiclesContract.)));
		SpecSheetImage = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.SPEC_SHEET_IMAGE_FIELD));
		Story1Name = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_NAME_FIELD));
		Story1Text = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_TEXT_FIELD));
		Story1Image = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_IMAGE_FIELD));
		Story2Name = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_NAME_FIELD));
		Story2Text = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_TEXT_FIELD));
		Story2Image = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY2_IMAGE_FIELD));
		Story3Name = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_NAME_FIELD));
		Story3Text = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY1_TEXT_FIELD));
		Story3Image = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.STORY3_IMAGE_FIELD));
		Latitude = vehicle.getDouble(vehicle.getColumnIndex(VehiclesContract.LATITUDE_FIELD));
		Longitude = vehicle.getDouble(vehicle.getColumnIndex(VehiclesContract.LONGITUDE_FIELD));
		Thumbnail = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.THUMBNAIL_FIELD));
		Bucket = vehicle.getString(vehicle.getColumnIndex(VehiclesContract.BUCKET_FIELD));

	}

	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(VehiclesContract.NAME_FIELD, Model);  // Deprecated
		values.put(VehiclesContract.VIN_FIELD, VIN);
		values.put(VehiclesContract.BRAND_FIELD, Brand);
		values.put(VehiclesContract.MODEL_FIELD, Model);
		values.put(VehiclesContract.HERO_IMAGE_FIELD, HeroImage);
		values.put(VehiclesContract.DESCRIPTION_FIELD, Model); // Deprecated
		values.put(VehiclesContract.TRACK_FIELD, Track);
		values.put(VehiclesContract.CATEGORY_FIELD, Category);
		values.put(VehiclesContract.SPEC_SHEET_FIELD, SpecSheet);
		values.put(VehiclesContract.SPEC_SHEET_IMAGE_FIELD, SpecSheetImage);
		values.put(VehiclesContract.STORY1_NAME_FIELD, Story1Name);
		values.put(VehiclesContract.STORY1_TEXT_FIELD, Story1Text);
		values.put(VehiclesContract.STORY1_IMAGE_FIELD, Story1Image);
		values.put(VehiclesContract.STORY2_NAME_FIELD, Story2Name);
		values.put(VehiclesContract.STORY2_TEXT_FIELD, Story2Text);
		values.put(VehiclesContract.STORY2_IMAGE_FIELD, Story2Image);
		values.put(VehiclesContract.STORY3_NAME_FIELD, Story3Name);
		values.put(VehiclesContract.STORY3_TEXT_FIELD, Story3Text);
		values.put(VehiclesContract.STORY3_IMAGE_FIELD, Story3Image);
		values.put(VehiclesContract.THUMBNAIL_FIELD, Thumbnail);
		values.put(VehiclesContract.BUCKET_FIELD, Bucket);
		values.put(VehiclesContract.LATITUDE_FIELD, Latitude);
		values.put(VehiclesContract.LONGITUDE_FIELD, Longitude);
		return values;
	}



}

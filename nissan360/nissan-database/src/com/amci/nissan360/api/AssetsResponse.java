package com.amci.nissan360.api;

import java.util.ArrayList;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("serial")
public class AssetsResponse extends ArrayList<Asset> {

}

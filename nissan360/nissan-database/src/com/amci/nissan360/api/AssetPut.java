package com.amci.nissan360.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class AssetPut extends Nissan360SpiceRequest< Boolean > {

	String uri;
	Asset asset;
	
	public AssetPut(Asset asset) {
		super(Boolean.class);
		
		// PUT 
		uri = ApiSettings.API_BASE_URI + "assets/" + asset.TagId;
		//uri = ApiSettings.API_BASE_URI + "assets/" ;
		this.asset = asset;
		
	}

	@Override
	public Boolean loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();

		/*
		 * 
		 * PUT logic
		 */
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Asset> requestEntity = new HttpEntity<Asset>(asset, headers);
		
		ResponseEntity<Boolean> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, requestEntity,	Boolean.class);
		HttpStatus code = responseEntity.getStatusCode();
		return true; // should check the code
		
	}

}

package com.amci.nissan360.api;

import android.content.ContentValues;

import com.amci.nissan360.database.AgendaContract;


public class AgendaItem {

	public String Name;
	/*
	 * might calculate these from the paren
	public int DayOfEvent;
	public String Date;
	*/
	public String Time;


	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(AgendaContract.NAME_FIELD, Name);
		//values.put(AgendaProvider.DAY_OF_EVENT_FIELD, DayOfEvent);
		//values.put(AgendaProvider.DATE_FIELD, Date);
		values.put(AgendaContract.TIME_FIELD, Time);
		return values;
	}
}

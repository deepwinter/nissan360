package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.EmptyResponse;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class ReservationTrackingPost extends Nissan360SpiceRequest< Boolean > {

	String uri;
	ReservationTracking reservationTracking;
	
	public ReservationTrackingPost(ReservationTracking reservationTracking) {
		super(Boolean.class);
		
		uri = ApiSettings.API_BASE_URI + "reservations/tracking";
		this.reservationTracking = reservationTracking;
		
	}

	@Override
	public Boolean loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.postForObject( uri, reservationTracking, Boolean.class );	
	}

}

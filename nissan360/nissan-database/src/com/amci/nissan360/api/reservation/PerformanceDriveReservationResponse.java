package com.amci.nissan360.api.reservation;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PerformanceDriveReservationResponse extends ReservationInfo {
	
}

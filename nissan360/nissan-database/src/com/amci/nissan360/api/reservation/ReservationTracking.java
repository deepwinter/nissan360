package com.amci.nissan360.api.reservation;

public class ReservationTracking {

	public static final String RESERVATION_TRACKING_STATE_ON_COURSE = "on_course";
	
	public String VehicleId;
	public String AttendeeId;
	public int ReservationId;
	public String Status;
	public String Details;

}

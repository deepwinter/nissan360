package com.amci.nissan360.api.reservation;


public class ReservationIdentity extends ReservationDescriptor {
	
	public int ReservationId;
	public String Track;
	public boolean Override;
	
	public ReservationIdentity(String attendeeId, String vehicleId, int reservationId) {
		this(attendeeId, vehicleId, reservationId, "");
	}

	public ReservationIdentity(String attendeeId, String vehicleId, int reservationId, String track) {
		super(attendeeId, vehicleId);
		ReservationId = reservationId;
		Track = track;
	}

}

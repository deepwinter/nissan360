package com.amci.nissan360.api.reservation;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.amci.nissan360.database.VehiclesContract;

public class PerformanceDriveReservationOptions {
	public String AttendeeId;
	public String VehicleId;
	public String Time;
	public String Track;
	
	public PerformanceDriveReservationOptions(String attendeeId,
			String vehicleId, DateTime requestedTimeSlot) {
		super();
		AttendeeId = attendeeId;
		VehicleId = vehicleId;
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Time = fmt.print(requestedTimeSlot);
		Track = VehiclesContract.TRACK_TYPE_HIGH_PERFORMANCE;
	}

	
}

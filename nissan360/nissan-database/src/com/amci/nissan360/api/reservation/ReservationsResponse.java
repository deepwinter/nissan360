package com.amci.nissan360.api.reservation;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationsResponse extends ArrayList<ReservationInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8583519585599814091L;

}

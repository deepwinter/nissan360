package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.EmptyResponse;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class ReservationCheckinRequest extends Nissan360SpiceRequest< ReservationInfo > {

	String uri;
	ReservationIdentity reservation;
	
	public ReservationCheckinRequest(ReservationIdentity reservation) {
		super(ReservationInfo.class);
		
		uri = ApiSettings.API_BASE_URI + "reservations/start/";
		this.reservation = reservation;
	}

	@Override
	public ReservationInfo loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.postForObject( uri, this.reservation, ReservationInfo.class  );	
	}

}

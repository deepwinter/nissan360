package com.amci.nissan360.api.reservation;

import com.amci.nissan360.database.ReservationsContract;

import android.content.ContentValues;


public class ReservationInfo {
	
	public final static String RESERVATION_CREATED_AND_QUEUED = "RESERVATION_CREATED_AND_QUEUED";
	public final static String CHECKIN_SUCCESS = "CHECKIN_SUCCESS";

	
	public int ReservationId;
	public String AttendeeId = "";
	public String Guest = "";
	public String Phone = "";
	public String Vehicle = "";
	public String VehicleId = "";
	public String Track = "";
	public String BeginTime = "";
	public String EndTime = "";
	public String Type = "";
	public float Latitude;
	public float Longitude;
	public int QueuePos;
	public String ResponseCode; // used by checkin to differentiate between checking with and without 
	
	
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(ReservationsContract.RESERVATION_ID_FIELD, ReservationId);
		values.put(ReservationsContract.ATTENDEE_ID_FIELD, AttendeeId);
		values.put(ReservationsContract.GUEST_FIELD, Guest);
		values.put(ReservationsContract.PHONE_FIELD, Phone);
		values.put(ReservationsContract.VEHICLE_FIELD, Vehicle);
		values.put(ReservationsContract.VEHICLE_ID_FIELD, VehicleId);
		values.put(ReservationsContract.TRACK_FIELD, Track);
		values.put(ReservationsContract.BEGIN_TIME_FIELD, BeginTime);
		values.put(ReservationsContract.END_TIME_FIELD, EndTime);
		values.put(ReservationsContract.TYPE_FIELD, Type);
		values.put(ReservationsContract.LATITUDE_FIELD, Latitude);
		values.put(ReservationsContract.LONGITUDE_FIELD, Longitude);
		values.put(ReservationsContract.QUEUE_POSITION_FIELD, QueuePos);
		return values;
	}
}

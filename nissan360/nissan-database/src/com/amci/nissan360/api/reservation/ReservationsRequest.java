package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class ReservationsRequest extends Nissan360SpiceRequest< ReservationsResponse > {

	String uri;
	
	public ReservationsRequest(String loginId) {
		super(ReservationsResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "reservations/for/"+loginId;
		
	}

	@Override
	public ReservationsResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, ReservationsResponse.class  );	
	}

}

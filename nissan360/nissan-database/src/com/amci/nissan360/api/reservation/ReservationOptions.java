package com.amci.nissan360.api.reservation;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import android.content.ContentValues;
import android.database.Cursor;

import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.ReservationsQueueContract;
import org.codehaus.jackson.annotate.JsonIgnore;

public class ReservationOptions extends ReservationDescriptor {
	public String Time;  // Just the current time;
	public String Track;

	// These two are for reservations upload queue
	@JsonIgnore // ignore this
	public String Uploaded = "NO";
	@JsonIgnore // ignore this
	public String ProviderId;

	public ReservationOptions(String attendeeId,
			String vehicleId, String track) {
		super(attendeeId, vehicleId);
		Track = track;
		DateTime dt = new DateTime();
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Time = fmt.print(dt);
	}

	@JsonIgnore
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(ReservationsQueueContract.ATTENDEE_ID_FIELD, AttendeeId);
		values.put(ReservationsQueueContract.VEHICLE_ID_FIELD, VehicleId);
		values.put(ReservationsQueueContract.TRACK_FIELD, Track);
		values.put(ReservationsQueueContract.TIME_FIELD, Time);
		values.put(ReservationsQueueContract.UPLOADED_FIELD, Uploaded);
		return values;
	}

	public static ReservationOptions loadFromCursor(Cursor pendingReservations) {
		ReservationOptions rO = new ReservationOptions(
				pendingReservations.getString(pendingReservations.getColumnIndex(ReservationsQueueContract.ATTENDEE_ID_FIELD)),
				pendingReservations.getString(pendingReservations.getColumnIndex(ReservationsQueueContract.VEHICLE_ID_FIELD)),
				pendingReservations.getString(pendingReservations.getColumnIndex(ReservationsQueueContract.TRACK_FIELD))
				);
		rO.Time = pendingReservations.getString(pendingReservations.getColumnIndex(ReservationsQueueContract.TIME_FIELD));
		rO.ProviderId = pendingReservations.getString(pendingReservations.getColumnIndex(ReservationsQueueContract.ID_FIELD));

		return rO;
	}

}

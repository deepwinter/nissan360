package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class PerformanceTimesRequest extends Nissan360SpiceRequest< PerformanceTimesResponse > {

	String uri;
	
	public PerformanceTimesRequest() {
		super(PerformanceTimesResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "config/performancetimes/";
		
	}

	@Override
	public PerformanceTimesResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, PerformanceTimesResponse.class  );	
	}

}

package com.amci.nissan360.api.reservation;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PerformanceTimesResponse extends ArrayList<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

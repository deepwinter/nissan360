package com.amci.nissan360.api.reservation;


public class ReservationDescriptor {

	public String AttendeeId;
	public String VehicleId;

	public ReservationDescriptor(String attendeeId, String vehicleId) {
		super();
		AttendeeId = attendeeId;
		VehicleId = vehicleId;
	}

}
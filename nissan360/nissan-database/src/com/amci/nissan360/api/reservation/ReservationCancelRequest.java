package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class ReservationCancelRequest extends Nissan360SpiceRequest< Boolean > {
	
	String uri;
	String reservationId;
	
	public ReservationCancelRequest(String reservationId) {
		super(Boolean.class);
		
		this.reservationId = reservationId;
		uri = ApiSettings.API_BASE_URI + "reservations/"+this.reservationId;
	}

	@Override
	public Boolean loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		restTemplate.delete(uri);
		return true;
	}
		
}

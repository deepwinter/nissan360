package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.EmptyResponse;
import com.amci.nissan360.api.Nissan360SpiceRequest;

public class ReservationCheckoutRequest extends Nissan360SpiceRequest< EmptyResponse > {
	
	String uri;
	ReservationIdentity reservation;
	
	public ReservationCheckoutRequest(ReservationIdentity reservation) {
		super(EmptyResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "reservations/complete/";
		this.reservation = reservation;
	}

	@Override
	public EmptyResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.postForObject( uri, this.reservation, EmptyResponse.class  );	
	}
}

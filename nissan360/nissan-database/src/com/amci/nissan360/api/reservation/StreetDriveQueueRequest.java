package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import com.amci.nissan360.api.ErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.Nissan360SpiceRequest;

public class StreetDriveQueueRequest extends Nissan360SpiceRequest< StreetDriveQueueResponse > {

	String uri;
	ReservationOptions options;
	
	public StreetDriveQueueRequest(ReservationOptions setOptions) {
		super(StreetDriveQueueResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "reservations/create/";
		options = setOptions;
	}

	@Override
	public StreetDriveQueueResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.postForObject( uri, options, StreetDriveQueueResponse.class );	
	}

}

package com.amci.nissan360.api.reservation;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.Nissan360SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class PerformanceDriveReservationRequest extends Nissan360SpiceRequest< PerformanceDriveReservationResponse > {

	String uri;
	PerformanceDriveReservationOptions options;
	
	public PerformanceDriveReservationRequest(PerformanceDriveReservationOptions setOptions) {
		super(PerformanceDriveReservationResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "reservations/create/";
		options = setOptions;
	}

	@Override
	public PerformanceDriveReservationResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.postForObject( uri, options, PerformanceDriveReservationResponse.class );	
	}

}

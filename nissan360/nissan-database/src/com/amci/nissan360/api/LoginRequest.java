package com.amci.nissan360.api;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;


public class LoginRequest extends Nissan360SpiceRequest< LoginUser > {

	String uri;
	
	public LoginRequest(String email) {
		super(LoginUser.class);
		
		uri = ApiSettings.API_BASE_URI + "attendees/search/" + email;
		
	}

	@Override
	public LoginUser loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, LoginUser.class  );	
	}

}

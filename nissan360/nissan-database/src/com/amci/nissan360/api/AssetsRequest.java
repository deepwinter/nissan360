package com.amci.nissan360.api;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class AssetsRequest extends Nissan360SpiceRequest< AssetsResponse > {

	String uri;
	
	public AssetsRequest() {
		super(AssetsResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "assets/";
		
	}

	@Override
	public AssetsResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, AssetsResponse.class  );	
	}

}

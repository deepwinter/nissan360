package com.amci.nissan360.api;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


public class Device {

	public String deviceId;
	public String Phone;
	public String AttendeeId;
	public double LocationLat;
	public double LocationLong;
	public String PushToken;
	public String Timestamp;
	
	public Device() {
		super();
		DateTime dt = new DateTime();
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Timestamp = fmt.print(dt);
	}
	
	

}

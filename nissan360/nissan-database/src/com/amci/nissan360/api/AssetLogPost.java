package com.amci.nissan360.api;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class AssetLogPost extends Nissan360SpiceRequest< EmptyResponse > {

	String uri;
	AssetLog assetLog;
	
	public AssetLogPost(AssetLog assetLog) {
		super(EmptyResponse.class);
		
		uri = ApiSettings.API_BASE_URI + "assets/log/";
		this.assetLog = assetLog;
		
	}

	@Override
	public EmptyResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.postForObject( uri, assetLog, EmptyResponse.class );	
	}

}

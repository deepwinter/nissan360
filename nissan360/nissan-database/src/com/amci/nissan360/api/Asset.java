package com.amci.nissan360.api;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.content.ContentValues;

import com.amci.nissan360.database.AssetsContract;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Asset {

	public String Name;
	public String Type;
	public String Location;
	public String TagId;
	public String AssetId;
	public double Latitude;
	public double Longitude;
	
	// Internal
	@JsonIgnore
	public int ProviderId;
	
	@JsonIgnore
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(AssetsContract.NAME_FIELD, Name);
		values.put(AssetsContract.TAG_ID_FIELD, TagId);
		values.put(AssetsContract.ASSET_ID_FIELD, AssetId);
		values.put(AssetsContract.TYPE_FIELD, Type);
		values.put(AssetsContract.LATITUDE_FIELD, Latitude);
		values.put(AssetsContract.LONGITUDE_FIELD, Longitude);
		return values;
	}
	
}

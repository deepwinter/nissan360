package com.amci.nissan360;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Utilities {
	
	
	public static ArrayList<View> getViewsByTag(ViewGroup root, String tag){
		ArrayList<View> views = new ArrayList<View>();
		final int childCount = root.getChildCount();
		for (int i = 0; i < childCount; i++) {
			final View child = root.getChildAt(i);
			if (child instanceof ViewGroup) {
				views.addAll(getViewsByTag((ViewGroup) child, tag));
			}

			final Object tagObj = child.getTag();
			if (tagObj != null && tagObj.equals(tag)) {
				views.add(child);
			}

		}
		return views;
	}
	
	public static String getNissan360MediaDir(Context c){
		return c.getFilesDir().getPath() + "/nissan360/";
	}

	public static boolean setImageBitmapNissan360(Context c,
			ImageView imageView, String string) {

		String imagePath = getNissan360MediaDir(c) + string;
		Bitmap bitmap = Nissan360.application.getImageCache().getBitmap(imagePath);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
			return true;
		} else {
			Bitmap bitmapMissing = Nissan360.application.getImageCache().getBitmap(getNissan360MediaDir(c) + "missing.png");
			if(bitmapMissing != null){
				imageView.setImageBitmap(bitmapMissing);
			}
			return false;
		}
	}
	
	public static Bitmap getMarkerForColor(Context c, String hexColor){
		
		String imagePath = getNissan360MediaDir(c) + "maps/marker_"+hexColor+".png";

		Bitmap bitmap = Nissan360.application.getImageCache().getBitmap(imagePath);
		
		return bitmap;
		
	}

	public static String formatDateTime(DateTime dateTime){
		DateTimeFormatter fmt = new DateTimeFormatterBuilder()
		.appendMonthOfYearShortText()
		.appendLiteral(" ")
		.appendDayOfMonth(2)
		.appendLiteral(" ")
		.appendHourOfHalfday(2)
		.appendLiteral(":")
		.appendMinuteOfHour(2)
		.toFormatter();
		return dateTime.toString(fmt);
	}

	public static String formatDate(DateTime dateTime){
		DateTimeFormatter fmt = new DateTimeFormatterBuilder()
		.appendMonthOfYearShortText()
		.appendLiteral(" ")
		.appendDayOfMonth(2)
		.appendLiteral(" ")
		.toFormatter();
		return dateTime.toString(fmt);
	}

	public static String formatTime(LocalTime localTime){
		DateTimeFormatter fmt = new DateTimeFormatterBuilder()
		.appendHourOfHalfday(2)
		.appendLiteral(":")
		.appendMinuteOfHour(2)
		.toFormatter();
		return localTime.toString(fmt);
	}

	public static boolean unpackZip(String zipFilePath, String targetDir)
	{       
		InputStream is;
		ZipInputStream zis;
		try 
		{
			String filename;
			is = new FileInputStream(zipFilePath);
			zis = new ZipInputStream(new BufferedInputStream(is));          
			ZipEntry ze;
			byte[] buffer = new byte[1024];
			int count;

			while ((ze = zis.getNextEntry()) != null) 
			{
				// zapis do souboru
				filename = ze.getName();

				// Need to create directories if not exists, or
				// it will generate an Exception...
				if (ze.isDirectory()) {
					File fmd = new File(targetDir + filename);
					fmd.mkdirs();
					continue;
				}

				FileOutputStream fout = new FileOutputStream(targetDir + "/" + filename);

				// cteni zipu a zapis
				while ((count = zis.read(buffer)) != -1) 
				{
					fout.write(buffer, 0, count);             
				}

				fout.close();               
				zis.closeEntry();
			}

			zis.close();
		} 
		catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public static String ByteArrayToHexString(byte [] inarray) 
	{
		int i, j, in;
		String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
		String out= "";

		for(j = 0 ; j < inarray.length ; ++j) 
		{
			in = (int) inarray[j] & 0xff;
			i = (in >> 4) & 0x0f;
			out += hex[i];
			i = in & 0x0f;
			out += hex[i];
		}
		return out;
	}
	
	
}

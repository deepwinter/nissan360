package com.amci.nissan360.widgets;

import com.amci.nissan360.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.TextView;


public class NissanTextView extends TextView {
	private final static int BOLD = 0;
	private final static int LIGHT = 1;
	
	

	public NissanTextView(Context context) {
		super(context);
	}

	public NissanTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if(!isInEditMode()) {
			parseAttributes(context, attrs); 
		}
	}

	public NissanTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if(!isInEditMode()) {
			parseAttributes(context, attrs);
		}
	}

	private void parseAttributes(Context context, AttributeSet attrs) {

		TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.NissanTextView);
		int typeface = values.getInt(R.styleable.NissanTextView_typeface, 0);

		Typeface tf;
	    switch(typeface) {
	        case BOLD: default:
	            //You can instantiate your typeface anywhere, I would suggest as a 
	            //singleton somewhere to avoid unnecessary copies
	    		tf = Typeface.createFromAsset(context.getAssets(), "fonts/NissanAG_Bold.ttf");
	            setTypeface(tf); 
	            break;
	        case LIGHT:
	    		tf = Typeface.createFromAsset(context.getAssets(), "fonts/NissanAG_Light.ttf");
	            setTypeface(tf);
	            break;
	    }
	}
	
	/*
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		onTouchEvent(ev);
		return false;
	}
	*/

}


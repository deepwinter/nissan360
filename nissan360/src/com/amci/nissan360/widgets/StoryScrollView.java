package com.amci.nissan360.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View;
import android.widget.ScrollView;

public class StoryScrollView extends ScrollView {

	private GestureDetector mGestureDetector;
	View.OnTouchListener mGestureListener;

	public StoryScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if(!isInEditMode()){
			mGestureDetector = new GestureDetector(context, new YScrollDetector());
			setFadingEdgeLength(0);
		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		onTouchEvent(ev);
		return false;
	}

	// Return false if we're scrolling in the x direction  
	class YScrollDetector extends SimpleOnGestureListener {
		
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			if(Math.abs(distanceY) > Math.abs(distanceX)) {
				return true;
			}
			return false;
		}
	}
}

package com.amci.nissan360.widgets;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class NotificationDialog {

	public static void show(Context c, String title, String message){
		AlertDialog.Builder builder1 = new AlertDialog.Builder(c);
		builder1.setTitle(title);
		builder1.setMessage(message);
		builder1.setCancelable(true);
		builder1.setNeutralButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alert11 = builder1.create();
		alert11.show();

	}
	
}

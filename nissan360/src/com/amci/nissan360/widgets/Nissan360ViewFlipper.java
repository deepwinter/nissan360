package com.amci.nissan360.widgets;

import java.util.jar.Attributes;

import com.amci.nissan360.R;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ViewFlipper;

public class Nissan360ViewFlipper extends ViewFlipper {

	GestureDetector gestureDetector;

	View.OnTouchListener mGestureListener;
	
	int whichChild = 0;

	public Nissan360ViewFlipper(Context context) {
		super(context);
		if(!isInEditMode()){
			gestureDetector = new GestureDetector(getContext(), new innerGestureDetector());
			
		}
	}

	public Nissan360ViewFlipper(Context context, AttributeSet attrs) {
		super(context, attrs);
		if(!isInEditMode()){
			gestureDetector = new GestureDetector(getContext(), new innerGestureDetector());
		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		boolean gestureResult = gestureDetector.onTouchEvent(ev);
		boolean rval = false;
		if (gestureResult) {
			//onTouchEvent(ev);
			rval = true;
			//this.setDisplayedChild(whichChild + 1);
		} else {
			rval = super.onInterceptTouchEvent(ev);
		}
		return rval;

	}
	
	class innerGestureDetector extends SimpleOnGestureListener {


		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

			//Only detect X axis fling
			if( Math.abs(velocityX) > Math.abs(velocityY)){
				if (velocityX < 0){
					// These are not the right animations
					setInAnimation(inFromRightAnimation());
					setOutAnimation(outToLeftAnimation());
					showNext();
				} else {
					setInAnimation(inFromLeftAnimation());
					setOutAnimation(outToRightAnimation());
					showPrevious();

				}
				return true;
			} else {
				return false;
			}

		}

	}
	
	private Animation inFromRightAnimation() {
		  Animation inFromRight = new TranslateAnimation(
		    Animation.RELATIVE_TO_PARENT, +1.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f);
		  inFromRight.setDuration(500);
		  inFromRight.setInterpolator(new AccelerateInterpolator());
		  return inFromRight;
		 }

		 private Animation outToLeftAnimation() {
		  Animation outtoLeft = new TranslateAnimation(
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, -1.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f);
		  outtoLeft.setDuration(500);
		  outtoLeft.setInterpolator(new AccelerateInterpolator());
		  return outtoLeft;
		 }

		 private Animation inFromLeftAnimation() {
		  Animation inFromLeft = new TranslateAnimation(
		    Animation.RELATIVE_TO_PARENT, -1.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f);
		  inFromLeft.setDuration(500);
		  inFromLeft.setInterpolator(new AccelerateInterpolator());
		  return inFromLeft;
		 }

		 private Animation outToRightAnimation() {
		  Animation outtoRight = new TranslateAnimation(
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, +1.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f,
		    Animation.RELATIVE_TO_PARENT, 0.0f);
		  outtoRight.setDuration(500);
		  outtoRight.setInterpolator(new AccelerateInterpolator());
		  return outtoRight;
		 }

}

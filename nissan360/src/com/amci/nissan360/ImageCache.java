package com.amci.nissan360;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;

public class ImageCache {

	private BitmapLruCache mMemoryCache;

	class BitmapLruCache extends LruCache<String, Bitmap> {

		public BitmapLruCache(int maxSize) {
			super(maxSize);
		}

		// recycle bitmaps once they lead the cache
		// hopefully this helps with memory issues	
		@Override
		protected void entryRemoved(boolean evicted, String key,
				Bitmap oldValue, Bitmap newValue) {
			if(newValue == null){
				return;
			}
			newValue.recycle();
			super.entryRemoved(evicted, key, oldValue, newValue);
			newValue = null;
			
		}
		
	}
	
	
	public ImageCache() {
	    // Get max available VM memory, exceeding this amount will throw an
	    // OutOfMemory exception. Stored in kilobytes as LruCache takes an
	    // int in its constructor.
	    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
	    
	    // Use 1/8th of the available memory for this memory cache.
	    final int cacheSize = maxMemory / 6; // This isn't enough memory
	    									 // The images are just too big...

	    mMemoryCache = new BitmapLruCache(cacheSize) {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) {
	            // The cache size will be measured in kilobytes rather than
	            // number of items.
	            return bitmap.getByteCount() / 1024;
	        }
	    };
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
	    if (getBitmapFromMemCache(key) == null) {
	        mMemoryCache.put(key, bitmap);
	    }
	}

	public Bitmap getBitmapFromMemCache(String key) {
	    Bitmap bitmap = mMemoryCache.get(key);
	    if(bitmap == null || bitmap.isRecycled()){
	    	return null;
	    } else {
	    	return bitmap;
	    }
	}
	
	
}

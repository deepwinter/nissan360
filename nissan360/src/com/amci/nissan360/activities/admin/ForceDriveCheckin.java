package com.amci.nissan360.activities.admin;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.base.RobospiceActivity;
import com.amci.nissan360.activities.drive.VehicleExperienceController;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.provider.AssetsProvider;

public class ForceDriveCheckin extends RobospiceActivity {

	private static final String VEHICLE_TAPPED_EXTRA = "VEHICLE_TAPPED_EXTRA";
	private PendingIntent pendingIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_force_drive_checkin);


	}


	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);

		if( getIntent().getExtras() != null && getIntent().getExtras().getString(VEHICLE_TAPPED_EXTRA) != null){


			Tag myTag = (Tag) getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
			String tagId = Utilities.ByteArrayToHexString(myTag.getId());
			//Toast.makeText(this, "NFC Found! " + tagId, Toast.LENGTH_SHORT).show();

			String [] projection = {
					AssetsContract.ASSET_ID_FIELD,
					AssetsContract.TAG_ID_FIELD,
					AssetsContract.TYPE_FIELD
			};

			Cursor results = getContentResolver().query( Uri.withAppendedPath(AssetsProvider.ASSETS_CONTENT_URI, tagId), projection, null, null, null);
			if(results.getCount() == 0){
				Log.w("TagDiscovered", "Tag Not Found");
				Toast.makeText(this, "Tag not found " + tagId, Toast.LENGTH_LONG).show();
				return;
			}
			results.moveToFirst();
			String type = results.getString(results.getColumnIndex(AssetsContract.TYPE_FIELD));
			String assetId = results.getString(results.getColumnIndex(AssetsContract.ASSET_ID_FIELD));

			if (type.equals(AssetsContract.VEHICLE_TYPE)){
				Intent i = new Intent(this, VehicleExperienceController.class);
				i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, assetId);
				i.putExtra(VehicleExperienceController.ATTEMPT_FORCE_START_DRIVE, true);
				startActivity(i);
				finish();
			} 
		}

	}

	protected void onResume() {
		super.onResume();

		Intent i = new Intent(this, ForceDriveCheckin.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP );
		i.putExtra(ForceDriveCheckin.VEHICLE_TAPPED_EXTRA, ForceDriveCheckin.VEHICLE_TAPPED_EXTRA);
		pendingIntent = PendingIntent.getActivity(
				this, 0, i, 0);
		IntentFilter tagDiscoveredFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter intentFiltersArray[] = new IntentFilter[] {tagDiscoveredFilter };

		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, intentFiltersArray, null);
		}


	}

	public void onPause() {
		super.onPause();
		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).disableForegroundDispatch(this);
		}
	}



}

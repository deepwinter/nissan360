package com.amci.nissan360.activities.admin;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.Nissan360Activity;

public class Loader extends Nissan360Activity {

	TextView mDoneText;
	ProgressBar mProgressSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loader);
		
		mDoneText = (TextView) findViewById(R.id.loader_done);
		mProgressSpinner = (ProgressBar) findViewById(R.id.loader_progress_bar);
	}
	
	public void onStart(){
		super.onStart();
		
		
		
		try {
			Nissan360.application.getNissanApiService().reloadAllConfigurationData(
					
					new LoadingDoneListener(){

						@Override
						public void done() {
							mDoneText.setVisibility(View.VISIBLE);
							mProgressSpinner.setVisibility(View.GONE);
						}
						
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	


}

	package com.amci.nissan360.activities.admin;

import java.io.File;

import net.osmand.IndexConstants;
import net.osmand.plus.activities.MapActivity;
import net.osmand.plus.activities.OsmandIntents;

import com.actionbarsherlock.app.SherlockActivity;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.AutoSetup;
import com.amci.nissan360.activities.drive.HighPerformanceTrackCheckinController;
import com.amci.nissan360.activities.drive.VehicleExperienceController;
import com.amci.nissan360.activities.nfc.TagDiscovered;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.state.ApplicationState;

import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class HiddenPanel extends SherlockActivity {

	ListView mListView;
	PendingIntent pendingIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hidden_panel);

		mListView = (ListView) findViewById(R.id.hiddenPanelOptionsList);

		String versionName = "Unknown Version";
		try {
			versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		String[] items = { "Load Application Data",  // 0
				"Reload All Media",   // 1
				"OSMAnd File Settings",  // 2
				"Force Drive Checkin", // 3
				"Manual Start East/Rural Drive", // 4 
				"Manual Start West/Urban Drive",  // 5
				"Force Performance Track Checkin", // 6
				"Force Exit Drive Mode",  // 7
				"App Version: " + versionName, // 8
				"Media Patch Version: " +  settings.getString(Nissan360.MEDIA_DOWNLOAD_HASH_PREFERENCE, null),
				"Media Patched At: " +  settings.getString(Nissan360.MEDIA_DOWNLOAD_HASH_TIMESTAMP, null),
				"Manual Start Debug Drive", // 11

		};
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
		mListView.setAdapter(arrayAdapter);

		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				switch(position){
				case 0:
				{
					Intent i = new Intent(HiddenPanel.this, Loader.class);
					startActivity(i);
					break;
				}
				case 1:
				{
					// clear prefs
					SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());	
					Editor editor = settings.edit();
					editor.remove(Nissan360.NISSAN360_APK_ASSETS_VERSION_PREFERENCE);
					editor.remove(Nissan360.NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE);
					editor.remove(Nissan360.MEDIA_DOWNLOAD_HASH_PREFERENCE);
					editor.commit();
					
					try {
						Nissan360.application.getNissanApiService().reloadAllConfigurationData(null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Intent i = new Intent(HiddenPanel.this, AutoSetup.class);
					startActivity(i);
					break;
				}
				case 2:
					boolean empty = ((Nissan360) getApplication()).getResourceManager().getIndexFileNames().isEmpty();
					if (empty) {
						File folder = ((Nissan360) getApplication()).getAppPath(IndexConstants.BACKUP_INDEX_DIR);
						if (folder.exists() && folder.isDirectory()) {
							String[] l = folder.list();
							empty = l == null || l.length == 0;
						}
					}
					if (empty) {
						startActivity(new Intent(HiddenPanel.this, OsmandIntents.getDownloadIndexActivity()));
					} else {
						startActivity(new Intent(HiddenPanel.this, OsmandIntents.getLocalIndexActivity()));
					}
					break;
			
				case 3:
					Intent i = new Intent(HiddenPanel.this, ForceDriveCheckin.class);
					startActivity(i);
					break;
				case 4:
					ApplicationState.moveToDriveMode(HiddenPanel.this, VehiclesContract.GPX_FILE_EAST_RURAL);
					break;
				case 5:
					ApplicationState.moveToDriveMode(HiddenPanel.this, VehiclesContract.GPX_FILE_WEST_URBAN);
					break;
				case 6:
					Intent intent = new Intent(HiddenPanel.this, HighPerformanceTrackCheckinController.class);
					intent.putExtra(HighPerformanceTrackCheckinController.INTENT_EXTRA_FORCE_CHECKIN, true);
					startActivity(intent);
					break;
				case 7:
					
					// TODO: need to check the user OUT of the current vehicle
					ApplicationState.leaveDriveMode(HiddenPanel.this);
					break;
				case 11:
					ApplicationState.moveToDriveMode(HiddenPanel.this, VehiclesContract.GPX_FILE_DEBUG);
					break;
				default:
					break;
				}

				finish();

			}

		});

	}

	protected void onResume() {
		super.onResume();


		// We want to be able to force a drive to start for a certain vehicle
		Intent i = new Intent(this, TagDiscovered.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP );
		i.putExtra(TagDiscovered.ATTEMPT_FORCE_START_DRIVE, true);
		pendingIntent = PendingIntent.getActivity(
				this, 0, i, 0);
		IntentFilter tagDiscoveredFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter intentFiltersArray[] = new IntentFilter[] {tagDiscoveredFilter };
		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, intentFiltersArray, null);
		}


	}

	public void onPause() {
		super.onPause();
		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).disableForegroundDispatch(this);
		}
	}


}

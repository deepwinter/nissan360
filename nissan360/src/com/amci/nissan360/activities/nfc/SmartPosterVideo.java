package com.amci.nissan360.activities.nfc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.winterroot.hzsv.CarouselAdapter;

import com.actionbarsherlock.app.SherlockActivity;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.base.Nissan360Activity;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.view.Menu;
import android.app.Activity; 
import android.content.Context; 
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat; 
import android.media.MediaPlayer; 
import android.media.MediaPlayer.OnBufferingUpdateListener; 
import android.media.MediaPlayer.OnCompletionListener; 
import android.media.MediaPlayer.OnErrorListener; 
import android.os.Bundle; 
import android.util.Log; 
import android.view.Menu; 
import android.view.SurfaceHolder; 
import android.view.SurfaceView; 
import android.view.Window; 
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class SmartPosterVideo extends Nissan360Activity {

	public static final String VIDEO_FILENAME_EXTRA = "VIDEO_FILENAME";
	private VideoView video;
	private MediaController ctlr;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.activity_smart_poster_video);
		
		
		
		Bundle extras = getIntent().getExtras();
		String filename = extras.getString(VIDEO_FILENAME_EXTRA);
		
		String imagePath = getFilesDir().getPath() + "/nissan360/" + filename;

		File clip= new File(imagePath);

		if (!clip.exists()) {
			Toast.makeText(this, "Video was not found", Toast.LENGTH_SHORT).show();
			finish();
		}


		if (clip.exists()) {
			video=(VideoView)findViewById(R.id.video);
			video.setVideoPath(clip.getAbsolutePath());

			ctlr=new MediaController(this);
			ctlr.setMediaPlayer(video);
			video.setMediaController(ctlr);
			video.requestFocus();
			video.start();
		}

	} 
}

package com.amci.nissan360.activities.nfc;

import java.io.File;

import com.actionbarsherlock.app.SherlockActivity;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.view.Menu;
import android.app.Activity; 
import android.content.Context; 
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat; 
import android.media.MediaPlayer; 
import android.media.MediaPlayer.OnBufferingUpdateListener; 
import android.media.MediaPlayer.OnCompletionListener; 
import android.media.MediaPlayer.OnErrorListener; 
import android.os.Bundle; 
import android.util.Log; 
import android.view.Menu; 
import android.view.SurfaceHolder; 
import android.view.SurfaceView; 
import android.view.Window; 
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class SmartPoster extends Nissan360NavigationActivity { 

	public static final String POSTER_FILENAME_EXTRA = "POSTER_FILENAME_EXTRA";
	ImageView mImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_smart_poster);
		
		mImageView = (ImageView) findViewById(R.id.smartPosterImage);

		Bundle extras = getIntent().getExtras();
		String filename = extras.getString(POSTER_FILENAME_EXTRA);
		if(filename == null){
			return;
		}


		String imagePath = getFilesDir().getPath() + "/nissan360/smart_posters/" + filename;
		File file= new File(imagePath);

		if (file.exists()) {

			try {
				mImageView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
			} catch (Exception e){
				e.printStackTrace();
			}

		} else {
			Toast.makeText(this, "Poster was not found", Toast.LENGTH_SHORT).show();
			finish();
		}

	}
}



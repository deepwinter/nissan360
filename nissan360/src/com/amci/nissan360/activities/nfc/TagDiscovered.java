package com.amci.nissan360.activities.nfc;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.admin.HiddenPanel;
import com.amci.nissan360.activities.base.Nissan360OptionsMenuActivity;
import com.amci.nissan360.activities.drive.HighPerformanceTrackCheckinController;
import com.amci.nissan360.activities.drive.VehicleExperienceController;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.helper.SmartPosterHelper;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.AssetsProvider;
import com.amci.nissan360.services.NissanApiService;
import com.amci.nissan360.state.ApplicationState;

import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

public class TagDiscovered extends Nissan360OptionsMenuActivity {

	public static String ATTEMPT_FORCE_START_DRIVE = "ATTEMPT_FORCE_START_DRIVE";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tag_discovered);
	}



	@Override
	protected void onStart() {
		super.onStart();
		handleNFCTag();
	}



	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);

		handleNFCTag();

	}

	private void handleNFCTag(){

		Tag myTag = (Tag) getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String tagId = Utilities.ByteArrayToHexString(myTag.getId());
		//Toast.makeText(this, "NFC Found! " + tagId, Toast.LENGTH_SHORT).show();

		String [] projection = {
				AssetsContract.ASSET_ID_FIELD,
				AssetsContract.TAG_ID_FIELD,
				AssetsContract.TYPE_FIELD
		};

		Cursor results = getContentResolver().query( Uri.withAppendedPath(AssetsProvider.ASSETS_CONTENT_URI, tagId), projection, null, null, null);
		if(results.getCount() == 0){
			Log.w("TagDiscovered", "Tag Not Found");
			finish();
			return;
		}
		results.moveToFirst();
		String type = results.getString(results.getColumnIndex(AssetsContract.TYPE_FIELD));
		String assetId = results.getString(results.getColumnIndex(AssetsContract.ASSET_ID_FIELD));

		if(ApplicationState.isInDriveMode(this)
				|| ApplicationState.isCheckedIn(this)){
			if(type.equals(AssetsContract.ADMIN_OVERRIDE_TYPE)){
				startHiddenPanel();
				return;
			}
		}


		// Skip the special types
		if( ! (type.equals(AssetsContract.PERFORMANCE_TRACK_SHUTTLE_TYPE)
				|| type.equals(AssetsContract.ADMIN_OVERRIDE_TYPE) ) ){


			try {
				Nissan360.application.getNissanApiService().logAssetTap(type, assetId, null, AssetsLogContract.INTERACTION_TYPE_TAP );
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		if(type.equals(AssetsContract.SMART_POSTER_TYPE)){

			SmartPosterHelper.launchSmartPoster(this, assetId);
			finish();

		} else if (type.equals(AssetsContract.VEHICLE_TYPE)){
			Intent i = new Intent(this, VehicleExperienceController.class);
			i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, assetId);
			if(getIntent().getExtras().getBoolean(ATTEMPT_FORCE_START_DRIVE, false)){
				i.putExtra(VehicleExperienceController.ATTEMPT_FORCE_START_DRIVE, true);
			}
			startActivity(i);
			finish();
		} else if (type.equals(AssetsContract.PERFORMANCE_TRACK_SHUTTLE_TYPE)){
			Intent i = new Intent(this, HighPerformanceTrackCheckinController.class);
			startActivity(i);
			finish();
		} else if (type.equals(AssetsContract.ADMIN_OVERRIDE_TYPE)){
			startHiddenPanel();
		} else {

			Toast.makeText(this, "No Type for Tag", Toast.LENGTH_SHORT).show();
		}

	}



	private void startHiddenPanel() {
		Intent i = new Intent(this, HiddenPanel.class);
		startActivity(i);
		finish();
	}

	
}

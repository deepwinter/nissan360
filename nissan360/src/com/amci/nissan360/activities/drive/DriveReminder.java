package com.amci.nissan360.activities.drive;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal.OnCancelListener;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amci.nissan360.MapIntentContract;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.api.reservation.ReservationCancelRequest;
import com.amci.nissan360.api.reservation.ReservationsRequest;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.ReservationsContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.PointsOfInterestProvider;
import com.amci.nissan360.provider.ReservationsProvider;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.state.ReservationState;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.walnutlabs.android.ProgressHUD;

public class DriveReminder extends Nissan360NavigationActivity {

	public static final String INTENT_EXTRA_MINUTES_UNTIL_RESERVATION = "INTENT_EXTRA_MINUTES_UNTIL_RESERVATION";
	public static final String INTENT_EXTRA_RESERVATION_ID = "INTENT_EXTRA_RESERVATION_ID";
	public static final String INTENT_EXTRA_MESSAGE = "INTENT_EXTRA_MESSAGE";
	Button driveLocationButton;
	Button snoozeButton;
	Button cancelDriveButton;
	TextView messageView;
	private double latitude = 0;
	private double longitude = 0;
	private String reservationId;
	private Cursor c;
	
	String model = "";
	String minutesUntil = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drive_reminder);

		Bundle extras = getIntent().getExtras();

		latitude = extras.getDouble(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE);
		longitude = extras.getDouble(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE);

		if(extras.containsKey(INTENT_EXTRA_MINUTES_UNTIL_RESERVATION)){
			minutesUntil = extras.getString(INTENT_EXTRA_MINUTES_UNTIL_RESERVATION, "");
		}

		reservationId = extras.getString(INTENT_EXTRA_RESERVATION_ID);
		// Get reservation record so we can get the VIN
		c = getContentResolver().query(
				Uri.withAppendedPath(ReservationsProvider.CONTENT_URI, reservationId),
						ReservationsProvider.getDefaultProjection(), null, null, null);
		if(c.getCount() > 0){
			c.moveToPosition(0);
			String vin = c.getString( c.getColumnIndex(ReservationsContract.VEHICLE_ID_FIELD));
			
			Cursor vCursor = getContentResolver().query(Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, vin),
					VehiclesProvider.getDefaultProjection(), null, null, null);
			if(vCursor.getCount() > 0){
				vCursor.moveToFirst();
				model = vCursor.getString(vCursor.getColumnIndex(VehiclesContract.MODEL_FIELD));
			}
		}
		
		
		messageView = (TextView) findViewById(R.id.drive_reminder_message_view);
		
		String message = "Drive Reminder";
		if(model.length() > 0){
			message += " - " + model + "\n\n";
		}
		
		if(minutesUntil.length() > 0){
			message += "Your test drive is scheduled to being in " + minutesUntil + " minutes.";
		}
		
		message += "Please proceed to the drive staging area\n\n";
		
		
		message += extras.getString(INTENT_EXTRA_MESSAGE, "");
		
		messageView.setText(message);
		

		driveLocationButton = (Button) findViewById(R.id.drive_reminder_location_map_button);
		driveLocationButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				

				Cursor c = getContentResolver().query(PointsOfInterestProvider.CONTENT_URI_GEO_ASSETS, PointsOfInterestProvider.getGeoAssetsProjection(),
						"subtype = 'Street Drive'", null, null);
				if(c.getCount() < 1){
					return;
				}
				c.moveToFirst();
				double latitude = c.getDouble(c.getColumnIndex(AssetsContract.LATITUDE_FIELD));
				double longitude = c.getDouble(c.getColumnIndex(AssetsContract.LONGITUDE_FIELD));
				
				if(latitude == 0 || longitude == 0){
					Toast.makeText(DriveReminder.this, "Coordinates not found", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent i = new Intent(DriveReminder.this, DriveLocationMap.class);
				i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE, latitude);
				i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE, longitude);
				startActivity(i);
				finish();
			}

		});

		snoozeButton = (Button) findViewById(R.id.drive_reminder_snooze_button);
		snoozeButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				finish();
			}

		});

		cancelDriveButton = (Button) findViewById(R.id.drive_reminder_cancel_drive_button);
		if(reservationId == null || reservationId.equals("")){
			cancelDriveButton.setVisibility(View.GONE);
		} else {
			cancelDriveButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {


					class CancelDriveDialogFragment extends DialogFragment implements OnCancelListener {

						public ProgressHUD mProgressHUD;

						@Override
						public Dialog onCreateDialog(Bundle savedInstanceState) {

							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Cancel Reservation");

							builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {

									// Initiate Cancel Reservation									
									try {
										Nissan360.application.getNissanApiService().cancelReservation(reservationId);
									} catch (Exception e) {
										Toast.makeText(getBaseContext(), "Could not confirm your cancellation at this time", Toast.LENGTH_SHORT).show();
										e.printStackTrace();
									}
									
									finish();

								}
							});


							builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									CancelDriveDialogFragment.this.dismiss();
								}
							});

							builder.setNeutralButton("Reschedule", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									CancelDriveDialogFragment.this.dismiss();


									if(c.getCount() < 1){
										Toast.makeText(getBaseContext(), "Could not confirm your cancellation at this time", Toast.LENGTH_SHORT).show();
										return;
										
									}
									
									// Initiate Cancel Reservation
									try {
										Nissan360.application.getNissanApiService().cancelReservation(reservationId);
									} catch (Exception e) {
										Toast.makeText(getBaseContext(), "Could not confirm your cancellation at this time", Toast.LENGTH_SHORT).show();
										e.printStackTrace();
										return;
									}
									
									c.moveToPosition(0);
									String vin = c.getString( c.getColumnIndex(ReservationsContract.VEHICLE_ID_FIELD));
									
									Cursor vCursor = getContentResolver().query(
											Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, vin),
											VehiclesProvider.getDefaultProjection(),
											null, null, null);
									if(vCursor.getCount() < 1){
										Toast.makeText(getBaseContext(), "Could not reschedule this vehicle", Toast.LENGTH_SHORT).show();
										return;
									}
									vCursor.moveToPosition(0);
									VehiclesHelper.startExperienceActivityForVehicle(DriveReminder.this, vCursor);
									finish();
								}
							});

							AlertDialog dialog = builder.create();
							return dialog;
						}

						@Override
						public void onCancel() {
							mProgressHUD.dismiss();

						}
					}

					CancelDriveDialogFragment dialog = new CancelDriveDialogFragment();
					dialog.show(getSupportFragmentManager(), "dialog");		

				}

			});
		}
	}

	

}

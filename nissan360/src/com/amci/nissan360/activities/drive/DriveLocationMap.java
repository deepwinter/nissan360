package com.amci.nissan360.activities.drive;

import com.amci.nissan360.MapIntentContract;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.PoiMapFragment;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class DriveLocationMap extends Nissan360NavigationActivity {

	GoogleMap map;
	
	double latitude = 0;
	double longitude = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drive_location_map);
		
		Bundle extras = getIntent().getExtras();
		if(extras == null){
			Toast.makeText(this, "Coordinates not set", Toast.LENGTH_SHORT).show();
			finish();
		}
		latitude = extras.getDouble(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE);
		longitude = extras.getDouble(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE);
	
		if(latitude == 0 || longitude == 0){
			Toast.makeText(this, "Coordinates not set", Toast.LENGTH_SHORT).show();
			//finish();
			
			latitude = 37.809672;
			longitude = -122.296222; 
		}
	}


	@Override
	protected void onStart() {
		super.onStart();
		
		// Set up the map fragment
		DriveLocationMapFragment driveLocationMapFragment = new DriveLocationMapFragment();
		//View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
		
		driveLocationMapFragment.setCoordinates(latitude, longitude);
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction;
		transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.drive_location_map_container, driveLocationMapFragment);
		transaction.commit();
	}
}

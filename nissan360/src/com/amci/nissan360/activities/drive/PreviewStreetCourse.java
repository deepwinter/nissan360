package com.amci.nissan360.activities.drive;

import java.io.File;
import java.util.ArrayList;

import net.winterroot.hzsv.CarouselAdapter;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.ui.AddToQueueOnClickListener;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ViewFlipper;

public class PreviewStreetCourse extends DriveActivity {

	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;

	ViewFlipper flipper;
	
	
	protected void addViewToFlipper(View view){
		flipper.addView(view);
	}

	
	public static final String INTENT_ACTION = "com.amci.nissan360.action.PREVIEW_STREET_COURSE";
	private GestureDetector gestureDetector;
	private OnGestureListener simpleOnGestureListener;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	

		flipper = new ViewFlipper(this);

		ArrayList<View> previousButtons = Utilities.getViewsByTag(flipper, "previous_button");
		for(View view : previousButtons){
			ImageButton button = (ImageButton) view;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeRight();
				}

			});
		}

		ArrayList<View> nextButtons = Utilities.getViewsByTag(flipper, "next_button");
		for(View view : nextButtons){
			ImageButton button = (ImageButton) view;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeLeft();
				}

			});
		}


		animFlipInForeward = AnimationUtils.loadAnimation(this, R.anim.flipin);
		animFlipOutForeward = AnimationUtils.loadAnimation(this, R.anim.flipout);
		animFlipInBackward = AnimationUtils.loadAnimation(this, R.anim.flipin_reverse);
		animFlipOutBackward = AnimationUtils.loadAnimation(this, R.anim.flipout_reverse);

		flipper.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		});
		
		simpleOnGestureListener = new SimpleOnGestureListener(){

			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
					float velocityY) {

				float sensitvity = 50;
				if((e1.getX() - e2.getX()) > sensitvity){
					SwipeLeft();
				}else if((e2.getX() - e1.getX()) > sensitvity){
					SwipeRight();
				}

				return true;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return true;
			}
		};

		gestureDetector= new GestureDetector(this, simpleOnGestureListener);
		
		setContentView(flipper);
		
		LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View eastRuralDriveView = layoutInflater.inflate(R.layout.activity_preview_street_course, flipper);
		ImageView previewEastRuralDriveImageView = (ImageView) eastRuralDriveView.findViewWithTag("preview_image");
		Utilities.setImageBitmapNissan360(this, previewEastRuralDriveImageView, "drive/east_rural_preview.png");
		
		Button addToQueueButton = (Button) eastRuralDriveView.findViewWithTag("street_course_reserve_button");
		addToQueueButton.setOnClickListener( new AddToQueueOnClickListener(this, vehicle.VIN) );
		
		/*
		View westUrbanDriveView = layoutInflater.inflate(R.layout.activity_preview_street_course, flipper);
		ImageView previewWestUrbanDriveImageView = (ImageView) westUrbanDriveView.findViewWithTag("preview_image");
		Utilities.setImageBitmapNissan360(this, previewWestUrbanDriveImageView, "drive/west_urban_preview.png");
		
		addToQueueButton = (Button) westUrbanDriveView.findViewWithTag("street_course_reserve_button");
		addToQueueButton.setOnClickListener( new AddToQueueOnClickListener(this, vehicle.VIN) );
		*/
		
	}

	private void SwipeRight(){
		flipper.setInAnimation(animFlipInBackward);
		flipper.setOutAnimation(animFlipOutBackward);
		flipper.showPrevious();
	}

	private void SwipeLeft(){
		flipper.setInAnimation(animFlipInForeward);
		flipper.setOutAnimation(animFlipOutForeward);
		flipper.showNext();
	}


	
}




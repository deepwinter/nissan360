package com.amci.nissan360.activities.drive;

import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ReservationAddedMessage extends Nissan360NavigationActivity {

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reservation_added_message);
		
		TextView messageText = (TextView) findViewById(R.id.reservation_added_message_view);
		messageText.setText("Your reservation has been successfully, please check your agenda for scheduling information.");
		
		Button okButton = (Button) findViewById(R.id.reservation_added_ok_button);
		okButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				finish();
			}
			
		});
	}

}

package com.amci.nissan360.activities.drive;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.agenda.Agenda;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.activities.vehicleselector.VehicleStoryActivity;
import com.amci.nissan360.api.ApiError;
import com.amci.nissan360.api.reservation.ReservationCheckinRequest;
import com.amci.nissan360.api.reservation.ReservationIdentity;
import com.amci.nissan360.api.reservation.ReservationInfo;
import com.amci.nissan360.api.reservation.ReservationOptions;
import com.amci.nissan360.database.ReservationsContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.ReservationsProvider;
import com.amci.nissan360.provider.ReservationsQueueProvider;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.services.RefreshReservationsListener;
import com.amci.nissan360.state.ApplicationState;
import com.amci.nissan360.state.ReservationState;
import com.amci.nissan360.ui.CourseSelectionArrayAdapter;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/*
 *  Just accepts an intent for a vehicle, and redirects to correct experience page
 */

public class VehicleExperienceController extends VehicleStoryActivity {

	public static String ATTEMPT_FORCE_START_DRIVE = "ATTEMPT_FORCE_START_DRIVE";
	private String vin;


	private static final int NOTHING_SELECTED = 9999;
	private static final String BUNDLE_VIN_KEY = "BUNDLE_VIN_KEY";
	int selectedIndex;

	protected boolean loadStoryViewOnCreate = false;
	protected boolean forceStart = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vehicle_experience_controller);

		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			vin = extras.getString(VehicleIntentContract.INTENT_EXTRA_VIN);
		}
		if(vin == null && savedInstanceState != null){
			// No VIN in extras.  So check the bundle
			vin = savedInstanceState.getString(BUNDLE_VIN_KEY);
		}

		if(vin==null){
			ApplicationState.leaveDriveMode(this);
			vehicleNotFound();
			return;
		}
		loadStoryView(vin);

		// Look up Vehicle
		String[] projection = {
				VehiclesContract.VIN_FIELD,
				VehiclesContract.TRACK_FIELD,
				VehiclesContract.BUCKET_FIELD
		};
		Cursor c = getContentResolver().query( Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, vin), projection, null, null, null);
		if(c.getCount() == 0){
			ApplicationState.leaveDriveMode(this);
			vehicleNotFound();
			return;
		}

		if(extras.getBoolean(ATTEMPT_FORCE_START_DRIVE, false)){
			forceStart = true;
		}

		// Check if this vehicle is NOT a street drive vehicle
		c.moveToPosition(0);
		String track = c.getString(c.getColumnIndex(VehiclesContract.TRACK_FIELD));
		if(! VehiclesContract.TRACK_TYPES_STREET.contains(track)){
			// If not, just show the correct experience page for this vehicle's track situation
			c.moveToPosition(0);
			Intent i = new Intent(VehicleStoryActivity.INTENT_ACTION);
			i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
			startActivity(i);
			finish();
		}

		// This is a street drivable vehicle
		// Check to see if this is a check out (we are in drive mode, moving back to not driving mode)
		if(ApplicationState.isInDriveMode(this)
				|| ApplicationState.isCheckedIn(this)
				){
			// it's a check out, so check them out
			String attendeeId = Nissan360.getLoggedInUser().LoginId;
			ReservationIdentity reservation = new ReservationIdentity(attendeeId, 
					ReservationState.getVin(this),
					ReservationState.getReservationId(this)
					);

			try {
				Nissan360.application.getNissanApiService().sendDriveCompleted(reservation);
			} catch (Exception e) {
				Toast.makeText(this, "Failed to send drive completed", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}


			ApplicationState.leaveDriveMode(this);
			finish();
			return;
		}



	}



	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(vin!=null){
			outState.putString(BUNDLE_VIN_KEY, vin);
		}
	}



	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(vin == null){
			vin = savedInstanceState.getString(BUNDLE_VIN_KEY);
		}
	}



	public void onStart(){
		super.onStart();

		// Otherwise Refresh reservations
		// Check if user has registration for vehicle
		// If yes, check if user can check in
		// If yes, check the user in

		if(!isFinishing()){
			try {
				Nissan360.application.getNissanApiService().refreshReservations(new RefreshReservationsListener(){

					@Override
					public void refreshSucceeded() {
						checkForRegistration();
					}

					@Override
					public void refreshFailed() {
						checkForRegistration();

					}

				});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}



	protected void checkForRegistration() {
		String[] selectionArgs = { vin };
		String [] projection = ReservationsProvider.getDefaultProjection();
		Cursor c = getContentResolver().query(ReservationsProvider.CONTENT_URI,
				projection,
				"VehicleId = ?", selectionArgs, null);
		if(c.getCount() < 1){
			reservationNotFound();
		} else {
			c.moveToPosition(0);
			reservationFound(c.getString(c.getColumnIndex(ReservationsContract.RESERVATION_ID_FIELD)),
					c.getString(c.getColumnIndex(ReservationsContract.TRACK_FIELD))	
					);
		}

	}

	private void reservationFound(String reservationId, final String course) {
		ReservationIdentity reservation = new ReservationIdentity(Nissan360.getLoggedInUser().LoginId, vin, Integer.parseInt(reservationId) );
		class ReservationCheckinRequestListener implements RequestListener<ReservationInfo>{

			@Override
			public void onRequestFailure(SpiceException arg0) {
				ConnectionErrorDialogFragment dialogFragment = new ConnectionErrorDialogFragment("Error confirming with server.  Please see an ambassador");
				dialogFragment.show(VehicleExperienceController.this.getSupportFragmentManager(), "dialog");					
			}

			@Override
			public void onRequestSuccess(ReservationInfo reservation) {


				// Checked in and cleared to begin the drive
				String routeFile = null; // Default?
				if(course.equals(VehiclesContract.TRACK_TYPE_STREET_URBAN)
						|| course.equals(VehiclesContract.TRACK_TYPE_STREET_TEST)){
					routeFile = VehiclesContract.GPX_FILE_WEST_URBAN;
				} else if (course.equals(VehiclesContract.TRACK_TYPE_STREET_RURAL)){
					routeFile = VehiclesContract.GPX_FILE_EAST_RURAL; 
				}

				if(routeFile != null) {
					ReservationState.setActiveReservation(VehicleExperienceController.this, vin, reservation.ReservationId);
					ApplicationState.moveToCheckedInMode(VehicleExperienceController.this);
					Intent i = new Intent(VehicleStoryActivity.INTENT_ACTION);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
					i.putExtra(VehicleIntentContract.INTENT_EXTRA_TRACK, routeFile);
					startActivity(i);
				} else {
					Toast.makeText(VehicleExperienceController.this, "Routing file not found!  Please see an ambassador", Toast.LENGTH_SHORT).show();
				}

			}

		}
		spiceManager.execute( new ReservationCheckinRequest(reservation), null, DurationInMillis.NEVER, new ReservationCheckinRequestListener() );				

	}


	AlertDialog aDialog;

	private void reservationNotFound() {


		final Dialog dialog = new Dialog(VehicleExperienceController.this);
		dialog.setContentView(R.layout.view_add_to_queue_dialog);
		dialog.setTitle("Please Select a Course");

		ListView coursesListView = (ListView) dialog.findViewById(R.id.queue_choose_course_list);
		final CourseSelectionArrayAdapter<String> coursesAdapter = new CourseSelectionArrayAdapter<String>(VehicleExperienceController.this, android.R.layout.simple_list_item_1);
		coursesAdapter.add(VehiclesContract.TRACK_TYPES_STREET.get(0));
		coursesAdapter.add(VehiclesContract.TRACK_TYPES_STREET.get(1));
		coursesListView.setAdapter(coursesAdapter);
		coursesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
				view.setSelected(true);
				selectedIndex = position;
			}

		});

		// Add the buttons
		Button yesButton = (Button) dialog.findViewById(R.id.queue_yes_button);
		yesButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if(selectedIndex == NOTHING_SELECTED){
					Toast.makeText(VehicleExperienceController.this, "You must select a course", Toast.LENGTH_LONG).show();
					return;
				}

				dialog.dismiss();

				final String track = VehiclesContract.TRACK_TYPES_STREET.get(selectedIndex);
				ReservationIdentity reservation = new ReservationIdentity(Nissan360.getLoggedInUser().LoginId, vin, 0, track );
				if(forceStart){
					reservation.Override = true;
				}
				class ReservationCheckinRequestListener implements RequestListener<ReservationInfo>{

					@Override
					public void onRequestFailure(SpiceException exception) {

						if(exception instanceof NoNetworkException){
							// Handle as a network problem

							ConnectionErrorDialogFragment dialogFragment = new ConnectionErrorDialogFragment(
									"The network was not available.  Your request has been logged and will be uploaded as soon as possible");
							dialogFragment.show(VehicleExperienceController.this.getSupportFragmentManager(), "dialog");	


							// And add to the reservations queue
							ReservationOptions reservationOptions = new ReservationOptions(Nissan360.getLoggedInUser().LoginId, vin, track);
							VehicleExperienceController.this.getContentResolver().insert(ReservationsQueueProvider.CONTENT_URI, reservationOptions.getContentValues());


						} else if (exception.getCause() instanceof HttpClientErrorException) {
							// I wonder if it's crashing in here, like the error exception is missing stuff or null

							// TODO: refactor as function
							HttpClientErrorException clientException = (HttpClientErrorException)exception.getCause();
							if (clientException.getStatusCode().equals(HttpStatus.BAD_REQUEST) ) 
							{
								String body = clientException.getResponseBodyAsString();
								try {
									ApiError error = Nissan360.objectMapper.readValue(body, ApiError.class);

									String dialogMessage = error.getErrorText();

									ConnectionErrorDialogFragment dialogFragment = new ConnectionErrorDialogFragment(
											dialogMessage);
									dialogFragment.show(VehicleExperienceController.this.getSupportFragmentManager(), "dialog");	

								} catch (JsonParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (JsonMappingException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

							}

						} else {

							String dialogMessage = "There was a problem with the request.  Please see an ambassador. ";

							String message = exception.getMessage();
							if(exception.getCause() instanceof HttpClientErrorException)
							{
								HttpClientErrorException e = (HttpClientErrorException)exception.getCause();
								String response = e.getResponseBodyAsString();
								dialogMessage += response;
							} else {
								dialogMessage += message;
							}

							ConnectionErrorDialogFragment dialogFragment = new ConnectionErrorDialogFragment(dialogMessage);
							dialogFragment.show(VehicleExperienceController.this.getSupportFragmentManager(), "dialog");	

						}




					}

					@Override
					public void onRequestSuccess(final ReservationInfo reservation) {

						if(reservation == null){
							Toast.makeText(VehicleExperienceController.this, "No Reservation", Toast.LENGTH_SHORT).show();
							return;
						}

						if(reservation.ResponseCode == null){
							Toast.makeText(VehicleExperienceController.this, "No Response Code From Server", Toast.LENGTH_SHORT).show();
							return;
						}

						if(reservation.ResponseCode.equals(ReservationInfo.RESERVATION_CREATED_AND_QUEUED)){
							// The user was added to the queue rather than checked in

							AlertDialog.Builder builder = new AlertDialog.Builder(VehicleExperienceController.this);
							builder.setMessage("This vehicle is not currently available.  Your reservation has been added to the queue").setTitle("Reservation Queued.");

							// Add the buttons
							builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									if(aDialog!=null){
										aDialog.dismiss();
									}
									Intent i = new Intent(VehicleExperienceController.this, Agenda.class);
									i.putExtra(Agenda.INTENT_EXTRA_CURRENT_TAB, Agenda.INTENT_EXTRA_QUEUE_TAB);
									startActivity(i);
									finish();
								}
							});
							aDialog = builder.create();
							aDialog.show();

						} else if (reservation.ResponseCode.equals(ReservationInfo.CHECKIN_SUCCESS)) {
							// They are approved to begin the drive

							String routeFile = null; // Default?
							if(VehiclesContract.TRACK_TYPES_STREET.get(selectedIndex).equals(VehiclesContract.TRACK_TYPE_STREET_URBAN)
									|| VehiclesContract.TRACK_TYPES_STREET.get(selectedIndex).equals(VehiclesContract.TRACK_TYPE_STREET_TEST)){
								routeFile = VehiclesContract.GPX_FILE_WEST_URBAN;
							} else if (VehiclesContract.TRACK_TYPES_STREET.get(selectedIndex).equals(VehiclesContract.TRACK_TYPE_STREET_RURAL)){
								routeFile = VehiclesContract.GPX_FILE_EAST_RURAL; 
							}

							if(routeFile != null) {
								ApplicationState.moveToCheckedInMode(VehicleExperienceController.this);
								ReservationState.setActiveReservation(VehicleExperienceController.this, vin, reservation.ReservationId);
								Intent i = new Intent(VehicleStoryActivity.INTENT_ACTION);
								i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
								i.putExtra(VehicleIntentContract.INTENT_EXTRA_TRACK, routeFile );
								startActivity(i);
								finish();
							} else {
								Toast.makeText(VehicleExperienceController.this, "Routing file not found!  Please see an ambassador", Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(VehicleExperienceController.this, "Invalid Response Code From Server", Toast.LENGTH_SHORT).show();
							return;
						}



					}

				}
				spiceManager.execute( new ReservationCheckinRequest(reservation), null, DurationInMillis.NEVER, new ReservationCheckinRequestListener() );				

			}
		});

		Button noButton = (Button) dialog.findViewById(R.id.queue_no_button);
		noButton.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		// Try to avoid a crash here.
		if(!isFinishing()){
			dialog.show();		
		}

	}

	private void vehicleNotFound(){
		Toast.makeText(VehicleExperienceController.this, "Sorry, vehicle not found.", Toast.LENGTH_SHORT).show();
		finish();
	}


	class ConnectionErrorDialogFragment extends DialogFragment {

		String message;

		public ConnectionErrorDialogFragment(String message2) {
			super();
			message = message2;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(message).setTitle("Reservation Lookup Failed.");

			// Add the buttons
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					ConnectionErrorDialogFragment.this.dismiss();
				}
			});



			AlertDialog dialog = builder.create();
			return dialog;
		}
	}

}

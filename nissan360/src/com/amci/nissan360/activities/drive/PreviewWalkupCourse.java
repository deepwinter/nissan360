package com.amci.nissan360.activities.drive;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.ui.SelectReservationOnClickListener;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.ImageView;

public class PreviewWalkupCourse extends DriveActivity {

	public static final String INTENT_ACTION = "com.amci.nissan360.action.PREVIEW_WALKUP_COURSE";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preview_walkup_course);

		Bundle extras = getIntent().getExtras();
		if(extras != null){
			String track = extras.getString(VehicleIntentContract.INTENT_EXTRA_TRACK);
			String imageFile = "missing";
			if(track != null){
				if(track.equals( VehiclesContract.TRACK_TYPE_PERFORMANCE )){
					imageFile = "drive/performance_preview.png";

				} else if (track.equals( VehiclesContract.TRACK_TYPE_WORLD_DRIVE )){ // working!
					imageFile = "drive/world_preview.png";

				} else if (track.equals( VehiclesContract.TRACK_TYPE_ADVENTURE)){
					imageFile = "drive/adventure_preview.png";

				} else if (track.equals(VehiclesContract.TRACK_TYPE_ALEAF_DRIVE)) { // missing from device
					imageFile = "drive/aleaf_preview.png";

				} else if (track.equals(VehiclesContract.TRACK_TYPE_LCV_DRIVE)){ // working!!
					imageFile = "drive/lcv_preview.png";

				}
			}

			if(imageFile != null){
				ImageView iv = (ImageView) findViewById(R.id.walkup_course_image);
				Utilities.setImageBitmapNissan360(this, iv, imageFile);
			}

		}

	}

}

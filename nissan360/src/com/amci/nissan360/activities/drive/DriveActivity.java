package com.amci.nissan360.activities.drive;

import java.util.Collection;
import java.util.Iterator;

import org.json.JSONException;

import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.api.Vehicle;
import com.amci.nissan360.api.configuration.TrackDescriptions;
import com.amci.nissan360.provider.VehiclesProvider;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class DriveActivity extends Nissan360NavigationActivity {
	
	public static final String TRACK_DESCRIPTION_PERFORMANCE_TYPE = "performance_drive";
	public static final String TRACK_DESCRIPTION_STREET_TYPE = "street_drive";
	public static final String TRACK_DESCRIPTION_HIGH_PERFORMANCE_TYPE = "high_performance_drive";
	public static final String TRACK_DESCRIPTION_WORLD_TYPE = "world_drive";
	public static final String TRACK_DESCRIPTION_ADVENTURE_TYPE = "adventure_drive";
	public static final String TRACK_DESCRIPTION_ALEAF_TYPE = "aleaf_drive";
	public static final String TRACK_DESCRIPTION_LCV_TYPE = "lcv_drive";

	protected Vehicle vehicle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if(extras == null){
			Log.e("DRIVE", "No Extras found!");
			return;
		}
		String vin = extras.getString(VehicleIntentContract.INTENT_EXTRA_VIN);

		if(vin == null){
			Log.e("DRIVE", "No VIN Extra found!");
			return;
		}

		Cursor vehicles = getContentResolver().query(Uri.withAppendedPath( VehiclesProvider.CONTENT_URI_BY_VIN, vin), 
				VehiclesProvider.getDefaultProjection(), null, null, null);
		if(vehicles.getCount() < 1){
			Log.e("DRIVE", "No vehicle for VIN");
			return;
		}
		vehicles.moveToPosition(0);
		vehicle = new Vehicle(vehicles);

	}

	public String getTrackDescription(String key){
		try {
			JSONDatabase configDatabase = Nissan360.getConfigDatabase();
			SearchFields search = SearchFields.Where("Type", TrackDescriptions.TRACK_DESCRIPTIONS_TYPE);
			Collection<JSONEntity> entities =  configDatabase.fetchByFields(search);
			Iterator<JSONEntity> i = entities.iterator();
			if(i.hasNext()){
				JSONEntity e = i.next();
				String trackDescription = e.getString(key);
				if(trackDescription != null){
					return trackDescription.replace("\\n", System.getProperty("line.separator"));
				}
			}

		} catch (InitJSONDatabaseExcepiton e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}

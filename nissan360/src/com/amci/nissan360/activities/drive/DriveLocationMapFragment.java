package com.amci.nissan360.activities.drive;

import com.amci.nissan360.map.OfflineDirectoryMapTileProvider;
import com.amci.nissan360.services.NissanApiService;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;

public class DriveLocationMapFragment extends SupportMapFragment {

	GoogleMap map;
	private double latitude;
	private double longitude;
	
	
	public void setCoordinates(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public void onStart() {
		super.onStart();

		//markers = new ArrayList<Marker>();
		
		map = getMap();
		if(map == null){
			return;
		}
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true);
		
		//LatLng latLng = new LatLng(NissanSettings.latitude,NissanSettings.longitude);
		//map.moveCamera( CameraUpdateFactory.newLatLngZoom(latLng, 16) );

		TileOverlayOptions tp  = new TileOverlayOptions().tileProvider(
				new OfflineDirectoryMapTileProvider(
						getActivity().getApplicationContext().getFilesDir().getPath() + "/nissan360/maps/event_site"));		map.addTileOverlay(tp);
		
		MarkerOptions options = new MarkerOptions();
		options.title("Drive Location");
		options.snippet("Check in for your drive here");
		LatLng latLng = new LatLng(latitude,longitude);
		options.position(latLng);
		Marker m = map.addMarker(options);
		m.showInfoWindow();

		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(NissanApiService.getLatitude(), NissanApiService.getLongitude()), 16);
		map.animateCamera(cameraUpdate);
	}

}

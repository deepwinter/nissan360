package com.amci.nissan360.activities.drive;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.ui.SelectReservationOnClickListener;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.ImageView;

public class PreviewHighPerformanceCourse extends DriveActivity {

	public static final String INTENT_ACTION = "com.amci.nissan360.action.PREVIEW_PERFORMANCE_COURSE";
	private Button reserveButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preview_performance_course);
		
		reserveButton = (Button) findViewById(R.id.performance_course_reserve_button);
		reserveButton.setOnClickListener( new SelectReservationOnClickListener(this, vehicle.VIN) );
		
		ImageView iv = (ImageView) findViewById(R.id.performance_course_image);
		Utilities.setImageBitmapNissan360(this, iv, "drive/high_performance_preview.png");

	}

}

package com.amci.nissan360.activities.drive;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.state.ReservationState;

public class CheckOut extends DriveActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_check_out);
		
		ImageView vehicleImage = (ImageView) findViewById(R.id.check_out_vehicle_image);
		ImageView vehicleSpecSheetImage = (ImageView) findViewById(R.id.check_out_spec_sheet_image);
		
		String vin = ReservationState.getVin(this);
		Cursor c = getContentResolver().query(Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, vin), VehiclesProvider.getDefaultProjection(), null, null, null);
	
		if(c.getCount() < 1){
			return;
		}
		c.moveToPosition(0);
		Utilities.setImageBitmapNissan360(this, vehicleImage, "vehicles/"+c.getString(c.getColumnIndex(VehiclesContract.HERO_IMAGE_FIELD)));
		Utilities.setImageBitmapNissan360(this, vehicleSpecSheetImage, "vehicles/"+c.getString(c.getColumnIndex(VehiclesContract.SPEC_SHEET_FIELD)));

	}

	

}

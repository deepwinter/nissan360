package com.amci.nissan360.activities.drive;

import roboguice.util.temp.Ln;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.base.Nissan360Activity;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.api.reservation.PerformanceDriveReservationOptions;
import com.amci.nissan360.api.reservation.PerformanceDriveReservationRequest;
import com.amci.nissan360.api.reservation.PerformanceDriveReservationResponse;
import com.amci.nissan360.api.reservation.ReservationOptions;
import com.amci.nissan360.api.reservation.StreetDriveQueueRequest;
import com.amci.nissan360.api.reservation.StreetDriveQueueResponse;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import com.amci.nissan360.ui.AddToQueueOnClickListener;

public class VehicleDriveStreet extends DriveActivity {

	public static final String INTENT_ACTION = "com.amci.nissan360.action.VEHICLE_DRIVE_STREET";
	Button addToQueueButton;
	Button courseMapButton;
	TextView mModelTextView;
	TextView mTrackTextView;
	ImageView heroImageView;
	
	public VehicleDriveStreet() {
		super();
		
		Ln.getConfig().setLoggingLevel(Log.VERBOSE);
		

	}

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_vehicle_drive_street);
		
		mModelTextView = (TextView) findViewById(R.id.street_vehicle_model_text);
		mTrackTextView = (TextView) findViewById(R.id.street_track_description_view);
		
		mModelTextView.setText(vehicle.Model);
		mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_STREET_TYPE));
		
		heroImageView = (ImageView) findViewById(R.id.street_vehicle_image);
		
		boolean rval;
		rval = Utilities.setImageBitmapNissan360(this, heroImageView, "vehicles/"+vehicle.HeroImage);
		if(!rval){
			Utilities.setImageBitmapNissan360(this, heroImageView, "vehicles/notavail_hero.png");
		}
		
		addToQueueButton = (Button) findViewById(R.id.add_to_queue_button);
		addToQueueButton.setOnClickListener( new AddToQueueOnClickListener(this, vehicle.VIN) );

		courseMapButton = (Button) findViewById(R.id.course_map_button);
		courseMapButton.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.PREVIEW_STREET_COURSE");
				i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vehicle.VIN);
				startActivity(i);

			}

		});

	}


	

}

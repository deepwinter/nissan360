package com.amci.nissan360.activities.drive;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.activities.drive.VehicleExperienceController.ConnectionErrorDialogFragment;
import com.amci.nissan360.api.EmptyResponse;
import com.amci.nissan360.api.reservation.ReservationCheckinRequest;
import com.amci.nissan360.api.reservation.ReservationIdentity;
import com.amci.nissan360.api.reservation.ReservationInfo;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.database.ReservationsContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.AssetsProvider;
import com.amci.nissan360.provider.ReservationsProvider;
import com.amci.nissan360.services.RefreshReservationsListener;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v4.app.DialogFragment;
import android.view.Menu;
import android.widget.Toast;

public class HighPerformanceTrackCheckinController extends Nissan360NavigationActivity {

	public static final String INTENT_EXTRA_FORCE_CHECKIN = "FORCE_CHECKIN";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_performance_track_checkin_controller);
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			boolean force = extras.getBoolean(INTENT_EXTRA_FORCE_CHECKIN, false);
			if(force){
				// Force the login
				attemptPerformanceTrackCheckin(true);
				return;
			}
		}
		
		// If they already have done a performance track checkin, just issue the completion message.
		Cursor c = getContentResolver().query(AssetsProvider.ASSETS_CONTENT_URI, 
				AssetsProvider.getDefaultProjection(), AssetsContract.TYPE_FIELD + "= '"+AssetsContract.PERFORMANCE_TRACK_SHUTTLE_TYPE+"'", null, null);
		if(c.getCount() > 0){
			performanceTrackThankYou();
			return;
		}
		

		// Refresh reservations and then look for a reservation
		try {
			Nissan360.application.getNissanApiService().refreshReservations(new RefreshReservationsListener(){

				@Override
				public void refreshSucceeded() {
					attemptPerformanceTrackCheckin();				
				}

				@Override
				public void refreshFailed() {
					attemptPerformanceTrackCheckin();				
				}

			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this, "Failed to user Nissan API Service", Toast.LENGTH_SHORT).show();
			Nissan360.application.doBindService();
		}
		
	}

	
	private void performanceTrackThankYou() {
		SimpleFinishingDialogFragment dialog = new SimpleFinishingDialogFragment("Thanks", "Thank you for testing out our vehicles on the performance track");
		dialog.show(HighPerformanceTrackCheckinController.this.getSupportFragmentManager(), "dialog");	
	}

	private void attemptPerformanceTrackCheckin(){
		attemptPerformanceTrackCheckin(false);
	}

	private  void attemptPerformanceTrackCheckin(boolean force){
		
		String attendeeId = Nissan360.getLoggedInUser().LoginId;
		String vin = "";
		int reservationId = 0;
		if(!force){
			// Look for a reservation for performance track
			Cursor c = getContentResolver().query(ReservationsProvider.CONTENT_URI, ReservationsProvider.getDefaultProjection(), 
					ReservationsContract.TRACK_FIELD + "= '"+ VehiclesContract.TRACK_TYPE_HIGH_PERFORMANCE + "'", null, null);
			if(c == null || c.getCount() < 1){
				performanceCheckinFailed();
			}

			// We found a reservation, so attempt a checkinString attendeeId, String vehicleId, int reservationId)
			c.moveToPosition(0);
			vin = c.getString(c.getColumnIndex(ReservationsContract.VEHICLE_ID_FIELD));
			reservationId = c.getInt(c.getColumnIndex(ReservationsContract.RESERVATION_ID_FIELD));
		} 
		
		ReservationIdentity reservation = new ReservationIdentity(attendeeId, vin, reservationId);
		if(force){
			reservation.Override = true;
		}

		class ReservationCheckinRequestListener implements RequestListener<ReservationInfo>{

			@Override
			public void onRequestFailure(SpiceException arg0) {
				performanceCheckinFailed();	
			}

			@Override
			public void onRequestSuccess(ReservationInfo arg0) {
				performanceCheckinSuccess();

			}

		}
		spiceManager.execute( new ReservationCheckinRequest(reservation), null, DurationInMillis.NEVER, new ReservationCheckinRequestListener() );				

	}

	public void performanceCheckinSuccess() {
		try {
			// Log the asset tap now.
			Nissan360.application.getNissanApiService().logAssetTap(AssetsContract.PERFORMANCE_TRACK_SHUTTLE_TYPE, AssetsContract.PERFORMANCE_TRACK_SHUTTLE_TYPE, null, AssetsLogContract.INTERACTION_TYPE_TAP);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		// Checked in and cleared to get on the shuttle
		ClearedForShuttleDialogFragment dialogFragment = new ClearedForShuttleDialogFragment("Your reservation was confirmed with the server and you are cleared to board the shuttle.");
		dialogFragment.show(HighPerformanceTrackCheckinController.this.getSupportFragmentManager(), "dialog");	
			
	}


	private void performanceCheckinFailed(){
		ConnectionErrorDialogFragment dialogFragment = new ConnectionErrorDialogFragment("Could not confirm your reservation with the server.  Please see an ambassador");
		dialogFragment.show(HighPerformanceTrackCheckinController.this.getSupportFragmentManager(), "dialog");			
	}
	
	class ConnectionErrorDialogFragment extends DialogFragment {

		String message;

		public ConnectionErrorDialogFragment(String message2) {
			super();
			message = message2;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(message).setTitle("Reservation Not Found.");

			// Add the buttons
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					ConnectionErrorDialogFragment.this.dismiss();
					finish();
				}
			});



			AlertDialog dialog = builder.create();
			return dialog;
		}
	}
	
	class ClearedForShuttleDialogFragment extends DialogFragment {

		String message;

		public ClearedForShuttleDialogFragment(String message2) {
			super();
			message = message2;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(message).setTitle("Checkin Successful.");

			// Add the buttons
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					ClearedForShuttleDialogFragment.this.dismiss();
					finish();
				}
			});



			AlertDialog dialog = builder.create();
			return dialog;
		}
	}
	
	
	class SimpleFinishingDialogFragment extends DialogFragment {

		String title;
		String message;

		public SimpleFinishingDialogFragment(String title2, String message2) {
			super();
			title = title2;
			message = message2;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(message).setTitle(title);

			// Add the buttons
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

					SimpleFinishingDialogFragment.this.dismiss();
					finish();
				}
			});



			AlertDialog dialog = builder.create();
			return dialog;
		}
	}
}

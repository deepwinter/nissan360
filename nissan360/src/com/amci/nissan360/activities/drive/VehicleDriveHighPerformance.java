package com.amci.nissan360.activities.drive;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.ui.SelectReservationOnClickListener;


public class VehicleDriveHighPerformance extends DriveActivity {

	public static final String INTENT_ACTION = "com.amci.nissan360.action.VEHICLE_DRIVE_PERFORMANCE";
	Button reserveButton;
	Button courseMapButton;
	TextView mModelTextView;
	TextView mTrackTextView;
	ImageView mVehicleImage;
	
	
	@Override
	public void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		setContentView(R.layout.activity_vehicle_drive_performance);

		mVehicleImage = (ImageView) findViewById(R.id.performance_vehicle_drive_image);
		mModelTextView = (TextView) findViewById(R.id.performance_track_vehicle_name);
		mTrackTextView = (TextView) findViewById(R.id.performance_track_description_view);
		
		boolean rval;
		rval = Utilities.setImageBitmapNissan360(this, mVehicleImage, "vehicles/"+vehicle.HeroImage);
		if(!rval){
			Utilities.setImageBitmapNissan360(this, mVehicleImage, "vehicles/notavail_hero.png");
		}
		
		
		
		mModelTextView.setText(vehicle.Model);
		mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_HIGH_PERFORMANCE_TYPE));

		reserveButton = (Button) findViewById(R.id.reserve_button);
		reserveButton.setOnClickListener( new SelectReservationOnClickListener(this, vehicle.VIN) );

		courseMapButton = (Button) findViewById(R.id.course_map_button);
		courseMapButton.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.PREVIEW_PERFORMANCE_COURSE");
				i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vehicle.VIN);
				startActivity(i);
				
			}
			
		});
		
	

	}


	
}

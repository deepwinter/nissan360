package com.amci.nissan360.activities.drive;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;

import com.actionbarsherlock.view.MenuItem;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.PhoneNumbers;
import com.amci.nissan360.activities.agenda.Agenda;
import com.amci.nissan360.activities.nfc.TagDiscovered;
import com.amci.nissan360.api.configuration.Contact;
import com.amci.nissan360.api.configuration.ContactList;
import com.amci.nissan360.state.ApplicationState;

import net.osmand.plus.activities.MapActivity;
import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;

public class TurnByTurn extends MapActivity {

	PendingIntent pendingIntent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(true);

		actionBar.setHomeButtonEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater mi= getMenuInflater();
		mi.inflate(R.menu.turn_by_turn, menu);

		return true;
	}

	protected void onResume() {
		super.onResume();

		pendingIntent = PendingIntent.getActivity(
				this, 0, new Intent(this, TagDiscovered.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP ), 0);
		IntentFilter tagDiscoveredFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter intentFiltersArray[] = new IntentFilter[] {tagDiscoveredFilter };

		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, intentFiltersArray, null);
		}

	}

	public void onPause() {
		super.onPause();
		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).disableForegroundDispatch(this);
		}
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK){
			if(flag && flag2){
				ApplicationState.leaveDriveMode(this);
				finish();
			}
		} else {
			return super.onKeyDown(keyCode, event);
		}

		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU ) {
			flag = true;
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_BACK){
			flag2 = true;
			return true;
		}
		return false;
	}

	boolean flag = false;
	boolean flag2 = false;
	boolean flag3 = false;
	int unlock = 0;

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_phone:
			if(!(flag && flag2 && flag3)){
				
				JSONDatabase configDatabase;
				try {
					configDatabase = Nissan360.getConfigDatabase();
				} catch (InitJSONDatabaseExcepiton e1) {
					e1.printStackTrace();
					return true;
				}
				SearchFields search = SearchFields.Where("Type", ContactList.CONTACTS_TYPE);
				Collection<JSONEntity> entities =  configDatabase.fetchByFields(search);
				Iterator<JSONEntity> i = entities.iterator();
				if(!i.hasNext()){
					return true;
				}
				JSONEntity contactListEntity = i.next();
				ContactList contactList;
				try {
					contactList = (ContactList) contactListEntity.asClass(ContactList.class);

					for(int j=0; j<contactList.contacts.size(); j++){
							Contact contact;
							contact = contactList.contacts.get(j);
							if(contact.name.equals(Contact.STREET_DRIVE_HELP_TYPE)){
								Intent intent = new Intent(Intent.ACTION_CALL);
								intent.setData(Uri.parse("tel:" + contact.phone_number));
								startActivity(intent);	
								return true;
							}
												
					}
					
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} else {


				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Exit?").setMessage("Are you sure you want to exit driving navigation?");

				// Add the buttons
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						ApplicationState.leaveDriveMode(TurnByTurn.this);
						finish();

					}
				});

				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						flag = false;
						flag2 = false;
						flag3 = false;

					}
				});


				AlertDialog dialog = builder.create();
				dialog.show();

			}
			return true;
		case android.R.id.home:
			if(flag & flag2){
				unlock++;
			}
			if(unlock > 4){
				flag3 = true;
			}
			return true;
		default:
			return false;
		}
	}




}

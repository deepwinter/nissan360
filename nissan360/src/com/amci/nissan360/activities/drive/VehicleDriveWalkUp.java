package com.amci.nissan360.activities.drive;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amci.nissan360.MapIntentContract;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.PointsOfInterestProvider;
import com.amci.nissan360.ui.SelectReservationOnClickListener;


public class VehicleDriveWalkUp extends DriveActivity {

	public static final String INTENT_ACTION = "com.amci.nissan360.action.VEHICLE_DRIVE_WALKUP";

	Button driveLocationButton;
	Button courseMapButton;
	TextView mModelTextView;
	TextView mTrackTextView;
	TextView mTrackNameView;
	ImageView vehicleImage;

	@Override
	public void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		setContentView(R.layout.activity_vehicle_drive_walkup);

		vehicleImage = (ImageView) findViewById(R.id.walkup_vehicle_track_image);
		
		boolean rval;
		rval = Utilities.setImageBitmapNissan360(this, vehicleImage, "vehicles/"+vehicle.HeroImage);
		if(!rval){
			Utilities.setImageBitmapNissan360(this, vehicleImage, "vehicles/notavail_hero.png");
		}
		

		driveLocationButton = (Button) findViewById(R.id.drive_location_button);
		driveLocationButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// Look up the drive location

				String subtype = "";
				if(vehicle.Track.equals( VehiclesContract.TRACK_TYPE_PERFORMANCE )){
					subtype = "Performance";

				} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_WORLD_DRIVE)){
					subtype = "World";

				} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_ADVENTURE)){
					subtype = "Adventure";

				} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_ALEAF_DRIVE)) {
					subtype = "ALEAF";

				} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_LCV_DRIVE )){
					subtype = "LCV";
				}


				Cursor c = getContentResolver().query(PointsOfInterestProvider.CONTENT_URI_GEO_ASSETS, PointsOfInterestProvider.getGeoAssetsProjection(),
						"subtype = '"+subtype+"'", null, null);
				if(c.getCount() < 1){
					return;
				}
				c.moveToFirst();
				double latitude = c.getDouble(c.getColumnIndex(AssetsContract.LATITUDE_FIELD));
				double longitude = c.getDouble(c.getColumnIndex(AssetsContract.LONGITUDE_FIELD));
				

				if(latitude == 0 || longitude == 0){
					Toast.makeText(VehicleDriveWalkUp.this, "Coordinates not found", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent i = new Intent(VehicleDriveWalkUp.this, DriveLocationMap.class);
				i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE, latitude);
				i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE, longitude);
				startActivity(i);
			}

		});

		mModelTextView = (TextView) findViewById(R.id.walkup_track_vehicle_name);
		mModelTextView.setText(vehicle.Model);
		mTrackTextView = (TextView) findViewById(R.id.walkup_track_description_view);
		mTrackNameView = (TextView) findViewById(R.id.walkup_track_name);
		if(vehicle.Track.equals( VehiclesContract.TRACK_TYPE_PERFORMANCE )){
			mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_PERFORMANCE_TYPE));
			mTrackNameView.setText(VehiclesContract.TRACK_TYPE_PERFORMANCE);

		} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_WORLD_DRIVE)){
			mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_WORLD_TYPE));
			mTrackNameView.setText(VehiclesContract.TRACK_TYPE_WORLD_DRIVE);

		} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_ADVENTURE)){
			mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_ADVENTURE_TYPE));
			mTrackNameView.setText(VehiclesContract.TRACK_TYPE_ADVENTURE);

		} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_ALEAF_DRIVE)) {
			mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_ALEAF_TYPE));
			mTrackNameView.setText(VehiclesContract.TRACK_TYPE_ALEAF_DRIVE);

		} else if (vehicle.Track.equals( VehiclesContract.TRACK_TYPE_LCV_DRIVE )){
			mTrackTextView.setText(getTrackDescription(TRACK_DESCRIPTION_LCV_TYPE));
			mTrackNameView.setText(VehiclesContract.TRACK_TYPE_LCV_DRIVE);

		}

		courseMapButton = (Button) findViewById(R.id.course_map_button);
		courseMapButton.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.PREVIEW_WALKUP_COURSE");
				i.putExtra(VehicleIntentContract.INTENT_EXTRA_TRACK, vehicle.Track);
				startActivity(i);

			}

		});



	}



}

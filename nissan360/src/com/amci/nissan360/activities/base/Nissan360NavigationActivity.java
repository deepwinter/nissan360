package com.amci.nissan360.activities.base;

import java.util.ArrayList;


import com.amci.nissan360.R;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.Displays;
import com.amci.nissan360.activities.Help;
import com.amci.nissan360.activities.drive.PreviewHighPerformanceCourse;
import com.amci.nissan360.activities.drive.PreviewStreetCourse;
import com.amci.nissan360.activities.drive.PreviewWalkupCourse;
import com.amci.nissan360.activities.drive.VehicleDriveHighPerformance;
import com.amci.nissan360.activities.drive.VehicleDriveStreet;
import com.amci.nissan360.activities.nfc.SmartPoster;
import com.amci.nissan360.activities.nfc.SmartPosterVideo;
import com.amci.nissan360.activities.vehicleselector.VehicleSelector;
import com.amci.nissan360.activities.vehicleselector.VehicleStoryActivity;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.state.ApplicationState;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class Nissan360NavigationActivity extends Nissan360OptionsMenuActivity {

	private MenuItem mSpinnerItem1;

	private int NAVIGATION_ITEM_COUNT = 4;
	private String[] menuOptions = { "Drive                              ", "Displays", "Agenda", "POI Map", "Media"}; 
	private Intent[] menuIntents = { 
			new Intent(VehicleSelector.INTENT_ACTION), 
			new Intent(Displays.INTENT_ACTION), 
			new Intent("com.amci.nissan360.action.AGENDA"), 
			new Intent("com.amci.nissan360.action.POI_MAP"),
			new Intent("com.amci.nissan360.action.MEDIA")
	};

	private String[] driveIntents = {
			VehicleSelector.INTENT_ACTION,
			VehicleDriveStreet.INTENT_ACTION,
			VehicleDriveHighPerformance.INTENT_ACTION,
			PreviewStreetCourse.INTENT_ACTION,
			PreviewHighPerformanceCourse.INTENT_ACTION,
			PreviewWalkupCourse.INTENT_ACTION,
			VehicleStoryActivity.INTENT_ACTION
	};

	private Intent[] mediaIntents;

	private int mSpinnerPositionModifier = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent[] mI = {
				new Intent(this, SmartPoster.class),
				new Intent(this, SmartPosterVideo.class)
		};
		mediaIntents = mI;


		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	
	public void setupSpinner(Menu menu){

		ArrayAdapter<String> ad1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		if(getIntent().getAction() == Help.INTENT_ACTION){
			ad1.add("Help");
			mSpinnerPositionModifier = -1;
		}

		for(String option : menuOptions){
			ad1.add(option);
		}

		mSpinnerItem1 = menu.findItem( R.id.menu_spinner1);
		View view1 = mSpinnerItem1.getActionView();
		boolean positionSet = false;
		Bundle extras = getIntent().getExtras();

		if (view1 instanceof Spinner)
		{
			final Spinner spinner = (Spinner) view1;
			spinner.setAdapter(ad1);
			int selectedPosition = 0;

			if(getIntent().getAction() != null) {
				boolean driveMatch = false;
				String action = getIntent().getAction();
				for(String s : driveIntents){
					if(action.equals(s)){
						selectedPosition = 0;
						driveMatch = true;
					}
				}
				if(extras != null){
					if(driveMatch){
						if(extras.containsKey(VehicleSelector.INTENT_EXTRA_SELECTOR_BUCKET)){
							if(extras.getString(VehicleSelector.INTENT_EXTRA_SELECTOR_BUCKET).equals(VehicleSelector.BUCKET_DISPLAY)){
								selectedPosition = 1;
								positionSet = true;
							}
						}

						if(!positionSet){
							if(extras.containsKey(VehicleIntentContract.INTENT_EXTRA_VIN)){
								String vin = extras.getString(VehicleIntentContract.INTENT_EXTRA_VIN);
								Cursor c = getContentResolver().query(Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, vin), VehiclesProvider.getDefaultProjection(), null, null, null);
								if(c.getCount() > 0){
									c.moveToPosition(0);
									String bucket = c.getString(c.getColumnIndex(VehiclesContract.BUCKET_FIELD));
									if(bucket.equals(VehicleSelector.BUCKET_DISPLAY)){
										selectedPosition = 1;
										positionSet = true;
									}
								}
							}
						}
						positionSet = true;
					}
				}
			}

			for ( Intent i : mediaIntents){
				if(getIntent().equals(i)){
					selectedPosition = 4;
					positionSet = true;
				}
			}

			if(!positionSet){
				int pos = 0;
				for(Intent i : menuIntents){
					if(i.getAction() == getIntent().getAction()){
						selectedPosition = pos;
						positionSet = true;
						break;
					}
					pos++;
				}
			}

			final int currentlySelectedPosition = selectedPosition;
			spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				boolean first = false;
				
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int position, long arg3) {

					if(!first){
						// Skip the first selection - the one which sets the spinner programmatically
						first = true;
						return;
					}
					
					// This deals with items added to drop down to display page names
					position = position + mSpinnerPositionModifier;
					if(position < 0){
						return;
					}

					if(position != currentlySelectedPosition){

						startActivityForPosition(position);

					} else {
						Intent i = menuIntents[position];

						switch(position){
						case 0:
						case 1:
							if( getIntent().getAction() == null){
								break;
							}
							if(! getIntent().getAction().equals(VehicleSelector.INTENT_ACTION) ){
								// What is this for ???
								// This is for when they are on a vehile page and then re-selet display or drive
								// need to launch the vehicle selector
								// but also need to not let this get triggered on menu setup.
								startActivityForPosition(position);
							}
							break;
														
						}
					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});

			spinner.setSelection(selectedPosition);

		}

	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {

		com.actionbarsherlock.view.MenuInflater mi=getSupportMenuInflater();
		mi.inflate(R.menu.nissan, menu);

		// Hide the alarm ?
		if(!ApplicationState.isReservationPending(this)){
			menu.removeItem(R.id.action_alarm);
		}
		
		setupSpinner(menu);

		return true;

	}
	
	public void startActivityForPosition(int position){
		Intent i = menuIntents[position];
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if(position==0){
			i.putExtra(VehicleSelector.INTENT_EXTRA_SELECTOR_BUCKET, VehicleSelector.BUCKET_DRIVE);
		} else if(position==1){
			i.putExtra(VehicleSelector.INTENT_EXTRA_SELECTOR_BUCKET, VehicleSelector.BUCKET_DISPLAY);							
		}

		startActivity(i);
		finish();
	}


}

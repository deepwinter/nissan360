package com.amci.nissan360.activities.base;

import android.content.DialogInterface.OnCancelListener;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;

public class RobospiceActivity extends SherlockFragmentActivity  {

	public SpiceManager spiceManager = new SpiceManager( JacksonSpringAndroidSpiceService.class );

	public RobospiceActivity() {
		super();
	}

	@Override
	protected void onStart() {
		super.onStart();
		spiceManager.start( this );


	}

	@Override
	protected void onStop() {
		if(spiceManager.isStarted()){
			try {
				spiceManager.shouldStop();
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		super.onStop();
	}

	public SpiceManager getSpiceManager() {
		return spiceManager;
	}

}
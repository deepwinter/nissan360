package com.amci.nissan360.activities.base;

import java.util.ArrayList;

import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;

import android.content.Context;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ViewFlipper;

public class FlipperActivity extends Nissan360NavigationActivity {



	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;

	ViewFlipper flipper;
	
	
	protected void addViewToFlipper(View view){
		flipper.addView(view);
	}

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		flipper = new ViewFlipper(this);

		/*
		for(int i=0; i< 4; i++){
			LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layoutInflater.inflate(R.layout.view_vehicle_story, flipper);
		}
		*/


		ArrayList<View> previousButtons = Utilities.getViewsByTag(flipper, "previous_button");
		for(View view : previousButtons){
			ImageButton button = (ImageButton) view;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeRight();
				}

			});
		}

		ArrayList<View> nextButtons = Utilities.getViewsByTag(flipper, "next_button");
		for(View view : nextButtons){
			ImageButton button = (ImageButton) view;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeLeft();
				}

			});
		}


		animFlipInForeward = AnimationUtils.loadAnimation(this, R.anim.flipin);
		animFlipOutForeward = AnimationUtils.loadAnimation(this, R.anim.flipout);
		animFlipInBackward = AnimationUtils.loadAnimation(this, R.anim.flipin_reverse);
		animFlipOutBackward = AnimationUtils.loadAnimation(this, R.anim.flipout_reverse);

		flipper.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		});
		
		setContentView(flipper);

	}

	private void SwipeRight(){
		flipper.setInAnimation(animFlipInBackward);
		flipper.setOutAnimation(animFlipOutBackward);
		flipper.showPrevious();
	}

	private void SwipeLeft(){
		flipper.setInAnimation(animFlipInForeward);
		flipper.setOutAnimation(animFlipOutForeward);
		flipper.showNext();
	}


	SimpleOnGestureListener simpleOnGestureListener = new SimpleOnGestureListener(){

		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {

			float sensitvity = 50;
			if((e1.getX() - e2.getX()) > sensitvity){
				SwipeLeft();
			}else if((e2.getX() - e1.getX()) > sensitvity){
				SwipeRight();
			}

			return true;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			return true;
		}
	};

	GestureDetector gestureDetector= new GestureDetector(this, simpleOnGestureListener);

}




package com.amci.nissan360.activities.base;

import android.content.Intent;

import com.actionbarsherlock.view.MenuItem;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.PhoneNumbers;
import com.amci.nissan360.activities.agenda.Agenda;

public class Nissan360OptionsMenuActivity extends Nissan360Activity {


	
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		Intent intent;
		switch (item.getItemId()) {
		case android.R.id.home:
	        finish();
	        return true;
		case R.id.action_settings:
			intent = new Intent("com.amci.nissan360.action.SETTINGS");
			startActivity(intent);
			return true;
		case R.id.action_help:
			intent = new Intent("com.amci.nissan360.action.HELP");
			startActivity(intent);
			return true;
		case R.id.action_dashboard:
			intent = new Intent("com.amci.nissan360.action.DASHBOARD");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			return true;
		case R.id.action_alarm:
			
			// Look up the current res
			//getContentResolver().query(Uri.withAppendedPath(ReservationsProvider.CONTENT_URI, pathSegment), projection, selection, selectionArgs, sortOrder)
			
			// set lat land lng
			// latitude = extras.getDouble(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE);
			// longitude = extras.getDouble(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE);
			
			intent = new Intent(this, Agenda.class);
			intent.putExtra(Agenda.INTENT_EXTRA_CURRENT_TAB, 1);
			startActivity(intent);
			finish();
			return true;
		case R.id.action_logout:
			// Log the user out
			Nissan360.logout(this);
			return true;
		case R.id.action_phone:
			Intent i = new Intent(this, PhoneNumbers.class);
			startActivity(i);
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	

}

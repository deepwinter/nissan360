package com.amci.nissan360.activities.base;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.preference.PreferenceManager;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.activities.AutoSetup;
import com.amci.nissan360.activities.nfc.TagDiscovered;

public class Nissan360Activity extends RobospiceActivity {


	protected static final String JSON_CACHE_KEY = "CACHE_KEY";
	PendingIntent pendingIntent;

	@Override
	protected void onStart(){
		super.onStart();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());		
		int currentAssetsVersion = settings.getInt(Nissan360.NISSAN360_APK_ASSETS_VERSION_PREFERENCE, 0);
		boolean initialMediaLoaded = settings.getBoolean(Nissan360.NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE, false);
		if(currentAssetsVersion < Nissan360.NISSAN360_APK_ASSETS_VERSION || !initialMediaLoaded){
			Intent i = new Intent(this, AutoSetup.class);
			startActivity(i);
			finish();
		}

	}

	protected void onResume() {
		super.onResume();

		pendingIntent = PendingIntent.getActivity(
				this, 0, new Intent(this, TagDiscovered.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP ), 0);
		IntentFilter tagDiscoveredFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter intentFiltersArray[] = new IntentFilter[] {tagDiscoveredFilter };

		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, intentFiltersArray, null);
		}

		if(controlAccess()){
			try {
				Nissan360.getLoggedInUser();
			} catch (Exception e) {
				Nissan360.logout(this);
			}
		}

	}

	public void onPause() {
		super.onPause();
		if(NfcAdapter.getDefaultAdapter(this) != null){
			NfcAdapter.getDefaultAdapter(this).disableForegroundDispatch(this);
		}
	}

	protected boolean controlAccess(){
		return true;
	}

}

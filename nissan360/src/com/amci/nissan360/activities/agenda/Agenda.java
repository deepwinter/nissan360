package com.amci.nissan360.activities.agenda;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragment;

import com.amci.nissan360.R;

import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Agenda extends Nissan360NavigationActivity {

	public static final int INTENT_EXTRA_QUEUE_TAB = 1;
	public static final String INTENT_EXTRA_CURRENT_TAB = "BUNDLE_CURRENT_TAB";

	SherlockFragment[] mFragments; 
	int currentTab;

	Button mEventsButton;
	Button mQueueButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agenda);

		mFragments = new SherlockFragment[2];
		mFragments[0] = new AgendaEventListFragment();
		mFragments[1] = new AgendaQueueFragment();

		mEventsButton = (Button) findViewById(R.id.agenda_events_list_tab);
		mQueueButton = (Button) findViewById(R.id.agenda_queue_tab);

		mEventsButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTab(0);
			}

		});

		mQueueButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTab(1);
			}

		});

		if(savedInstanceState != null && savedInstanceState.containsKey(INTENT_EXTRA_CURRENT_TAB)){
			currentTab = savedInstanceState.getInt(INTENT_EXTRA_CURRENT_TAB);
		}

	}

	@Override
	public void onStart(){
		super.onStart();
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			currentTab = extras.getInt(INTENT_EXTRA_CURRENT_TAB, 0);
		}
	}

	@Override
	public void onResume(){
		super.onResume();
		showTab(currentTab);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putInt(INTENT_EXTRA_CURRENT_TAB, currentTab);

	}

	Object lock = new Object();
	
	public void showTab(int position){
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction;

		synchronized(lock){
			switch (position){
			case 0:
				transaction = fragmentManager.beginTransaction();
				transaction.replace(R.id.fragment_container, mFragments[0]);
				transaction.commit();
				mEventsButton.setSelected(true);
				mQueueButton.setSelected(false);
				break;

			case 1:
				transaction = fragmentManager.beginTransaction();
				transaction.replace(R.id.fragment_container, mFragments[1]);
				transaction.commit();
				mEventsButton.setSelected(false);
				mQueueButton.setSelected(true);

				break;

			}
			currentTab = position;
		}

	}




}

package com.amci.nissan360.activities.agenda;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.TimeOfDay;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.actionbarsherlock.app.SherlockFragment;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.database.AgendaContract;
import com.amci.nissan360.provider.AgendaProvider;
import com.amci.nissan360.widgets.Nissan360ViewFlipper;

public class AgendaEventListFragment extends SherlockFragment implements LoaderManager.LoaderCallbacks<Cursor>  {

	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;

	ViewFlipper flipper;

	AgendaCursorAdapter adapters[];
	LoaderManager loaderManagers[];
	ListView listViews[];
	View pages[];

	class AgendaCursorAdapter extends SimpleCursorAdapter {

		public AgendaCursorAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			super.bindView(view, context, cursor);

			try {
				String time = cursor.getString(cursor.getColumnIndex(AgendaContract.TIME_FIELD));
				TextView timeView = (TextView) view.findViewById(R.id.event_time_text);
				timeView.setText(time);
			} catch (Exception e){
				e.printStackTrace();
			}
		}

		@Override
		public View getView(int position, View arg1, ViewGroup arg2) {
			View view = super.getView(position, arg1, arg2);

			if(position % 2 == 0){
				view.setBackgroundResource(R.color.list_item_dark);
			} else {
				view.setBackgroundResource(R.color.list_item_light);

			}
			return view;
		}


	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// maybe do a pre-load of all the agenda days currently in the system
		// and then do this view setup in the Loader callback??

		flipper = new Nissan360ViewFlipper(getActivity());

		adapters = new AgendaCursorAdapter[4];
		loaderManagers = new LoaderManager[4];
		listViews = new ListView[4];
		pages = new View[4];
		for(int i=0; i< 4; i++){
			// Get events for each day of the event
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View agendaDayView = layoutInflater.inflate(R.layout.view_agenda_day, null);
			pages[i] = agendaDayView;

			String from[] = { AgendaContract.NAME_FIELD, AgendaContract.TIME_FIELD };
			int to[] = { R.id.event_name_text, R.id.event_time_text };

			adapters[i] = new AgendaCursorAdapter(getActivity(), R.layout.list_item_event_list, null, from, to);

			ListView listView = (ListView) agendaDayView.findViewWithTag("agenda_day_list");
			listView.setAdapter(adapters[i]);
			listViews[i] = listView;

			ImageButton moveLeftButton = (ImageButton) agendaDayView.findViewWithTag("agenda_page_left_button");
			moveLeftButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeLeft();					
				}

			});
			ImageButton moveRightButton = (ImageButton) agendaDayView.findViewWithTag("agenda_page_right_button");
			moveRightButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeRight();					
				}

			});



			flipper.addView(agendaDayView);

		}

		animFlipInForeward = AnimationUtils.loadAnimation(getActivity(), R.anim.flipin);
		animFlipOutForeward = AnimationUtils.loadAnimation(getActivity(), R.anim.flipout);
		animFlipInBackward = AnimationUtils.loadAnimation(getActivity(), R.anim.flipin_reverse);
		animFlipOutBackward = AnimationUtils.loadAnimation(getActivity(), R.anim.flipout_reverse);

		flipper.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		});

		return flipper;


	}

	@Override
	public void onStart() {
		super.onStart();
		for(int i=0; i< 4; i++){

			LoaderManager lm = getActivity().getSupportLoaderManager();
			lm.initLoader(i, null, this);
			loaderManagers[i] = lm;
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Save and reload currently visible page fragment
		super.onSaveInstanceState(outState);
	}

	private void SwipeLeft(){
		flipper.setInAnimation(animFlipInBackward);
		flipper.setOutAnimation(animFlipOutBackward);
		flipper.showPrevious();
	}

	private void SwipeRight(){
		flipper.setInAnimation(animFlipInForeward);
		flipper.setOutAnimation(animFlipOutForeward);
		flipper.showNext();
	}


	SimpleOnGestureListener simpleOnGestureListener = new SimpleOnGestureListener(){

		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {

			float sensitvity = 50;
			if((e1.getX() - e2.getX()) > sensitvity){
				SwipeLeft();
			}else if((e2.getX() - e1.getX()) > sensitvity){
				SwipeRight();
			}

			return true;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			return true;
		}
	};

	GestureDetector gestureDetector= new GestureDetector(getActivity(), simpleOnGestureListener);

	@Override
	public Loader<Cursor> onCreateLoader(int loaderId, Bundle arg1) {
		String[] projection = {
				AgendaContract.ID_FIELD,
				AgendaContract.NAME_FIELD,
				AgendaContract.TIME_FIELD,
				AgendaContract.DATE_FIELD
		};


		// Create a new CursorLoader with the following query parameters.
		return new CursorLoader(getActivity(), 
				Uri.withAppendedPath(AgendaProvider.CONTENT_URI_BY_DAY, String.valueOf(loaderId + 1) ),
				projection, null, null, AgendaContract.ID_FIELD);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor c) {
		int count = c.getCount();

		adapters[l.getId()].swapCursor(c);
		adapters[l.getId()].notifyDataSetChanged();

		if(c.getCount() > 0){
			c.moveToPosition(0);
			TextView dayTitleView = (TextView) pages[l.getId()].findViewById(R.id.date_view);
			String date = c.getString(c.getColumnIndex(AgendaContract.DATE_FIELD));
			DateTime dateTime = new DateTime(date);
			dayTitleView.setText(Utilities.formatDate(dateTime));
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> l) {
		adapters[l.getId()].swapCursor(null);
		adapters[l.getId()].notifyDataSetChanged();
	}




}

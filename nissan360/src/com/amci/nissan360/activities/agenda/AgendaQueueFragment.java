package com.amci.nissan360.activities.agenda;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.actionbarsherlock.app.SherlockFragment;
import com.amci.nissan360.MapIntentContract;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.RobospiceActivity;
import com.amci.nissan360.activities.drive.DriveLocationMap;
import com.amci.nissan360.activities.drive.VehicleDriveWalkUp;
import com.amci.nissan360.api.reservation.ReservationCancelRequest;
import com.amci.nissan360.api.reservation.ReservationInfo;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.ReservationsContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.PointsOfInterestProvider;
import com.amci.nissan360.provider.ReservationsProvider;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.state.ApplicationState;
import com.amci.nissan360.state.ReservationState;
import com.amci.nissan360.widgets.NotificationDialog;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class AgendaQueueFragment extends SherlockFragment implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int LOADER_ID = 3003;

	ImageButton mCancelReservationButton;
	ListView mListView;
	ReservationsAdapter reservationsAdapter;

	private Cursor reservationsCursor;


	class ReservationsAdapter extends CursorAdapter {

		static final int STREET_DRIVE_TYPE = 0;
		static final int PERFORMANCE_DRIVE_TYPE = 1;

		private final LayoutInflater mInflater;

		public ReservationsAdapter(Context context, Cursor c) {
			super(context, c);
			mInflater=LayoutInflater.from(context);

		}

		@Override
		public void bindView(View view, Context context, final Cursor cursor) {

			TextView vehicleNameView = (TextView) view.findViewWithTag("reservation_vehicle_name");
			String vehicle = cursor.getString(cursor.getColumnIndex(ReservationsContract.VEHICLE_FIELD));
			vehicleNameView.setText(vehicle);
			int reservationId = cursor.getInt(cursor.getColumnIndex(ReservationsContract.RESERVATION_ID_FIELD));

			String track = getCursor().getString(getCursor().getColumnIndex(ReservationsContract.TRACK_FIELD));
			if(track.equals(ReservationsContract.RURAL_DRIVE)
					|| track.equals(ReservationsContract.URBAN_DRIVE)){
				DateTime startTime;
				TextView waitTimeView = (TextView) view.findViewById(R.id.reservation_wait_time);
				String waitTimeMessage = "";

				int queuePos = cursor.getInt(cursor.getColumnIndex(ReservationsContract.QUEUE_POSITION_FIELD));

				try {
					String startTimeString = cursor.getString(cursor.getColumnIndex(ReservationsContract.BEGIN_TIME_FIELD));
					startTime = new DateTime( startTimeString );
					DateTime now = new DateTime();
					Duration duration = new Duration(now, startTime);

					waitTimeMessage = "";

					// Within... a certain number of minute
					// When they are in reservation pending mode, show within 15 minutes
					String waitTimeMessageV2;
					if(ApplicationState.isReservationPending(getActivity())
							&& ApplicationState.reservationIsPending(reservationId)
							){
						waitTimeMessageV2 = "Within 15 Minutes";
					} else {
						int number = 45 * queuePos;
						if(number  > 60){
							int hours = (int) (number / 60);
							int minutes = number % 60;
							waitTimeMessageV2 = "Within " + String.valueOf(hours) + " Hours, " + String.valueOf(minutes) + " Minutes";
						} else {
							waitTimeMessageV2 = "Within " + String.valueOf(number) + " Minutes";
						}
					}

					/*
					long hoursDiff = duration.getStandardHours();
					long minutesDiff = duration.getStandardMinutes();
					if( hoursDiff <= 0L || minutesDiff <= 0L ){
						waitTimeMessage = "Available Now";
					} else {
						String hours = String.valueOf( duration.getStandardHours() );
						String minutes = String.valueOf( duration.getStandardMinutes() - duration.getStandardHours() * 60);
						waitTimeMessage ="Estimated wait time " + hours + " hrs " + minutes + "minutes";
					}
					 */
					waitTimeView.setText(waitTimeMessageV2);


				} catch (IllegalArgumentException e){
					e.printStackTrace();
				}


				TextView numberInQueue = (TextView) view.findViewById(R.id.reservation_place_in_queue);
				numberInQueue.setText( "Queue Position: " + String.valueOf(queuePos) );




			} else {
				DateTime startTime = new DateTime( getCursor().getString(getCursor().getColumnIndex(ReservationsContract.BEGIN_TIME_FIELD)) );
				DateTime endTime = new DateTime( getCursor().getString(getCursor().getColumnIndex(ReservationsContract.END_TIME_FIELD)) );
				TextView reservationScheduledTimeView = (TextView) view.findViewById(R.id.reservation_scheduled_time);
				DateTimeFormatter fmt = DateTimeFormat.forPattern("h:mm");
				String scheduledTimeMessage = "Scheduled Time : " + fmt.print(startTime) + " - " + fmt.print(endTime);
				reservationScheduledTimeView.setText(scheduledTimeMessage);
			}


			mCancelReservationButton = (ImageButton) view.findViewById(R.id.queue_cancel_reservation_button);
			final int cursorPosition = cursor.getPosition();
			mCancelReservationButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {


					AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
					builder1.setTitle("Cancel Reservation?");
					builder1.setMessage("Do you want to cancel this reservation?");
					builder1.setCancelable(true);
					builder1.setPositiveButton(android.R.string.yes,
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();


							if(cursor == null
									|| cursorPosition + 1 > cursorPosition + 1
									|| cursorPosition < 0 ){
								return;
							}
							cursor.moveToPosition(cursorPosition);
							final int reservationId = cursor.getInt(cursor.getColumnIndex(ReservationsContract.RESERVATION_ID_FIELD));

							// Send delete the server
							class ReservationDeleteListener implements RequestListener < Boolean> {

								@Override
								public void onRequestFailure(
										SpiceException arg0) {
									// As an enhancement, we could add a record to the queue table indicating a delete here
									// But then the queue table MUST send the queued requests synchronously
									NotificationDialog.show(getActivity(), "Error", "Could not confirm your cancellation at this time");

								}

								@Override
								public void onRequestSuccess(
										Boolean arg0) {
									// Success!  Let them know!


									// Delete res. locally
									getActivity().getContentResolver().delete(
											Uri.withAppendedPath(ReservationsProvider.CONTENT_URI, String.valueOf(reservationId)),
											null, null);

									AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
									builder1.setTitle("Reservation Cancelled");
									builder1.setCancelable(true);
									builder1.setNeutralButton(android.R.string.ok,
											new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});

									AlertDialog alert11 = builder1.create();
									alert11.show();

								}


							}

							((RobospiceActivity) getActivity()).getSpiceManager().execute( new ReservationCancelRequest( String.valueOf(reservationId) ), null, DurationInMillis.NEVER, new ReservationDeleteListener() );

						}
					});
					builder1.setNegativeButton(android.R.string.no,
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});

					AlertDialog alert11 = builder1.create();
					alert11.show();

				}

			});

		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view;

			String track = getCursor().getString(getCursor().getColumnIndex(ReservationsContract.TRACK_FIELD));
			if(VehiclesContract.TRACK_TYPES_STREET.contains( track ) ){
				view = mInflater.inflate(R.layout.list_item_reservation_street,parent,false); ;
			} else {
				view = mInflater.inflate(R.layout.list_item_reservation_performance,parent,false); ;
			}

			return view;
		}

		@Override
		public int getViewTypeCount() {
			return 2;
		}

		@Override
		public int getItemViewType(int position) {
			getCursor().moveToPosition(position);
			if( VehiclesContract.TRACK_TYPES_STREET.contains( getCursor().getString(mCursor.getColumnIndex(ReservationsContract.TRACK_FIELD)) ) ){
				return STREET_DRIVE_TYPE;
			} else {
				return PERFORMANCE_DRIVE_TYPE;
			}

		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_agenda_queue_fragment, container, false);
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();

		try {
			Nissan360.application.getNissanApiService().refreshReservations(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mListView = (ListView) getView().findViewById(R.id.queue_list);

		LoaderManager lm = getActivity().getSupportLoaderManager();
		lm.initLoader(LOADER_ID, null, this);

		// Going to need to write a custom adapter here to handle the performance reservations
		// Actually, that's complicated, instead let's just customize the .. item.. oh
		// gonna need a custom adapter for that as well.
		reservationsAdapter = new ReservationsAdapter(getActivity(), null);
		mListView.setAdapter(reservationsAdapter);

		mListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				reservationsCursor.moveToPosition(position);

				String VIN = reservationsCursor.getString(reservationsCursor.getColumnIndex(ReservationsContract.VEHICLE_ID_FIELD));
				if(VIN == null){
					return;
				}

				Cursor c = getActivity().getContentResolver().query( Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, VIN), 
						VehiclesProvider.getDefaultProjection(), null, null, null
						);
				if(c.getCount() < 1){
					Toast.makeText(getActivity(), "Vehicle not found", Toast.LENGTH_SHORT).show();
					return;
				}

				c.moveToPosition(0);
				String track = c.getString(c.getColumnIndex(VehiclesContract.TRACK_FIELD));
				String bucket = c.getString(c.getColumnIndex(VehiclesContract.BUCKET_FIELD));
				if(track == null){
					Toast.makeText(getActivity(), "Track not found", Toast.LENGTH_SHORT).show();
					return;
				}				

				double latitude = 0;
				double longitude = 0;

				if(track.equals(VehiclesContract.TRACK_TYPE_HIGH_PERFORMANCE)){


					Cursor geoCursor = getActivity().getContentResolver().query(PointsOfInterestProvider.CONTENT_URI_GEO_ASSETS, PointsOfInterestProvider.getGeoAssetsProjection(),
							"title = 'High Performance Shuttle'", null, null);
					if(geoCursor.getCount() < 1){
						Toast.makeText(getActivity(), "Location not found", Toast.LENGTH_SHORT).show();
						return;
					}

					geoCursor.moveToFirst();
					latitude = geoCursor.getDouble(c.getColumnIndex(AssetsContract.LATITUDE_FIELD));
					longitude = geoCursor.getDouble(c.getColumnIndex(AssetsContract.LONGITUDE_FIELD));

				} else if(VehiclesContract.TRACK_TYPES_STREET.contains(track)){
					Cursor geoCursor = getActivity().getContentResolver().query(PointsOfInterestProvider.CONTENT_URI_GEO_ASSETS, PointsOfInterestProvider.getGeoAssetsProjection(),
							"subtype = 'Street Drive'", null, null);
					if(geoCursor.getCount() < 1){
						Toast.makeText(getActivity(), "Location not found", Toast.LENGTH_SHORT).show();
						return;
					}

					geoCursor.moveToFirst();
					latitude = geoCursor.getDouble(c.getColumnIndex(AssetsContract.LATITUDE_FIELD));
					longitude = geoCursor.getDouble(c.getColumnIndex(AssetsContract.LONGITUDE_FIELD));
				}

				if(latitude == 0 || longitude == 0){
					Toast.makeText(getActivity(), "Coordinates not found", Toast.LENGTH_SHORT).show();
					return;
				}
				
				Intent i = new Intent(getActivity(), DriveLocationMap.class);
				i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE, latitude);
				i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE, longitude);
				getActivity().startActivity(i);	
				
			}

		});


	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {


		// Create a new CursorLoader with the following query parameters.
		return new CursorLoader(getActivity(), 
				ReservationsProvider.CONTENT_URI,
				ReservationsProvider.getDefaultProjection(), null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor c) {
		reservationsCursor = c;
		reservationsAdapter.swapCursor(c);
		reservationsAdapter.notifyDataSetChanged();


	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		reservationsAdapter.swapCursor(null);
		reservationsAdapter.notifyDataSetChanged();
	}




}

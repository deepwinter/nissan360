package com.amci.nissan360.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.Nissan360Activity;
import com.amci.nissan360.api.AllUsersRequest;
import com.amci.nissan360.api.AllUsersResponse;
import com.amci.nissan360.api.LoginUser;
import com.amci.nissan360.state.ApplicationState;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class Login extends Nissan360Activity {


	// Login Data
	private String mEmail;

	// UI references.
	private EditText mEmailView;
	private Spinner mCountriesSpinner;
	private Button mSignInButton;
	private ProgressBar mProgressBar;

	// Data
	private List<String> countries = new ArrayList<String>();
	private ArrayAdapter<String> spinnerAdapter;
	private ObjectMapper objectMapper;

	// Locks
	private static JSONDatabase usersDatabase;


	private int hiddenLaunch = 0;

	private View mLoginFormView;


	@Override
	protected boolean controlAccess(){
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		objectMapper = new ObjectMapper();

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		String loggedInUserJSON = settings.getString(Nissan360.LOGGED_IN_USER_PREFERENCE, null);
		if(loggedInUserJSON != null){
			try {
				LoginUser loginUser = objectMapper.readValue(loggedInUserJSON, LoginUser.class);
				if(loginUser != null){
					Nissan360.setLoggedInUser(loginUser);

					ApplicationState.startRootActivityForCurrentState(Login.this);
					finish();
				}

				// Falling through exceptions below just means we're going to need them to log in
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		try {
			usersDatabase = JSONDatabase.GetDatabase(getApplicationContext(), "UsersJSONDatabaseConfiguration");
		} catch (InitJSONDatabaseExcepiton e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Refresh the database of users for quick, offline login and country verification


		setContentView(R.layout.activity_login);

		mEmailView = (EditText) findViewById(R.id.email_entry);
		mLoginFormView = (View) findViewById(R.id.login_form);
		mCountriesSpinner = (Spinner) findViewById(R.id.country_spinner);
		mSignInButton = (Button) findViewById(R.id.sign_in_button);
		mProgressBar = (ProgressBar) findViewById(R.id.login_progress_bar);

		mSignInButton.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						showProgress();
						attemptLogin();
					}
				});

		mSignInButton.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View v) {
				hiddenLaunch++;
				if(hiddenLaunch>1){
					launchHiddenActivityIfActivated();
				}
				return false;
			}

		});

		mCountriesSpinner.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View v) {
				hiddenLaunch++;
				if(hiddenLaunch>1){
					launchHiddenActivityIfActivated();
				}
				return false;
			}

		});

		showProgress();
		new AsyncTask<Void, Void, Void>() {  

			@Override
			protected Void doInBackground(Void... params) {
				synchronized(usersDatabase){
					readCountriesFromDatabase();
				}
				return null;



			}


			@Override
			protected void onPostExecute(Void v) {
				if(isFinishing()){
					return;
				}
				spinnerAdapter = new ArrayAdapter<String>(Login.this, android.R.layout.simple_spinner_item, countries);
				spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				mCountriesSpinner.setAdapter(spinnerAdapter);	
				hideProgressBar();
			}

		}.execute(null, null, null);


		// iff nothing has ever been loaded
		Collection<JSONEntity> entities = usersDatabase.fetchAllEntities();


		if(entities.size() == 0){

			showProgress();
			refreshUsersList();
		}
	}

	protected void launchHiddenActivityIfActivated() {
		// Force the login

		LoginUser lUser = new LoginUser();
		lUser.LoginId = mEmailView.getText().toString();
		lUser.Email = mEmailView.getText().toString();
		Nissan360.setLoggedInUser(lUser);

		loadUserData();
	}

	public void onStart(){
		super.onStart();
		// Best place to check for google play services
		// servicesConnected();
	}

	private void showProgress(){
		mSignInButton.setVisibility(View.GONE);
		mProgressBar.setVisibility(View.VISIBLE);
	}

	private void hideProgressBar(){
		mSignInButton.setVisibility(View.VISIBLE);
		mProgressBar.setVisibility(View.GONE);
	}

	private void readCountriesFromDatabase() {

		Collection<JSONEntity> entities = usersDatabase.fetchAllEntities();

		/*
		 * Causes threading issues
		if(entities.size() == 0){
			mCountriesSpinner.setVisibility(View.GONE);
			showProgress();
			return;
		} else {
			mCountriesSpinner.setVisibility(View.VISIBLE);
			hideProgressBar();	
		}
		 */

		for(JSONEntity e : entities){
			try {
				String country = "";
				if(e.dataKeys().contains("Country")){
					country = e.getString("Country");
				} 
				if(! countries.contains(country)){
					countries.add(country);
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				// Just continue on as a best effort here.
			}
		}
	}




	private void refreshUsersList() {

		// Update the list of users
		class AllUsersRequestListener implements RequestListener< AllUsersResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				//showProgress(false);
				if(e instanceof NoNetworkException){
					Toast.makeText(Login.this, "Network connection is unavailable", Toast.LENGTH_LONG).show();
					spiceManager.cancelAllRequests();
				} else {
					Toast.makeText(Login.this, "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}			

				hideProgressBar();
			}

			@Override
			public void onRequestSuccess(AllUsersResponse allUsers) {
				if(isFinishing()){
					return;
				}

				AllUsersResponse[] params = new AllUsersResponse[1];
				params[0] = allUsers;

				// This is synchronized to avoid checking the database while it's beeing updated
				showProgress();

				new AsyncTask<AllUsersResponse, Void, Void>() {  

					@Override
					protected Void doInBackground(AllUsersResponse... params) {
						synchronized(usersDatabase){

							usersDatabase.deleteAll();

							Iterator<LoginUser> it = params[0].iterator();

							while (it.hasNext()) {
								//Map.Entry<String, LoginUser> pair = (Map.Entry<String, LoginUser>)it.next();
								//LoginUser user = (LoginUser) pair.getValue();
								LoginUser user = it.next();
								user.Email = user.Email.toUpperCase();
								user.LoginId = user.LoginId.toUpperCase();

								try {
									usersDatabase.insert(user, LoginUser.class);
								} catch (JsonProcessingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
						return null;


					}


					@Override
					protected void onPostExecute(Void v) {
						if(isFinishing()){
							return;
						}

						showProgress();
						spinnerAdapter.clear();

						new AsyncTask<Void, Void, Void>() {  

							@Override
							protected Void doInBackground(Void... params) {
								synchronized(usersDatabase){
									readCountriesFromDatabase();
								}
								return null;



							}


							@Override
							protected void onPostExecute(Void v) {
								if(isFinishing()){
									return;
								}
								spinnerAdapter.notifyDataSetChanged();
								mCountriesSpinner.invalidate();
								hideProgressBar();
							}




						}.execute(null, null, null);


					}




				}.execute(params);



			}

		}

		spiceManager.execute( new AllUsersRequest(), null, DurationInMillis.NEVER, new AllUsersRequestListener() );


	}

	class MHandler extends Handler {

		public MHandler() {
			super();
		}

		public int offset;

		public void handleMessage(Message msg) {
			if(isFinishing()){
				return;
			}
			if(spinnerAdapter == null){
				return;
			}
			if(mCountriesSpinner == null){
				return;
			}
			if(usersDatabase == null){
				return;
			}
			if(	mSignInButton == null || mProgressBar == null){
				return;
			}

			spinnerAdapter.clear();
			readCountriesFromDatabase();
			spinnerAdapter.notifyDataSetChanged();
			mCountriesSpinner.invalidate();
			hideProgressBar();
		}


	};
	MHandler mHandler = new MHandler();

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */

	private void attemptLogin(){
		attemptLogin(true);
	}

	private void attemptLogin(boolean retryOnFail) {

		// Reset errors.
		mEmailView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
			hideProgressBar();
		} else {

			try {

				// Verify email and country
				SearchFields search = SearchFields.Where("LoginId", mEmail.toUpperCase());

				ArrayList<JSONEntity> entities = new ArrayList<JSONEntity>( usersDatabase.fetchByFields(search) );
				if(entities.size() > 0){
					LoginUser loginUser = (LoginUser) entities.get(0).asClass(LoginUser.class);

					String selectedCountry = (String) spinnerAdapter.getItem( mCountriesSpinner.getSelectedItemPosition() );
					if(loginUser.Country.equals(selectedCountry)){

						SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

						try {
							String loggedInUserJSON = objectMapper.writeValueAsString(loginUser);
							settings.edit().putString(Nissan360.LOGGED_IN_USER_PREFERENCE, loggedInUserJSON).commit();
						} catch (JsonProcessingException e) {
							e.printStackTrace();
							// Just ignore, they are logged in but it won't persist
						}

						Nissan360.setLoggedInUser(loginUser);

						loadUserData();


					} else {
						if(!retryOnFail){
							mEmailView.setError("Invalid Country for this Email");
							focusView = mEmailView;
							focusView.requestFocus();
							hideProgressBar();
						} else {
							refreshUsersList();
							attemptLogin(false);
						}
					}


				} else {
					if(!retryOnFail){
						mEmailView.setError("Email Address Not Found");
						focusView = mEmailView;
						focusView.requestFocus();
						hideProgressBar();
					} else {
						refreshUsersList();
						attemptLogin(false);
					}
				}

			} catch (JsonParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (JsonMappingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalStateException e2){
				e2.printStackTrace();
			}
		}

	}


	private void loadUserData() {
		try {
			Nissan360.application.getNissanApiService().refreshReservations(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		goToDashboard();


	}

	private void goToDashboard() {
		try {
			Nissan360.application.getNissanApiService().sendBeacon();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Send a beacon
		Intent i = new Intent("com.amci.nissan360.action.DASHBOARD");
		startActivity(i);
		finish();		
	}


}

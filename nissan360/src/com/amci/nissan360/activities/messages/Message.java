package com.amci.nissan360.activities.messages;

import com.amci.nissan360.R;
import com.amci.nissan360.R.id;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Message extends Nissan360NavigationActivity {

	TextView messageView;
	Button closeButton;
	
	public static final String MESSAGE_EXTRA = "MESSAGE_EXTRA";
	public static final String TYPE_EXTRA = "TYPE_EXTRA";

	public static final String ALERT_TYPE = "ALERT_TYPE";
	private static final String ANNOUNCEMENT_TYPE = "ANNOUNCEMENT_TYPE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		
		messageView = (TextView) findViewById(R.id.messageView);
		closeButton = (Button) findViewById(R.id.messageCloseButton);
		
		String messageText = getIntent().getExtras().getString(MESSAGE_EXTRA);
		
		messageView.setText(messageText);
		closeButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Message.this.finish();
			}
			
		});
		
		ImageView background = (ImageView) findViewById(R.id.message_background);
		String type = getIntent().getExtras().getString(TYPE_EXTRA, ANNOUNCEMENT_TYPE);
		if(type.equals(ALERT_TYPE)){
			background.setImageResource(R.drawable.alert);
		}
		
	}



}

package com.amci.nissan360.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.activities.base.RobospiceActivity;
import com.amci.nissan360.api.DownloadFileRequest;
import com.amci.nissan360.state.ApplicationState;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;
import com.octo.android.robospice.request.listener.RequestStatus;

public class AutoSetup extends RobospiceActivity {

	TextView mDoneText;
	ProgressBar mProgressSpinner;
	TextView mProgressLog;
	Button mRestartButton;

	private AssetManager assetManager;

	// Initial Media
	public static final String initialMediaUrl = "http://nissan.whitemanenterprises.com/media/initial_media.zip";

	public static final String TAG = "AutoSetup";

	boolean doneWithAssets = false;
	boolean doneWithMedia = false;
	private SharedPreferences settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_copy_assets);

		mDoneText = (TextView) findViewById(R.id.loader_done);
		mProgressSpinner = (ProgressBar) findViewById(R.id.loader_progress_bar);
		mProgressLog = (TextView) findViewById(R.id.progress_log);
		mRestartButton = (Button) findViewById(R.id.restart_button);
		mRestartButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mRestartButton.setVisibility(View.GONE);
				testForDone();
			}
			
		});
		

		assetManager = this.getAssets();

		load();

	}
	
	private void load(){
		// Load Assets
		settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());		
		int currentAssetsVersion = settings.getInt(Nissan360.NISSAN360_APK_ASSETS_VERSION_PREFERENCE, 0);
		boolean initialMediaLoaded = settings.getBoolean(Nissan360.NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE, false);
		if(currentAssetsVersion < Nissan360.NISSAN360_APK_ASSETS_VERSION){
			// Copy all the assets!
			updateProgress("Need to copy assets");
			copyAssetsInBackground();
		} else if (!initialMediaLoaded)  {
			updateProgress("Assets done on start");
			assetsDone();
			downloadInitialMediaZipFile();
		} else {
			finish();
		}
	}

	private final String PROGRESS_MESSAGE = "PROGRESS_MESSAGE";

	class MHandler extends Handler {

		public MHandler() {
			super();
		}

		public int offset;

		public void handleMessage(Message message) {
			if(mProgressLog == null){
				return;
			}
			Bundle b = message.getData();
			if(b == null)
				return;
			String logMessage = b.getString(PROGRESS_MESSAGE);
			mProgressLog.setText( mProgressLog.getText()+ "\n"+logMessage);
		}

	};
	MHandler hanlder = new MHandler();

	public void updateProgress(String message){
		Log.d(TAG, message);
		Message msg = new Message();
		Bundle bundle = new Bundle();
		bundle.putString(PROGRESS_MESSAGE, message);
		msg.setData(bundle);
		msg.setTarget(hanlder);
		msg.sendToTarget();
	}

	@Override
	public void onStart(){
		super.onStart();



	}

	private void assetsDone(){
		updateProgress("Done with assets copy");
		doneWithAssets = true;
		settings.edit().putInt(Nissan360.NISSAN360_APK_ASSETS_VERSION_PREFERENCE, Nissan360.NISSAN360_APK_ASSETS_VERSION).commit();

	}

	private void downloadInitialMediaZipFile() {
		updateProgress("Starting downloadInitialMediaZipFile");
		
		// Call the download in the background service
		
		class DownloadMediaAssetsRequestListener implements RequestListener<String>, RequestProgressListener {

			int counter = 0;
			
			@Override
			public void onRequestFailure(SpiceException e) {
				updateProgress("Failed downloading initial zip");
				// Show Restart Button
				mRestartButton.setVisibility(View.VISIBLE);
			}

			@Override
			public void onRequestSuccess(String tempFile) {
				updateProgress("Success downloading zip");
				doneWithMedia = Utilities.unpackZip(tempFile, getApplicationContext().getFilesDir().getPath() + "/nissan360/" );
				if(!doneWithMedia){
					updateProgress("Success unpacking zip");
					Toast.makeText(AutoSetup.this, "Failed to unpack initial media", Toast.LENGTH_SHORT).show();
					updateProgress("ERROR: Initiate media did not install correctly, manual rerun required");
				} else {
					settings.edit().putBoolean(Nissan360.NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE, true).commit();
				}
				doneWithMedia = false;
				testForDone();
			}

			@Override
			public void onRequestProgressUpdate(RequestProgress progress) {
				RequestStatus status = progress.getStatus();
				counter++;
				updateProgress(status.name() + ": " + String.valueOf(progress.getProgress()) +"%" );
			}

		}
		
		try {
			Nissan360.application.getNissanApiService().sendRequest(new DownloadFileRequest(initialMediaUrl),
					new DownloadMediaAssetsRequestListener());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testForDone() {
		if(doneWithMedia && doneWithAssets){
			initiateMediaPatchAndLaunch();
		} else if (doneWithAssets) {
			Toast.makeText(this, "Retrying", Toast.LENGTH_SHORT).show();
			load();
		}
	}

	public void initiateMediaPatchAndLaunch(){
		updateProgress("Initiate config update and media patch");
		// Done with update, initiate beacon to check media hashes
		try {
			Nissan360.application.getNissanApiService().sendBeacon();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			updateProgress("Failed media patch");
			return;
		}
		if(Nissan360.getLoggedInUser(true) == null ){
			Intent i = new Intent(this, Login.class);
			startActivity(i);
		} else {
			updateProgress("Launch activity");
			ApplicationState.startRootActivityForCurrentState(this);
		}
		finish();
	}

	private void copyAssetsInBackground() {
		new AsyncTask<Void, Void, String>() {   // Could also be Robospice

			@Override
			protected String doInBackground(Void... params) {
				try {
					updateProgress("Starting copying assets");
					copyAssetsFileOrDir("nissan360");
					updateProgress("Done unpacking binary assets");
					return "Done unpacking binary assets";
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					updateProgress("Assets copy thread was interrupted");
					return "Assets copy thread was interrupted";
				} 

			}


			@Override
			protected void onPostExecute(String msg) {
				updateProgress("Done with copyAssetsInBackground");
				if(msg != null){
					Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
				}
				//mDisplay.append(msg + "\n");
				assetsDone();
				boolean initialMediaLoaded = settings.getBoolean(Nissan360.NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE, false);
				if(!initialMediaLoaded){
					downloadInitialMediaZipFile();
				} else {
					doneWithMedia = true;
					testForDone();
				}
			}



		}.execute(null, null, null);
	}


	// Copy Assets
	private void copyAssetsFileOrDir(String path) throws InterruptedException {
		Thread.sleep(5);
		String assets[] = null;
		try {
			assets = assetManager.list(path);
			if (assets.length == 0) {
				copyFile(path);
			} else {
				String fullPath = getApplicationContext().getFilesDir().getPath() + "/" + path;
				File dir = new File(fullPath);
				if (!dir.exists())
					dir.mkdir();
				for (int i = 0; i < assets.length; ++i) {
					copyAssetsFileOrDir(path + "/" + assets[i]);
				}
			}
		} catch (IOException ex) {
			Log.e("tag", "I/O Exception", ex);
		}
	}

	private void copyFile(String filename) {
		AssetManager assetManager = this.getAssets();

		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open(filename);
			String newFileName = getApplicationContext().getFilesDir().getPath() + "/" + filename;
			out = new FileOutputStream(newFileName);

			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch (Exception e) {
			Log.e("tag", e.getMessage());
		}

	}

}

package com.amci.nissan360.activities;

import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class Displays extends Nissan360NavigationActivity {

	public static final String INTENT_ACTION = "com.amci.nissan360.action.VEHICLE_DISPLAYS";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_displays);
	}

}

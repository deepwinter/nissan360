package com.amci.nissan360.activities;

import net.osmand.plus.activities.MapActivity;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.amci.nissan360.R;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.activities.admin.HiddenPanel;
import com.amci.nissan360.activities.admin.Loader;
import com.amci.nissan360.activities.base.Nissan360Activity;
import com.amci.nissan360.activities.base.Nissan360OptionsMenuActivity;
import com.amci.nissan360.activities.vehicleselector.VehicleSelector;
import com.amci.nissan360.api.LoginUser;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Dashboard extends Nissan360OptionsMenuActivity {

	ImageButton mDriveButton;
	ImageButton mDisplaysButton;
	ImageButton mAgendaButton;
	ImageButton mPOIMapButton;
	ImageButton mMediaButton;
	ImageButton mHelpButton;
	TextView mGuestNameView;

	private int hiddenLaunch = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		mGuestNameView = (TextView) findViewById(R.id.guest_name_view);
		LoginUser user = Nissan360.application.getLoggedInUser();
		String name;
		if(user.Name != null && user.Name != ""){
			name = user.Name;
		} else {
			name = user.LoginId;
		}
		mGuestNameView.setText(name);
		
		mDriveButton = (ImageButton) findViewById(R.id.drive_button);
		mDriveButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.VEHICLE_SELECTOR");
				startActivity(i);

			}

		});

		mDisplaysButton = (ImageButton) findViewById(R.id.displays_button);



		mDisplaysButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.VEHICLE_DISPLAYS");
				startActivity(i);

			}

		});

		mDisplaysButton.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View v) {
				hiddenLaunch++;
				if(hiddenLaunch>1){
					launchHiddenActivityIfActivated();
				}
				return false;
			}

		});

		
		mDisplaysButton.setOnTouchListener(new OnTouchListener () {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
					//.Log.d("TouchTest", "Touch down");
				} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
					hiddenLaunch--;
				}				
				return false;
			}
		});


		mAgendaButton = (ImageButton) findViewById(R.id.agenda_button);
		mAgendaButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.AGENDA");
				startActivity(i);

			}

		});


		mPOIMapButton = (ImageButton) findViewById(R.id.poi_map_button);
		mPOIMapButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.POI_MAP");
				startActivity(i);

			}

		});


		mMediaButton = (ImageButton) findViewById(R.id.media_button);
		mMediaButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.MEDIA");
				startActivity(i);

			}

		});


		mHelpButton = (ImageButton) findViewById(R.id.help_button);
		mHelpButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent i = new Intent("com.amci.nissan360.action.HELP");
				startActivity(i);

			}

		});

		mHelpButton.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View v) {
				hiddenLaunch++;
				if(hiddenLaunch>1){
					launchHiddenActivityIfActivated();
				}
				return false;
			}

		});


		mHelpButton.setOnTouchListener(new OnTouchListener () {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
					//.Log.d("TouchTest", "Touch down");
				} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
					hiddenLaunch--;
				}				
				return false;
			}
		});
	}

	protected void launchHiddenActivityIfActivated() {
		if(hiddenLaunch > 1){
			Intent i = new Intent(this, HiddenPanel.class);
			startActivity(i);
			hiddenLaunch=0;
		}

	}
	
	

	@Override
	protected void onResume() {
		super.onResume();
		hiddenLaunch = 0;
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {	
		com.actionbarsherlock.view.MenuInflater mi=getSupportMenuInflater();
		mi.inflate(R.menu.dashboard, menu);
		return true;
	}


}

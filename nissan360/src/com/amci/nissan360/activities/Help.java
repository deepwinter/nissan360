package com.amci.nissan360.activities;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.actionbarsherlock.view.MenuInflater;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;


public class Help extends Nissan360NavigationActivity {
	
	public static String INTENT_ACTION = "com.amci.nissan360.action.HELP";
	private ViewFlipper flipper;
	private SimpleOnGestureListener simpleOnGestureListener;
	private GestureDetector gestureDetector;

	
	private LayoutInflater layoutInflater;

	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		
	
		animFlipInForeward = AnimationUtils.loadAnimation(this, R.anim.flipin);
		animFlipOutForeward = AnimationUtils.loadAnimation(this, R.anim.flipout);
		animFlipInBackward = AnimationUtils.loadAnimation(this, R.anim.flipin_reverse);
		animFlipOutBackward = AnimationUtils.loadAnimation(this, R.anim.flipout_reverse);
		
		flipper = new ViewFlipper(this);

		layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		
		View view = layoutInflater.inflate(R.layout.view_help_page, null);
		ImageView image = (ImageView) view.findViewById(R.id.help_image);
		image.setImageResource(R.drawable.how_to_tap);
		flipper.addView(view);
		
		view = layoutInflater.inflate(R.layout.view_help_page, null);
		image = (ImageView) view.findViewById(R.id.help_image);
		image.setImageResource(R.drawable.help_map);
		flipper.addView(view);
		
		
		ArrayList<View> previousButtons = Utilities.getViewsByTag(flipper, "previous_button");
		for(View buttonView : previousButtons){
			ImageButton button = (ImageButton) buttonView;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeRight();
				}

			});
		}

		ArrayList<View> nextButtons = Utilities.getViewsByTag(flipper, "next_button");
		for(View buttonView : nextButtons){
			ImageButton button = (ImageButton) buttonView;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeLeft();
				}

			});
		}
		
		RelativeLayout container = (RelativeLayout) findViewById(R.id.help_flipper_container);
		container.addView(flipper);
		
		flipper.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		});

		
		simpleOnGestureListener = new SimpleOnGestureListener(){

			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
					float velocityY) {

				float sensitvity = 50;
				if((e1.getX() - e2.getX()) > sensitvity){
					SwipeLeft();
				}else if((e2.getX() - e1.getX()) > sensitvity){
					SwipeRight();
				}

				return true;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return true;
			}
		};

		gestureDetector= new GestureDetector(this, simpleOnGestureListener);

	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {

		MenuInflater mi= getSupportMenuInflater();
		mi.inflate(R.menu.help, menu);

		setupSpinner(menu);
		
		return true;
	}

	private void SwipeRight(){
		flipper.setInAnimation(animFlipInBackward);
		flipper.setOutAnimation(animFlipOutBackward);
		flipper.showPrevious();
	}

	private void SwipeLeft(){
		flipper.setInAnimation(animFlipInForeward);
		flipper.setOutAnimation(animFlipOutForeward);
		flipper.showNext();
	}

}

package com.amci.nissan360.activities;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;



import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;
import net.winterroot.hzsv.CarouselAdapter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.NissanSettings;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.api.configuration.Colors;
import com.amci.nissan360.api.configuration.TrackDescriptions;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.Nissan360Database;
import com.amci.nissan360.database.PointsOfInterestContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.map.OfflineDirectoryMapTileProvider;
import com.amci.nissan360.provider.PointsOfInterestProvider;
import com.amci.nissan360.provider.VehiclesProvider;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;

public class PoiMapFragment extends SupportMapFragment implements GoogleMap.OnCameraChangeListener, LoaderManager.LoaderCallbacks<Cursor>  {

	GoogleMap map;

	private static final int POI_ITEMS_LOADER = 2002;
	private static final int VEHICLE_DISPLAYS_LOADER = 2003;

	LoaderManager lm;

	List<Marker> markers;

	private static final String SELECT_STRING_ALL = "";
	private static final String SELECT_STRING_TRACKS = Nissan360Database.TABLE_POIS+".type = 'Tracks'";
	private static final String SELECT_STRING_GUEST_SERVICES = Nissan360Database.TABLE_POIS+".type = 'Guest Services'";
	private static final String SELECT_STRING_NONE = "SELECT_STRING_NONE";


	String selectString = SELECT_STRING_ALL;

	private Colors colors = null;



	@Override
	public void onStart() {
		super.onStart();

		// load the colors
		try {
			JSONDatabase configDatabase = Nissan360.getConfigDatabase();
			SearchFields search = SearchFields.Where("Type", Colors.COLORS_TYPE);
			Collection<JSONEntity> entities =  configDatabase.fetchByFields(search);
			Iterator<JSONEntity> i = entities.iterator();
			if(i.hasNext()){
				JSONEntity e = i.next();
				colors = (Colors) e.asClass(Colors.class);
			}

		} catch (InitJSONDatabaseExcepiton e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		markers = new ArrayList<Marker>();

		lm = getActivity().getSupportLoaderManager();
		lm.initLoader(POI_ITEMS_LOADER, null, this);
		lm.initLoader(VEHICLE_DISPLAYS_LOADER, null, this);

		map = getMap();
		if(map != null){
			map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			map.setMyLocationEnabled(true);

			LatLng latLng = new LatLng(NissanSettings.latitude,NissanSettings.longitude);
			CameraPosition position = new CameraPosition(latLng, 18, 0, 40);
			map.moveCamera( CameraUpdateFactory.newCameraPosition(position) );

			TileOverlayOptions tp  = new TileOverlayOptions().tileProvider(
					new OfflineDirectoryMapTileProvider(
							getActivity().getApplicationContext().getFilesDir().getPath() + "/nissan360/maps/event_site"));
			map.addTileOverlay(tp);

			map.setOnCameraChangeListener(this);
		} else {
			// Delay the reload of this map or something?
			// the view doesn't appear to be loaded yet.  or something
		}
	}



	@Override
	public void onCameraChange(CameraPosition position) {
		float maxZoom = 20.0f;
		if (position.zoom > maxZoom) {
			map.animateCamera(CameraUpdateFactory.zoomTo(maxZoom));
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {

		switch(id){
		case POI_ITEMS_LOADER:
			return new CursorLoader(getActivity(), 
					PointsOfInterestProvider.CONTENT_URI_GEO_ASSETS,
					PointsOfInterestProvider.getGeoAssetsProjection(),
					selectString, null, null);

		case VEHICLE_DISPLAYS_LOADER:
			return new CursorLoader(getActivity(), 
					VehiclesProvider.CONTENT_URI_GEO_ASSETS,
					VehiclesProvider.getGeoAssetsProjection(),
					"Track = 'Display'", null, null);

		default:
			return null;
		}

	}


	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if(map == null){
			return;
		}

		switch(loader.getId()){
		case POI_ITEMS_LOADER:

			for(int i=0; i<cursor.getCount(); i++){
				cursor.moveToPosition(i);
				MarkerOptions options = new MarkerOptions();
				options.title(cursor.getString(cursor.getColumnIndex(PointsOfInterestContract.TITLE_FIELD)));
				double latitude = cursor.getDouble(cursor.getColumnIndex(AssetsContract.LATITUDE_FIELD));
				double longitude = cursor.getDouble(cursor.getColumnIndex(AssetsContract.LONGITUDE_FIELD));
				LatLng latLng = new LatLng(latitude,longitude);

				if(latLng != null) {
					options.position(latLng);
					String subtype = cursor.getString(cursor.getColumnIndex(PointsOfInterestContract.SUBTYPE_FIELD));
					if(subtype != null && colors != null) {
						String color = colors.getColor(Colors.COLOR_MAP_KIND, subtype);
						if(color != null ){
							Bitmap icon = Utilities.getMarkerForColor(getActivity(), color);
							if(icon!=null){
								BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(icon);
								options.icon(descriptor);
							}
						}
					}

					Marker m = map.addMarker(options);
					markers.add(m);
				}
			}
			break;

		case VEHICLE_DISPLAYS_LOADER:
			for(int i=0; i<cursor.getCount(); i++){
				cursor.moveToPosition(i);
				MarkerOptions options = new MarkerOptions();
				options.title(cursor.getString(cursor.getColumnIndex(VehiclesContract.MODEL_FIELD)));
				double latitude = cursor.getDouble(cursor.getColumnIndex(AssetsContract.LATITUDE_FIELD));
				double longitude = cursor.getDouble(cursor.getColumnIndex(AssetsContract.LONGITUDE_FIELD));
				LatLng latLng = new LatLng(latitude,longitude);
				if(latLng != null) {
					options.position(latLng);
					
					Bitmap icon = Utilities.getMarkerForColor(getActivity(), "default");
					if(icon!=null){
						BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(icon);
						options.icon(descriptor);
					}
					
					Marker m = map.addMarker(options);
					markers.add(m);
				}
			}
			break;
		default:
			break;
		}

	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		Iterator<Marker> i = markers.iterator();
		while(i.hasNext()){
			Marker m = i.next();
			m.remove();
		}
		markers.removeAll(markers);
	}


	private void removeAllMarkers(){
		Iterator<Marker> i = markers.iterator();
		while(i.hasNext()){
			Marker m = i.next();
			m.remove();
		}
		markers.removeAll(markers);
	}

	public void showAllPoints() {
		removeAllMarkers();
		selectString = SELECT_STRING_ALL;
		lm.restartLoader(POI_ITEMS_LOADER, null, this);
		lm.restartLoader(VEHICLE_DISPLAYS_LOADER, null, this);
	}



	public void showTracksOnly() {
		removeAllMarkers();
		selectString = SELECT_STRING_TRACKS;
		lm.restartLoader(POI_ITEMS_LOADER, null, this);		
	}



	public void showVehicleExhibitsOnly() {
		removeAllMarkers();
		selectString = SELECT_STRING_NONE;
		lm.restartLoader(VEHICLE_DISPLAYS_LOADER, null, this);

	}



	public void showGuestServicesOnly() {
		removeAllMarkers();
		selectString = SELECT_STRING_GUEST_SERVICES;
		lm.restartLoader(POI_ITEMS_LOADER, null, this);			
	}

}

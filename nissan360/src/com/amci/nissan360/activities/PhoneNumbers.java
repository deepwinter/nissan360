package com.amci.nissan360.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONArray;
import org.json.JSONException;

import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;

import com.actionbarsherlock.app.SherlockActivity;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.api.configuration.Contact;
import com.amci.nissan360.api.configuration.ContactList;
import com.amci.nissan360.api.configuration.TrackDescriptions;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PhoneNumbers extends SherlockActivity {

	ListView mListView;
	ArrayList<String> numbers = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_phone_numbers);

		mListView = (ListView) findViewById(R.id.phone_number_list);
		ArrayAdapter<String> contactsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

		JSONDatabase configDatabase;
		try {
			configDatabase = Nissan360.getConfigDatabase();
		} catch (InitJSONDatabaseExcepiton e1) {
			e1.printStackTrace();
			return;
		}
		SearchFields search = SearchFields.Where("Type", ContactList.CONTACTS_TYPE);
		Collection<JSONEntity> entities =  configDatabase.fetchByFields(search);
		Iterator<JSONEntity> i = entities.iterator();
		if(!i.hasNext()){
			return;
		}
		JSONEntity contactListEntity = i.next();
		ContactList contactList;
		try {
			contactList = (ContactList) contactListEntity.asClass(ContactList.class);

			for(int j=0; j<contactList.contacts.size(); j++){
					Contact contact;
					contact = contactList.contacts.get(j);
					numbers.add(contact.phone_number);
					contactsAdapter.add(contact.name);
			
			}
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	

		mListView.setAdapter(contactsAdapter);

		mListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + numbers.get(arg2)));
				PhoneNumbers.this.startActivity(intent);	
				finish();
			}

		});


	}



}

package com.amci.nissan360.activities;

import java.util.ArrayList;
import java.util.List;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.R.layout;
import com.amci.nissan360.R.menu;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.helper.SmartPosterHelper;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.AssetsLogProvider;
import com.amci.nissan360.provider.VehiclesProvider;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Media extends Nissan360NavigationActivity {

	ListView mediaListView;
	List<String> assets;
	List<String> assetTypes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_media);

		mediaListView = (ListView) findViewById(R.id.media_log_listview);

		mediaListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				String type = assetTypes.get(position);
				String assetId = assets.get(position);
				if(type != null && assetId != null){
					if(type.equals(AssetsContract.SMART_POSTER_TYPE)){
						SmartPosterHelper.launchSmartPoster(Media.this, assetId);
					} else if(type.equals(AssetsContract.VEHICLE_TYPE)){
						Cursor c = getContentResolver().query(Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, assetId),
								VehiclesProvider.getDefaultProjection(), null, null, null);
						if(c.getCount() > 0){
							c.moveToPosition(0);
							VehiclesHelper.startExperienceActivityForVehicle(Media.this, c);
						} else {
							Toast.makeText(Media.this, "Vehicle Not Found", Toast.LENGTH_SHORT).show();
						}
					}
				}
				
			}
			
		});
		
	}

	@Override
	public void onResume(){
		super.onResume();
		
		Cursor c = getContentResolver().query(AssetsLogProvider.CONTENT_URI, 
				AssetsLogProvider.getDefaultProjection(),
				"(" + AssetsLogContract.TYPE_FIELD + "=  'SmartPoster'"
						+ " OR " + AssetsLogContract.TYPE_FIELD + "=  'Vehicle' )"
						+ " AND " + AssetsLogContract.ATTENDEE_ID_FIELD + " = '"+ Nissan360.getLoggedInUser().LoginId +"'",
						null, AssetsLogContract.TIMESTAMP_FIELD);

		assets = new ArrayList<String>();
		assetTypes = new ArrayList<String>();
		List<String> names = new ArrayList<String>();
		for(int i=0; i<c.getCount(); i++){
			c.moveToPosition(i);
			if(!assets.contains(c.getString(c.getColumnIndex(AssetsLogContract.ASSET_ID_FIELD)))){
				assets.add(c.getString(c.getColumnIndex(AssetsLogContract.ASSET_ID_FIELD)));
				assetTypes.add(c.getString(c.getColumnIndex(AssetsLogContract.TYPE_FIELD)));
				String name = c.getString(c.getColumnIndex(AssetsLogContract.MEDIA_NAME_FIELD));
				if(name == null){
					name = c.getString(c.getColumnIndex(AssetsLogContract.ASSET_ID_FIELD));
				}
				names.add(name);

			}
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
		mediaListView.setAdapter(adapter);
		
		try {
			Nissan360.application.getNissanApiService().sendAllAssetLogItems();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
	
	}


}

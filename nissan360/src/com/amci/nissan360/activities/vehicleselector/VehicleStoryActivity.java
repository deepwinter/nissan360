package com.amci.nissan360.activities.vehicleselector;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockFragment;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.api.Vehicle;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.state.ApplicationState;
import com.amci.nissan360.state.ReservationState;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;


public class VehicleStoryActivity extends Nissan360NavigationActivity {


	public static final String INTENT_ACTION = "com.amci.nissan360.action.VEHICLE_STORIES";
	public static final String INTENT_EXTRA_MODE = "INTENT_EXTRA_MODE";
	public static final String MODE_NORMAL = "MODE_NORMAL";
	public static final String MODE_START_DRIVE = "MODE_START_DRIVE";


	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;

	ViewFlipper flipper;
	private SimpleOnGestureListener simpleOnGestureListener;
	private GestureDetector gestureDetector;

	private LayoutInflater layoutInflater;

	private String mode = MODE_NORMAL;
	private Button learnButton;
	private Button mediaButton;
	private Button driveButton;
	private Cursor vehicleCursor;

	protected boolean loadStoryViewOnCreate = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(loadStoryViewOnCreate){
			if(getIntent() != null && getIntent().getExtras() != null ) {
				String vin = getIntent().getExtras().getString(VehicleIntentContract.INTENT_EXTRA_VIN);
				loadStoryView(vin);
			}
		}

	}

	protected void loadStoryView(String vin) {
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			mode = extras.getString(INTENT_EXTRA_MODE, MODE_NORMAL);
		}

		setContentView(R.layout.activity_vehicle_story);
		flipper = new ViewFlipper(this);

		layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// get the VIN and load the vehicle
		vehicleCursor = getContentResolver().query(Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, vin), VehiclesProvider.getDefaultProjection(), null, null, null);
		if(vehicleCursor.getCount() < 1){
			return;
		}

		vehicleCursor.moveToPosition(0);

		if(ApplicationState.isCheckedIn(this)){
			addSpecSheeToFlipperIfExists();
		}

		if( ! vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY1_TEXT_FIELD)).contentEquals("")){
			addStoryToFlipper(
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.NAME_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY1_NAME_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY1_TEXT_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY1_IMAGE_FIELD))
					);
		}
		if( ! vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY2_TEXT_FIELD)).contentEquals("")){
			addStoryToFlipper(
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.NAME_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY2_NAME_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY2_TEXT_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY2_IMAGE_FIELD))
					);
		}
		if( ! vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY3_TEXT_FIELD)).contentEquals("")){
			addStoryToFlipper(
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.NAME_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY3_NAME_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY3_TEXT_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.STORY3_IMAGE_FIELD))
					);
		}

		if(!ApplicationState.isCheckedIn(this)){
			addSpecSheeToFlipperIfExists();
		}


		ArrayList<View> previousButtons = Utilities.getViewsByTag(flipper, "previous_button");
		for(View view : previousButtons){
			ImageButton button = (ImageButton) view;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeRight();
				}

			});
		}

		ArrayList<View> nextButtons = Utilities.getViewsByTag(flipper, "next_button");
		for(View view : nextButtons){
			ImageButton button = (ImageButton) view;
			button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SwipeLeft();
				}

			});
		}


		animFlipInForeward = AnimationUtils.loadAnimation(this, R.anim.flipin);
		animFlipOutForeward = AnimationUtils.loadAnimation(this, R.anim.flipout);
		animFlipInBackward = AnimationUtils.loadAnimation(this, R.anim.flipin_reverse);
		animFlipOutBackward = AnimationUtils.loadAnimation(this, R.anim.flipout_reverse);

		flipper.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		});

		RelativeLayout container = (RelativeLayout) findViewById(R.id.vehicle_story_flipper_container);
		container.addView(flipper);

		simpleOnGestureListener = new SimpleOnGestureListener(){

			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
					float velocityY) {

				float sensitvity = 50;
				if((e1.getX() - e2.getX()) > sensitvity){
					SwipeLeft();
				}else if((e2.getX() - e1.getX()) > sensitvity){
					SwipeRight();
				}

				return true;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return true;
			}
		};

		gestureDetector= new GestureDetector(this, simpleOnGestureListener);


		// Set up the buttons

		learnButton = (Button) findViewById(R.id.vehicle_story_learn_button);

		mediaButton = (Button) findViewById(R.id.vehicle_story_media_button);
		mediaButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(vehicleCursor == null 
						|| vehicleCursor.getCount() < 1
						){
					return;
				}

				final String vehicleName = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.NAME_FIELD));
				final String vin = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.VIN_FIELD));

				VehicleInteractionResponder.respondToMediaTap(VehicleStoryActivity.this, vehicleName, vin);

			}



		});

		driveButton = (Button) findViewById(R.id.vehicle_start_drive_button);
		if(vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.TRACK_FIELD)).equals(VehiclesContract.TRACK_TYPE_DISPLAY)){
			driveButton.setEnabled(false);
		} else {
			if(ApplicationState.isCheckedIn(this)){
				driveButton.setText("Start");


				driveButton.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {

						if(vehicleCursor == null 
								|| vehicleCursor.getCount() < 1
								){
							return;
						}


						Bundle extras = VehicleStoryActivity.this.getIntent().getExtras();
						if(extras == null){
							Toast.makeText(VehicleStoryActivity.this, "No Track Specified", Toast.LENGTH_SHORT).show();
							return;
						}
						String routeFile = extras.getString(VehicleIntentContract.INTENT_EXTRA_TRACK);
						if(routeFile == null){
							Toast.makeText(VehicleStoryActivity.this, "Track File not Specified", Toast.LENGTH_SHORT).show();
							return;
						}

						ApplicationState.moveToDriveMode(VehicleStoryActivity.this, routeFile);

						finish();
					}

				});

			} else {
				driveButton.setText("Drive");

				driveButton.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {

						if(vehicleCursor == null 
								|| vehicleCursor.getCount() < 1
								){
							return;
						}

						VehiclesHelper.startExperienceActivityForVehicle(VehicleStoryActivity.this, vehicleCursor);
					}

				});

			}
		}
	}

	private void addSpecSheeToFlipperIfExists() {
		if ( ! vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.SPEC_SHEET_IMAGE_FIELD)).contentEquals("") ){
			addSpecSheetToFlipper(
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.SPEC_SHEET_IMAGE_FIELD)),
					vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.SPEC_SHEET_FIELD))
					);
		}		
	}

	// Note: fields are named backwards somewhere, definitely in layout file
	private void addSpecSheetToFlipper(String specSheetImage , String specSheetVehicleImage) {
		View view = layoutInflater.inflate(R.layout.view_vehicle_spec_sheet, null);
		ImageView specSheetVehicleImageView = (ImageView) view.findViewById(R.id.vehicle_spec_sheet_vehicle_image);
		boolean rval;
		rval = Utilities.setImageBitmapNissan360(this, specSheetVehicleImageView, "vehicles/"+specSheetVehicleImage);
		if(!rval){
			Utilities.setImageBitmapNissan360(this, specSheetVehicleImageView, "notavail_specs.png");
		}

		ImageView specSheetImageView = (ImageView) view.findViewById(R.id.vehicle_spec_sheet_image);
		rval = Utilities.setImageBitmapNissan360(this, specSheetImageView, "vehicles/"+specSheetImage);
		if(!rval){
			Utilities.setImageBitmapNissan360(this, specSheetVehicleImageView, "notavail_hero.png");
		}

		flipper.addView(view);
	}

	private void addStoryToFlipper(String model, String title, String storyText, String image) {
		View view = layoutInflater.inflate(R.layout.view_vehicle_story, null);
		TextView nameView = (TextView) view.findViewById(R.id.vehicle_story_vehicle_name);
		nameView.setText(model);
		TextView storyTitleView = (TextView) view.findViewById(R.id.vehicle_story_name);
		storyTitleView.setText(title);
		TextView vehicleTextView = (TextView) view.findViewById(R.id.vehicle_story_text);
		vehicleTextView.setText(storyText);
		ImageView imageView = (ImageView) view.findViewById(R.id.vehicle_story_image);
		
		boolean rval;
		rval = Utilities.setImageBitmapNissan360(this, imageView, "vehicles/"+image);
		if(!rval){
			Utilities.setImageBitmapNissan360(this, imageView, "notavail_hero.png");
		}


		flipper.addView(view);

	}

	private void SwipeRight(){
		flipper.setInAnimation(animFlipInBackward);
		flipper.setOutAnimation(animFlipOutBackward);
		flipper.showPrevious();
	}

	private void SwipeLeft(){
		flipper.setInAnimation(animFlipInForeward);
		flipper.setOutAnimation(animFlipOutForeward);
		flipper.showNext();
	}




}

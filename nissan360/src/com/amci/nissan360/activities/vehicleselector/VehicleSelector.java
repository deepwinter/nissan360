package com.amci.nissan360.activities.vehicleselector;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragment;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.VehiclesProvider;

public class VehicleSelector extends Nissan360NavigationActivity {


	public static final String INTENT_ACTION = "com.amci.nissan360.action.VEHICLE_SELECTOR";
	public static final String INTENT_EXTRA_SELECTOR_BUCKET = "INTENT_EXTRA_SELECTOR_BUCKET";
	public static final String BUCKET_DRIVE = VehiclesContract.BUCKET_DRIVE_TYPE;
	public static final String BUCKET_DISPLAY = VehiclesContract.BUCKET_DISPLAY_TYPE;
	private static final String BUNDLE_CATEGORY_INDEXES = "BUNDLE_CATEGORY_INDEXES";
	private static final String BUNDLE_VEHICLE_INDEXES = "BUNDLE_VEHICLE_INDEXES";
	private static final String BUNDLE_CURRENT_TAB = "BUNDLE_CURRENT_TAB";
	private static final int NO_TAB_SELECTED = 9999;

	VehicleSelectorFragment[] fragments;
	FragmentManager fragmentManager;
	int[] currentCategoryIndexes;
	int [] currentVehicleIndexes;
	int currentTab = NO_TAB_SELECTED;

	ImageButton brandButton1;
	ImageButton brandButton2;
	ImageButton brandButton3;
	ImageButton brandButton4;


	String selectorType;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vehicle_selector);

		fragmentManager = getSupportFragmentManager();

		if(savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_CATEGORY_INDEXES)){
			currentCategoryIndexes = savedInstanceState.getIntArray(BUNDLE_CATEGORY_INDEXES);
		} else {
			int[] init = {0, 0, 0, 0};
			currentCategoryIndexes = init;
		}
		if(savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_VEHICLE_INDEXES)){
			currentVehicleIndexes = savedInstanceState.getIntArray(BUNDLE_VEHICLE_INDEXES);
		} else {
			int[] init = {0, 0, 0, 0};
			currentVehicleIndexes = init;
		}
		if(savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_CURRENT_TAB)){
			currentTab = savedInstanceState.getInt(BUNDLE_CURRENT_TAB);
		}

		fragments = new VehicleSelectorFragment[4];
		fragments[0] = new VehicleSelectorFragment("Nissan", selectorType, currentCategoryIndexes[0], currentVehicleIndexes[0] );
		fragments[1] = new VehicleSelectorFragment("Infiniti", selectorType, currentCategoryIndexes[1], currentVehicleIndexes[1] );
		fragments[2] = new VehicleSelectorFragment("Datsun", selectorType, currentCategoryIndexes[2], currentVehicleIndexes[2]);
		fragments[3] = new VehicleSelectorFragment(VehiclesProvider.BRAND_PARTNER_TYPE, selectorType, currentCategoryIndexes[3], currentVehicleIndexes[3]);	

		brandButton1 = (ImageButton) findViewById(R.id.vehicle_selector_brand_button1);
		brandButton1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTab(0);
				brandButton1.setSelected(true);
				brandButton2.setSelected(false);
				brandButton3.setSelected(false);
				brandButton4.setSelected(false);

			}

		});

		brandButton2 = (ImageButton) findViewById(R.id.vehicle_selector_brand_button2);
		brandButton2.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTab(1);
				brandButton1.setSelected(false);
				brandButton2.setSelected(true);
				brandButton3.setSelected(false);
				brandButton4.setSelected(false);
			}

		});

		brandButton3 = (ImageButton) findViewById(R.id.vehicle_selector_brand_button3);
		brandButton3.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTab(2);
				brandButton1.setSelected(false);
				brandButton2.setSelected(false);
				brandButton3.setSelected(true);
				brandButton4.setSelected(false);
			}

		});

		brandButton4 = (ImageButton) findViewById(R.id.vehicle_selector_brand_button4);
		brandButton4.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTab(3);
				brandButton1.setSelected(false);
				brandButton2.setSelected(false);
				brandButton3.setSelected(false);
				brandButton4.setSelected(true);
			}

		});

		if(currentTab == NO_TAB_SELECTED){
			brandButton1.setSelected(true);
		}
	}

	public void onStart(){
		super.onStart();
	}

	public void onResume(){
		super.onResume();
		showTab(currentTab);
	}

	public void registerCarouselState(VehicleSelectorFragment fragment, int currentCategoryIndex, int currentVehicleIndex){
		// find the relevant fragment
		int NO_MATCH = 999;
		int match = NO_MATCH;
		for(int i=0; i<4; i++){
			if(fragment  == fragments[i]){
				match = i;
			}
		}
		if(match == NO_MATCH){
			Toast.makeText(this, "Not working", Toast.LENGTH_SHORT).show();
			return;
		}
		currentCategoryIndexes[match] = currentCategoryIndex;
		currentVehicleIndexes[match] = currentVehicleIndex;

	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putIntArray(BUNDLE_CATEGORY_INDEXES, currentCategoryIndexes);
		outState.putIntArray(BUNDLE_VEHICLE_INDEXES, currentVehicleIndexes);
		outState.putInt(BUNDLE_CURRENT_TAB, currentTab);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState.containsKey(BUNDLE_CATEGORY_INDEXES)){
			currentCategoryIndexes = savedInstanceState.getIntArray(BUNDLE_CATEGORY_INDEXES);
			fragments[0].selectedCategoryIndex = currentCategoryIndexes[0];
			fragments[1].selectedCategoryIndex = currentCategoryIndexes[1];
			fragments[2].selectedCategoryIndex = currentCategoryIndexes[2];
			fragments[3].selectedCategoryIndex = currentCategoryIndexes[3];

		}
		if(savedInstanceState.containsKey(BUNDLE_VEHICLE_INDEXES)){
			currentVehicleIndexes = savedInstanceState.getIntArray(BUNDLE_VEHICLE_INDEXES);
			fragments[0].selectedVehicleIndex = currentVehicleIndexes[0];
			fragments[1].selectedVehicleIndex = currentVehicleIndexes[1];
			fragments[2].selectedVehicleIndex = currentVehicleIndexes[2];
			fragments[3].selectedVehicleIndex = currentVehicleIndexes[3];
		}

	}

	Object lock = new Object();
	
	public void showTab(int position){
		FragmentTransaction transaction;

		
		if(position == NO_TAB_SELECTED){
			position = 0;
		} else if (position == currentTab){
			return;
		}
		
		synchronized(lock){

			// Replace whatever is in the fragment_container view with this fragment,
			// and add the transaction to the back stack
			switch (position){
			case 0:
				transaction = fragmentManager.beginTransaction();
				transaction.replace(R.id.vehicle_selector_container, fragments[0]);
				transaction.commit();
				break;

			case 1:
				transaction = fragmentManager.beginTransaction();
				transaction.replace(R.id.vehicle_selector_container, fragments[1]);
				transaction.commit();
				break;

			case 2:
				transaction = fragmentManager.beginTransaction();
				transaction.replace(R.id.vehicle_selector_container, fragments[2]);
				transaction.commit();
				break;

			case 3:
				transaction = fragmentManager.beginTransaction();
				transaction.replace(R.id.vehicle_selector_container, fragments[3]);
				transaction.commit();
				break;
			}	
			currentTab = position;
			getSupportFragmentManager().executePendingTransactions();
		}
	}


	/*
	public void moveToFragment(SherlockFragment fragment){
		FragmentTransaction transaction;
		transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.vehicle_selector_container, fragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}
	 */




}

package com.amci.nissan360.activities.vehicleselector;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.Nissan360Activity;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.provider.AssetsLogProvider;
import com.amci.nissan360.services.SendAssetLogListener;

public class VehicleInteractionResponder {

	public static void respondToMediaTap(final Nissan360Activity context,  final String vehicleName,  final String vin) {
		Cursor c = context.getContentResolver().query(
				Uri.withAppendedPath(AssetsLogProvider.CONTENT_URI_BY_ASSET_ID, vin), 
				AssetsLogProvider.getDefaultProjection(),
				null, null, null);
		if(c.getCount() > 0){
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Media for " + vehicleName + " has already been added to your briefcase");
			builder.setPositiveButton("Ok", null);
			builder.create().show();
			return;
		}

		/* The onClicks should call a handler
		 * If you extend AlertDialog this is possible
		 * but alert dialog's constructors are private
		 */
		AlertDialog dialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Do you want to add media for " + vehicleName + " to your briefcase?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {


				if(dialog != null){
					dialog.dismiss();
				}
				applyMediaToBriefacase(context, vehicleName, vin);

			}

		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(dialog != null){
					dialog.dismiss();
				}
			}

		});
		dialog = builder.create();
		dialog.show();
	}
	
	
	private static void applyMediaToBriefacase(final Nissan360Activity context, final String vehicleName,
			String vin) {
		
		class AddMediaListener implements SendAssetLogListener {

			@Override
			public void assetLogCached() {

				class ConfirmSendMediaDialogFragment extends DialogFragment {
					@Override
					public Dialog onCreateDialog(Bundle savedInstanceState) {

						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setMessage("Content could not be shared, will retry later.  Admin will be notified").setTitle("Media Queued");

						// Add the buttons
						builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								ConfirmSendMediaDialogFragment.this.dismiss();
							}
						});

						AlertDialog dialog = builder.create();
						return dialog;
					}
				}

				ConfirmSendMediaDialogFragment dialog = new ConfirmSendMediaDialogFragment();
				dialog.show(context.getSupportFragmentManager(), "dialog");

			}

			@Override
			public void assetLogSend() {

				class ConfirmSendMediaDialogFragment extends DialogFragment {
					@Override
					public Dialog onCreateDialog(Bundle savedInstanceState) {

						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setMessage("Media content for " + vehicleName + " has been sent to your Nissan 360 Briefcase").setTitle("Media Sent");

						// Add the buttons
						builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								ConfirmSendMediaDialogFragment.this.dismiss();
							}
						});

						AlertDialog dialog = builder.create();
						return dialog;
					}
				}

				ConfirmSendMediaDialogFragment dialog = new ConfirmSendMediaDialogFragment();
				dialog.show(context.getSupportFragmentManager(), "dialog");

			}

		}

		try {
			Nissan360.application.getNissanApiService().logAssetTap(AssetsContract.VEHICLE_TYPE, vin, new AddMediaListener(), AssetsLogContract.INTERACTION_TYPE_BRIEFCASE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


				

	
}

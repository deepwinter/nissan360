package com.amci.nissan360.activities.vehicleselector;

import java.util.ArrayList;
import java.util.List;

import net.winterroot.hzsv.CarouselAdapter;
import net.winterroot.hzsv.HorizontalZoomingCarousel;
import net.winterroot.hzsv.HorizontalZoomingCarouselListener;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.base.Nissan360Activity;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.database.AssetsLogContract;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.helper.VehiclesHelper;
import com.amci.nissan360.provider.AssetsLogProvider;
import com.amci.nissan360.provider.VehiclesProvider;
import com.amci.nissan360.services.SendAssetLogListener;



public class VehicleSelectorFragment extends SherlockFragment implements HorizontalZoomingCarouselListener, LoaderManager.LoaderCallbacks<Cursor>  {

	String selectedVin;
	String brand;
	int startingIndex;

	Button learnButton;
	Button mediaButton;
	Button driveButton;
	TextView vehicleNameText;
	TextView mVehicleCategoryText;


	HorizontalZoomingCarousel vehicleTypeCarousel;
	HorizontalZoomingCarousel vehicleCarousel;

	// The callbacks through which we will interact with the LoaderManager.
	private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;
	private static int BRAND_ITEMS_LOADER_ID = 1;
	private static int VEHICLE_ITEMS_LOADER_ID = 100;
	// The adapter that binds our data to the Carousel
	private CarouselAdapter vehicleTypeAdapter;
	private CarouselAdapter vehicleAdapter;
	private Cursor vehicleCursor;
	private Cursor vehicleTypeCursor;

	private int MY_BRAND_LOADER_ID;
	private int MY_VEHICLES_LOADER_ID;

	public int selectedCategoryIndex = 0;
	public int selectedVehicleIndex = 0;




	public VehicleSelectorFragment(String brand, String bucket) {
		super();
		this.brand = brand;
	}


	public VehicleSelectorFragment(String brand, String selectorType, int startingCategoryIndex, int startingVehicleIndex) {
		super();
		this.brand = brand;
		selectedCategoryIndex = startingCategoryIndex;
		selectedVehicleIndex = startingVehicleIndex;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	View fragmentMainView = null;
	private int moveToVehicleOnLoad;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		// Attempting to 
		//if(fragmentMainView == null){
		fragmentMainView = inflater.inflate(R.layout.activity_vehicle_selector_fragment, container, false);
		//} 

		return fragmentMainView;
	}


	@Override
	public void onStart() {
		super.onStart();

		// onStart is called every time the fragment displays

		MY_BRAND_LOADER_ID = BRAND_ITEMS_LOADER_ID++;
		mCallbacks = this;
		LoaderManager lm = getActivity().getSupportLoaderManager();
		lm.initLoader(MY_BRAND_LOADER_ID, null, mCallbacks);
		vehicleTypeAdapter = new CarouselAdapter(getActivity(), null);

		MY_VEHICLES_LOADER_ID = VEHICLE_ITEMS_LOADER_ID++;
		vehicleAdapter = new CarouselAdapter(getActivity(), null);

		vehicleNameText = (TextView) getView().findViewById(R.id.vehicle_name_text);
		mVehicleCategoryText = (TextView) getView().findViewById(R.id.vehicle_name_name_view);

		learnButton = (Button) getView().findViewById(R.id.vehicle_learn_button);
		learnButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(vehicleCursor == null 
						|| vehicleCursor.getCount() < 1
						|| vehicleCursor.getCount() < selectedVehicleIndex ){
					return;
				}

				Intent i = new Intent(VehicleStoryActivity.INTENT_ACTION);
				vehicleCursor.moveToPosition(selectedVehicleIndex);

				i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.VIN_FIELD)));

				startActivity(i);
			}

		});
		mediaButton = (Button) getView().findViewById(R.id.vehicle_media_button);
		mediaButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(vehicleCursor == null 
						|| vehicleCursor.getCount() < 1
						|| vehicleCursor.getCount() < vehicleCarousel.getCurrentSelectedIndex() + 1 ){
					return;
				}


				int position = vehicleCarousel.getCurrentSelectedIndex();
				vehicleCursor.moveToPosition(position);
				final String vehicleName = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.NAME_FIELD));
				final String vin = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.VIN_FIELD));

				VehicleInteractionResponder.respondToMediaTap((Nissan360Activity) getActivity(), vehicleName, vin);


			}



		});

		driveButton = (Button) getView().findViewById(R.id.vehicle_drive_button);

		driveButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {

				if(vehicleCursor == null 
						|| vehicleCursor.getCount() < 1
						|| vehicleCursor.getCount() < vehicleCarousel.getCurrentSelectedIndex() + 1 ){
					return;
				}

				if(selectedVin == null){
					return;
				}
				int position = vehicleCarousel.getCurrentSelectedIndex();
				vehicleCursor.moveToPosition(position);
				VehiclesHelper.startExperienceActivityForVehicle(getActivity(), vehicleCursor);
			}

		});


		ViewGroup vg = (ViewGroup) getView().findViewById(R.id.vehicle_type_selector_container);
		if(vehicleTypeCarousel != null){
			vg.removeView(vehicleTypeCarousel);
		}
		int typeMarginFactor = 40;
		vehicleTypeCarousel = new HorizontalZoomingCarousel(getActivity(), 256 + typeMarginFactor, 115 + typeMarginFactor);
		vehicleTypeCarousel.setZoomEnabled(false);
		vehicleTypeCarousel.setAdapter(vehicleTypeAdapter);
		vehicleTypeCarousel.setListener(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, 115 + typeMarginFactor);
		params.topMargin = 40;
		vehicleTypeCarousel.setLayoutParams(params);
		vg.addView(vehicleTypeCarousel);



		ViewGroup vgi = (ViewGroup) getView().findViewById(R.id.vehicle_item_selector_container);
		if(vgi != null){
			vgi.removeView(vehicleCarousel);
			vehicleCarousel = null;
		}
		int marginFactor = 0;
		int width = (int) ( 538 ) + marginFactor;
		int height = (int) ( 350 ) + marginFactor;
		vehicleCarousel = new HorizontalZoomingCarousel(getActivity(), width, height);
		vehicleCarousel.setZoomEnabled(true);
		vehicleCarousel.matrixMultiplier = 1;
		vehicleCarousel.flingVelocity = 300;
		vehicleCarousel.setAdapter(vehicleAdapter);
		vehicleCarousel.setListener( new HorizontalZoomingCarouselListener(){


			@Override
			public void onItemSelected(int index) {
				setCurrentVehicle(index);

			}

			@Override
			public void onStartedMove() {
				// Don't change until the next one is selected
				// vehicleNameText.setText("");

			}

		});
		RelativeLayout.LayoutParams itemParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, height + marginFactor);
		itemParams.topMargin = 40;
		vehicleCarousel.setLayoutParams(itemParams);
		vgi.addView(vehicleCarousel);


	}



	



	protected void setCurrentVehicle(int index) {

		if(index < 0){
			return;
		}

		selectedVehicleIndex = index;
		if(vehicleCursor != null){

			if(index + 1 > vehicleCursor.getCount()){
				Exception e = new Exception("Index out of bounds");
				e.printStackTrace();
				return;
			}

			vehicleCursor.moveToPosition(index);
			int columnIndex = vehicleCursor.getColumnIndex(VehiclesContract.NAME_FIELD);
			String name = vehicleCursor.getString(columnIndex);
			vehicleNameText.setText(name);

			// And set selected VIN here
			selectedVin = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.VIN_FIELD));
			String bucket = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.BUCKET_FIELD));
			String track = vehicleCursor.getString(vehicleCursor.getColumnIndex(VehiclesContract.TRACK_FIELD));
			if(bucket.equals("Display") || track.equals("Display")){
				driveButton.setEnabled(false);
			} else {
				driveButton.setEnabled(true);
			}

		}
		VehicleSelector vehicleSelector = (VehicleSelector) getActivity();
		if(vehicleSelector != null){
			// Threading issues
			vehicleSelector.registerCarouselState(this, selectedCategoryIndex, selectedVehicleIndex);
		}
	}


	@Override
	public void onPause() {
		super.onPause();
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		// NEVER GETS CALLED
		super.onSaveInstanceState(outState);
	}




	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public void onDestroy() {
		// Called when fragment replaced
		super.onDestroy();
	}



	@Override
	public void onDestroyView() {
		// Called when fragment replaced
		super.onDestroyView();
		//freeMemory();

	}


	@Override
	public void onLowMemory() {
		super.onLowMemory();

		Toast.makeText(getActivity(), "Low memory", Toast.LENGTH_SHORT).show();

		freeMemory();

	}

	private void freeMemory(){

		// Kill the carousels!  They will be recreated onStart() anyway.
		vehicleTypeCarousel = null;
		vehicleCarousel = null;
	}


	// For the top scroller
	@Override
	public void onItemSelected(int index) {
		if( getActivity() == null){
			return;
		}

		if(index > vehicleTypeCursor.getCount()-1 || index < 0){
			// Fragment lifecycle issues, just ignore late threads
			return;
		}

		selectedCategoryIndex = index;

		vehicleTypeCursor.moveToPosition(index);
		String category = vehicleTypeCursor.getString(vehicleTypeCursor.getColumnIndex(VehiclesContract.CATEGORY_FIELD )) ;
		mVehicleCategoryText.setText( category );

		// Set the currently selected item type
		if(moveToVehicleOnLoad == 0){
			selectedVehicleIndex = 0;
		}
		LoaderManager lm = getActivity().getSupportLoaderManager();
		lm.restartLoader(MY_VEHICLES_LOADER_ID, null, mCallbacks);

	}

	@Override
	public void onStartedMove() {
		/*
		 * Do nothing when I start to move
		vehicleNameText.setText("");
		synchronized(this){
			if(vehicleCarousel != null){
				vehicleAdapter.swapCursor(null);
				vehicleCarousel.cursorUpdate();
			}
		}
		 */

	}




	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {

		if(id == MY_BRAND_LOADER_ID) {
			// Get the cursor for the given brand

			String[] projection = {
					VehiclesContract.CATEGORY_FIELD
			};
			// Create a new CursorLoader with the following query parameters.

			// TODO need to query by bucket
			return new CursorLoader(getActivity(), 
					Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_BRAND,brand),
					projection, null, null, null);
		} else {

			if(vehicleTypeCursor.getCount() == 0){
				return null;
			}

			if(vehicleTypeCursor.getCount() < selectedCategoryIndex + 1 ){
				return null;
			}
			vehicleTypeCursor.moveToPosition(selectedCategoryIndex);
			String type = vehicleTypeCursor.getString(vehicleTypeCursor.getColumnIndex(VehiclesContract.CATEGORY_FIELD));
			if(type == null){
				return null;
			}
			type = type.replace("/", "#SLASH#");
			String segment =  brand+"/"+type;

			return new CursorLoader(getActivity(), 
					Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_TYPE_AND_BRAND, segment),
					VehiclesProvider.getDefaultProjection(), null, null, VehiclesContract.MODEL_FIELD);

		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

		if(cursor.getCount() == 0){
			return;
		}

		if (loader.getId() == MY_BRAND_LOADER_ID) {

			if(vehicleTypeCarousel == null){
				return;
			}

			//Derive a cursor containing only the types in this brand

			String[] columns = { "_id",  VehiclesContract.CATEGORY_FIELD, CarouselAdapter.IMAGE_COLUMN };  // Cursor Adapter Required Columns
			MatrixCursor typesCursor = new MatrixCursor(columns);
			List<String> typesAdded = new ArrayList<String>();
			for(int i=0; i<cursor.getCount(); i++){
				cursor.moveToPosition(i);
				String category = cursor.getString(cursor.getColumnIndex(VehiclesContract.CATEGORY_FIELD) );
				if(!typesAdded.contains(category)){
					String imagePath = "vehicles/icon_" +  category.toLowerCase().replace("/", "_") + ".png";
					String[] columnValues = { String.valueOf(i), category, imagePath };
					typesCursor.addRow(columnValues);
					typesAdded.add(category);
				}
			}
			vehicleTypeCursor = typesCursor;

			vehicleTypeAdapter.swapCursor(typesCursor);
			vehicleTypeCarousel.cursorUpdate();

			if(selectedCategoryIndex < cursor.getCount()){
				vehicleTypeCarousel.scrollToPage(selectedCategoryIndex);
				if(selectedVehicleIndex != 0){
					// This means we are loading with some saved instance state
					moveToVehicleOnLoad = selectedVehicleIndex;
				}
				if(selectedCategoryIndex == 0){
					// This needs to be manually triggered
					onItemSelected(selectedCategoryIndex);
				}
			}

		} else {

			if(vehicleCarousel == null){
				return;
			}

			// it's the item adapter
			vehicleCursor = cursor;

			try {
				String[] columns = { "_id", CarouselAdapter.IMAGE_COLUMN };
				MatrixCursor imagesCursor = new MatrixCursor(columns);
				for(int i=0; i<cursor.getCount(); i++){
					cursor.moveToPosition(i);
					String imageName = cursor.getString(cursor.getColumnIndex(VehiclesContract.THUMBNAIL_FIELD) );
					String imagePath = "vehicles/" + imageName;
					String[] columnValues = { String.valueOf(i), imagePath };
					imagesCursor.addRow(columnValues);
				}
				vehicleAdapter.swapCursor(imagesCursor);
				vehicleCarousel.cursorUpdate();

				if(moveToVehicleOnLoad != 0){
					selectedVehicleIndex = moveToVehicleOnLoad;
					moveToVehicleOnLoad = 0;
				}

				if(selectedVehicleIndex < cursor.getCount()){
					vehicleCarousel.scrollToPage(selectedVehicleIndex);
					setCurrentVehicle(selectedVehicleIndex);
				}
			} catch (IllegalStateException e){
				e.printStackTrace();
				return;
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		// For whatever reason, the Loader's data is now unavailable.
		// Remove any references to the old data by replacing it with
		// a null Cursor.
		if (loader.getId() == MY_BRAND_LOADER_ID) {
			vehicleTypeAdapter.swapCursor(null);
			if(vehicleTypeCarousel != null){
				vehicleTypeCarousel.cursorUpdate();
			}
		} else {
			vehicleCursor = null;
			vehicleAdapter.swapCursor(null);
			if(vehicleCarousel != null){
				vehicleCarousel.cursorUpdate();
			}
		}
	}


	


}

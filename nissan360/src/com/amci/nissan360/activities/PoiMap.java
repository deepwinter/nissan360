package com.amci.nissan360.activities;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.amci.nissan360.R;
import com.amci.nissan360.Utilities;
import com.amci.nissan360.activities.base.Nissan360NavigationActivity;
import com.amci.nissan360.provider.PointsOfInterestProvider;
import com.google.android.gms.maps.SupportMapFragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class PoiMap extends Nissan360NavigationActivity  {

	Button allButton;
	Button vehicleExhibitsButton;
	Button tracksButton;
	Button guestServicesButton;
	ImageView poiMapLegend;
	private PoiMapFragment mapFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poi_map);


		allButton = (Button) findViewById(R.id.poi_map_all);
		tracksButton = (Button) findViewById(R.id.poi_map_tracks);
		vehicleExhibitsButton = (Button) findViewById(R.id.poi_map_vehicle_exhibits);
		guestServicesButton = (Button) findViewById(R.id.poi_map_guest_services);
		poiMapLegend = (ImageView) findViewById(R.id.poi_map_legend);
		Utilities.setImageBitmapNissan360(PoiMap.this, poiMapLegend, "maps/poi_legend_all.png");

		allButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				allButton.setSelected(true);
				tracksButton.setSelected(false);
				vehicleExhibitsButton.setSelected(false);
				guestServicesButton.setSelected(false);
				if(mapFragment != null) {
					mapFragment.showAllPoints();
					Utilities.setImageBitmapNissan360(PoiMap.this, poiMapLegend, "maps/poi_legend_all.png");
					poiMapLegend.setVisibility(View.VISIBLE);

				}
			}

		});

		tracksButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				allButton.setSelected(false);
				tracksButton.setSelected(true);
				vehicleExhibitsButton.setSelected(false);
				guestServicesButton.setSelected(false);
				if(mapFragment != null) {
					mapFragment.showTracksOnly();
					Utilities.setImageBitmapNissan360(PoiMap.this, poiMapLegend, "maps/poi_legend_tracks.png");
					poiMapLegend.setVisibility(View.VISIBLE);

				}
			}

		});

		vehicleExhibitsButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				allButton.setSelected(false);
				tracksButton.setSelected(false);
				vehicleExhibitsButton.setSelected(true);
				guestServicesButton.setSelected(false);
				if(mapFragment != null) {
					mapFragment.showVehicleExhibitsOnly();
					poiMapLegend.setVisibility(View.GONE);
				}
			}

		});

		guestServicesButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				allButton.setSelected(false);
				tracksButton.setSelected(false);
				vehicleExhibitsButton.setSelected(false);
				guestServicesButton.setSelected(true);
				if(mapFragment != null) {
					mapFragment.showGuestServicesOnly();
					Utilities.setImageBitmapNissan360(PoiMap.this, poiMapLegend, "maps/poi_legend_guest_services.png");
					poiMapLegend.setVisibility(View.VISIBLE);

				}
			}

		});

		allButton.setSelected(true);

	}





	@Override
	protected void onStart() {
		super.onStart();

		// Init to default button state
		allButton.setSelected(true);
		tracksButton.setSelected(false);
		vehicleExhibitsButton.setSelected(false);
		guestServicesButton.setSelected(false);

		mapFragment = new PoiMapFragment();

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction;
		transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.poi_map_container, mapFragment);
		transaction.commit();
	}




}

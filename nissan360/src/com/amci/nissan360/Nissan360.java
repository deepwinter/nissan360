package com.amci.nissan360;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.Timestamp;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import net.osmand.plus.OsmandApplication;
import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.amci.nissan360.api.ApiSettings;
import com.amci.nissan360.api.LoginUser;
import com.amci.nissan360.services.NissanApiService;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/*
 * 
 * Can't build in arca because we are over the method limit in this APK
 * 
 * import org.acra.*;
import org.acra.annotation.*;

@ReportsCrashes(formKey = "", 
				formUri = "http://winterroot.iriscouch.com/acra-app/_design/acra-storage/_update/report",
				 reportType = org.acra.sender.HttpSender.Type.JSON,
			        httpMethod = org.acra.sender.HttpSender.Method.PUT,
			        formUriBasicAuthLogin="nissan360",
			        formUriBasicAuthPassword="nissan360",
			        // Your usual ACRA configuration
			        mode = ReportingInteractionMode.TOAST,
			        resToastText = R.string.crash_toast_text)
 */
public class Nissan360 extends OsmandApplication {

	static public Nissan360 application;

	static public final String LOGGED_IN_USER_PREFERENCE = "LOGGED_IN_USER_PREFERENCE";
	static public final String NISSAN360_APK_ASSETS_VERSION_PREFERENCE = "NISSAN360_APK_ASSETS_VERSION_PREFERENCE";
	static public final String NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE = "NISSAN360_INITIAL_MEDIA_LOADED_PREFERNCE";
	static public final String CONTENT_HASH_PREFERENCE = "CONTENT_HASH_PREFERENCE";
	static public final String MEDIA_DOWNLOAD_HASH_PREFERENCE = "MEDIA_DOWNLOAD_HASH_PREFERENCE";
	static public int NISSAN360_APK_ASSETS_VERSION = 20; // DONT INCREMENT THIS ANYMORE!

	static private LoginUser loggedInUser;

	String phoneNumber;
	TelephonyManager tMgr;
	String deviceId;
	SharedPreferences prefs;
	Context context;
	String pushToken; // This is the push token

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final String PROPERTY_ON_SERVER_EXPIRATION_TIME = "onServerExpirationTimeMs";
	/**
	 * Default lifespan (7 days) of a reservation until it is considered expired.
	 */
	public static final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;

	/**
	 * Substitute you own sender ID here.
	 * The is the PROJECT id from the google API console
	 */
	String SENDER_ID = "664100751520";

	/**
	 * Tag used on gcm log messages.
	 */
	static final String TAG = "GCMDemo";

	public static final String MEDIA_DOWNLOAD_HASH_TIMESTAMP = "MEDIA_DOWNLOAD_HASH_TIMESTAMP";

	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();

	// Databases
	static JSONDatabase configDatabase = null;

	// Service
	private NissanApiService mBoundService;
	private boolean mIsBound = false;

	private MediaImageCache imageCache;

	public static ObjectMapper objectMapper;

	static {
		objectMapper = new ObjectMapper();
	}

	static public void logout(Activity context){

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);		
		settings.edit().putString(Nissan360.LOGGED_IN_USER_PREFERENCE, null).commit();
		loggedInUser = null;
		Intent intent = new Intent("com.amci.nissan360.action.LOGIN");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
		context.finish();

	}

	static public boolean apiIsReachable(){
		boolean exists = false;

		try {
			SocketAddress sockaddr = new InetSocketAddress(ApiSettings.API_HOST, ApiSettings.API_PORT);
			// Create an unbound socket
			Socket sock = new Socket();

			// This method will block no more than timeoutMs.
			// If the timeout occurs, SocketTimeoutException is thrown.
			int timeoutMs = 500;   // 2 seconds
			sock.connect(sockaddr, timeoutMs);
			exists = true;
		} catch(Exception e){
			e.printStackTrace();
		}
		return exists;
	}


	public NissanApiService getNissanApiService() throws Exception{
		if(mBoundService == null){
			throw new Exception();
		}
		return mBoundService;
	}

	@Override
	public File getAppPath(String path) {
		if(path == null) {
			path = "";
		}
		return new File(getSettings().getExternalStorageDirectory(), "n360/" + path);
	}

	PendingIntent pendingIntent;

	private AssetManager assetManager;

	@Override
	public void onCreate() {
		super.onCreate();
		// ACRA.init(this);

		Nissan360.application = this;
		imageCache = new MediaImageCache();
		assetManager = this.getAssets();
		startService(new Intent(this, NissanApiService.class));
		doBindService();

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());		

		ObjectMapper objectMapper = new ObjectMapper();
		String loggedInUserJSON = settings.getString(Nissan360.LOGGED_IN_USER_PREFERENCE, null);
		if(loggedInUserJSON != null){
			LoginUser loginUser = null;
			try {
				loginUser = objectMapper.readValue(loggedInUserJSON, LoginUser.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(loginUser != null){
				Nissan360.loggedInUser = loginUser;
			}
		}

		// TODO: if we have an error here, restart with login

		context = getApplicationContext();
		pushToken = ""; // getRegistrationId(context);

		if (pushToken.length() == 0) {
			registerBackground();
		} else {
			Log.i("PUSH", "Push Token "+pushToken);
		}
		gcm = GoogleCloudMessaging.getInstance(this);

		preloadCacheInBackground();

	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration id, app versionCode, and expiration time in the 
	 * application's shared preferences.
	 */
	Timer gcmRegistrationTimer;
	private void registerBackground() {
		new AsyncTask<Void, Void, String>() {   // Could also be Robospice

			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					pushToken = gcm.register(SENDER_ID);
					Log.i("PUSH", "Push Token "+pushToken);

					//msg = "Device registered, registration id=" + pushToken;
					msg = null;

					// You should send the registration ID to your server over HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your app.

					// For this demo: we don't need to send it because the device
					// will send upstream messages to a server that echo back the message
					// using the 'from' address in the message.

					// Save the pushToken - no need to register again.
					setRegistrationId(context, pushToken);
				} catch (IOException ex) {
					ex.printStackTrace();
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}


			@Override
			protected void onPostExecute(String msg) {
				if(msg != null){
					Toast.makeText(getApplicationContext(), "GCM PUSH registration failure", Toast.LENGTH_SHORT).show();
					// Try again with backoff
					gcmRegistrationTimer = new Timer();
					gcmRegistrationTimer.schedule(new TimerTask(){

						@Override
						public void run() {
							registerBackground();
						}

					}, 1000 * 60 * 1);


				} else {
					Toast.makeText(getApplicationContext(), "GCM PUSH registration success", Toast.LENGTH_SHORT).show();

				}
			}



		}.execute(null, null, null);
	}

	public void preloadCacheInBackground() {
		new AsyncTask<Void, Void, Void>() {   // Could also be Robospice

			@Override
			protected Void doInBackground(Void... params) {

				// These images are packaged with the app, so they will always be present
				imageCache.getBitmap(Utilities.getNissan360MediaDir(getApplicationContext())+"vehicles/icon_car.png");
				imageCache.getBitmap(Utilities.getNissan360MediaDir(getApplicationContext())+"vehicles/icon_concept.png");
				imageCache.getBitmap(Utilities.getNissan360MediaDir(getApplicationContext())+"vehicles/icon_suv.png");
				imageCache.getBitmap(Utilities.getNissan360MediaDir(getApplicationContext())+"vehicles/icon_taxi.png");
				imageCache.getBitmap(Utilities.getNissan360MediaDir(getApplicationContext())+"vehicles/icon_truck.png");
				imageCache.getBitmap(Utilities.getNissan360MediaDir(getApplicationContext())+"vehicles/icon_van_bus.png");

				return null;
			}


			@Override
			protected void onPostExecute(Void v) {

			}



		}.execute(null, null, null);
	}




	public static LoginUser getLoggedInUser() {
		return getLoggedInUser(false);
	}

	public static LoginUser getLoggedInUser(boolean allowNull) {
		if(Nissan360.loggedInUser == null){
			if(!allowNull){
				Intent i = new Intent("com.amci.nissan360.action.LOGIN");
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK );
				Nissan360.application.getApplicationContext().startActivity(i);
				return new LoginUser(); // To avoid null pointer exception
			} else {
				return null;
			}
		} else {
			return Nissan360.loggedInUser;
		}
	}

	public static void setLoggedInUser(LoginUser user){
		Nissan360.loggedInUser = user;
	}

	/**
	 * Stores the registration id, app versionCode, and expiration time in the
	 * application's {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration id
	 */
	private void setRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.v(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		long expirationTime = System.currentTimeMillis() + REGISTRATION_EXPIRY_TIME_MS;

		Log.v(TAG, "Setting registration expiry time to " +
				new Timestamp(expirationTime));
		editor.putLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, expirationTime);
		editor.commit();
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		return getSharedPreferences("GCM_PREFS", Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Checks if the registration has expired.
	 *
	 * <p>To avoid the scenario where the device sends the registration to the
	 * server but the server loses it, the app developer may choose to re-register
	 * after REGISTRATION_EXPIRY_TIME_MS.
	 *
	 * @return true if the registration has expired.
	 */
	private boolean isRegistrationExpired() {
		final SharedPreferences prefs = getGCMPreferences(context);
		// checks if the information is not stale
		long expirationTime =
				prefs.getLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, -1);
		return System.currentTimeMillis() > expirationTime;
	}


	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service.  Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			mBoundService = ((NissanApiService.LocalBinder)service).getService();

			// Tell the user about this for our demo.
			Toast.makeText(getBaseContext(), "ApiQueueService Connected",
					Toast.LENGTH_SHORT).show();
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.
			mBoundService = null;
			Toast.makeText(getBaseContext(), "ApiQueueService Disconnected",
					Toast.LENGTH_SHORT).show();
		}
	};

	public void doBindService() {
		// Establish a connection with the service.  We use an explicit
		// class name because we want a specific service implementation that
		// we know will be running in our own process (and thus won't be
		// supporting component replacement by other applications).
		bindService(new Intent(getBaseContext(), 
				NissanApiService.class), mConnection, Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	void doUnbindService() {
		if (mIsBound) {
			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}


	public String getPhoneNumber(){
		if(phoneNumber == null){
			if(tMgr==null){
				tMgr = getTelephonyManager(); 
			}
			phoneNumber = tMgr.getLine1Number();
		}

		return phoneNumber;

	}

	public String getDeviceId(){
		if(deviceId == null){
			if(tMgr==null){
				tMgr = getTelephonyManager(); 
			}
			deviceId = tMgr.getDeviceId();
		}
		return deviceId;
	}


	private TelephonyManager getTelephonyManager(){
		tMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		return tMgr;
	}

	public String getPushToken(){
		return pushToken;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		doUnbindService();
	}


	// Copy Assets
	private void copyAssetsFileOrDir(String path) throws InterruptedException {
		Thread.sleep(500);
		String assets[] = null;
		try {
			assets = assetManager.list(path);
			if (assets.length == 0) {
				copyFile(path);
			} else {
				String fullPath = getApplicationContext().getFilesDir().getPath() + "/" + path;
				File dir = new File(fullPath);
				if (!dir.exists())
					dir.mkdir();
				for (int i = 0; i < assets.length; ++i) {
					copyAssetsFileOrDir(path + "/" + assets[i]);
				}
			}
		} catch (IOException ex) {
			Log.e("tag", "I/O Exception", ex);
		}
	}

	private void copyFile(String filename) {
		AssetManager assetManager = this.getAssets();

		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open(filename);
			String newFileName = getApplicationContext().getFilesDir().getPath() + "/" + filename;
			out = new FileOutputStream(newFileName);

			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch (Exception e) {
			Log.e("tag", e.getMessage());
		}

	}

	public static JSONDatabase getConfigDatabase() throws InitJSONDatabaseExcepiton {
		if(configDatabase == null){
			configDatabase = JSONDatabase.GetDatabase(application.getApplicationContext(), "ConfigurationJSONDatabaseConfiguration");
		}
		return configDatabase;
	}

	public MediaImageCache getImageCache(){
		return imageCache;
	}
}

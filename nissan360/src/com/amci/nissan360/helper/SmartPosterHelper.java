package com.amci.nissan360.helper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.amci.nissan360.activities.nfc.SmartPoster;
import com.amci.nissan360.activities.nfc.SmartPosterVideo;
import com.amci.nissan360.database.AssetsContract;
import com.amci.nissan360.provider.AssetsProvider;

public class SmartPosterHelper {

	public static void launchSmartPoster(Context c, String smartPosterIdentifier){
		String [] smartPosterProjection = {
				AssetsContract.SMART_POSTER_FILE_FIELD,
		};

		// Get the smart poster record itself
		Cursor smartPoster = c.getContentResolver().query( Uri.withAppendedPath(AssetsProvider.SMART_POSTERS_CONTENT_URI, smartPosterIdentifier), smartPosterProjection, null, null, null);
		if(smartPoster.getCount() == 0){
			Log.w("Smart Poster", "Smart Poster Not Found");
			Toast.makeText(c, "Poster Not Found", Toast.LENGTH_LONG).show();
			return;
		}
		smartPoster.moveToFirst();
		String file = smartPoster.getString(smartPoster.getColumnIndex(AssetsContract.SMART_POSTER_FILE_FIELD));
		if(file == null){
			Toast.makeText(c, "Poster File Not Found", Toast.LENGTH_LONG).show();
			return;
		}
		if(file.endsWith("mp4") ){
			// it's a video
			Intent i = new Intent(c, SmartPosterVideo.class);
			i.putExtra(SmartPosterVideo.VIDEO_FILENAME_EXTRA, file);
			c.startActivity(i);

		} else if(file.endsWith("png") || file.endsWith("jpg")){
			// it's a png
			Intent i = new Intent(c, SmartPoster.class);
			i.putExtra(SmartPoster.POSTER_FILENAME_EXTRA, file);
			c.startActivity(i);
		}
	}

}

package com.amci.nissan360.helper;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import net.smart_json_database.InitJSONDatabaseExcepiton;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import com.amci.nissan360.MapIntentContract;
import com.amci.nissan360.Nissan360;
import com.amci.nissan360.VehicleIntentContract;
import com.amci.nissan360.activities.drive.DriveLocationMap;
import com.amci.nissan360.activities.drive.VehicleDriveHighPerformance;
import com.amci.nissan360.activities.drive.VehicleDriveStreet;
import com.amci.nissan360.activities.drive.VehicleDriveWalkUp;
import com.amci.nissan360.activities.vehicleselector.VehicleStoryActivity;
import com.amci.nissan360.api.configuration.Coordinate;
import com.amci.nissan360.api.configuration.Coordinates;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.VehiclesProvider;

public class VehiclesHelper {

	public static void startExperienceActivityForVehicle(Context c, Cursor v){

		String track = v.getString(v.getColumnIndex(VehiclesContract.TRACK_FIELD));
		String vin = v.getString(v.getColumnIndex(VehiclesContract.VIN_FIELD));
		String bucket = v.getString(v.getColumnIndex(VehiclesContract.BUCKET_FIELD));

		if(track.equals(VehiclesContract.TRACK_TYPE_HIGH_PERFORMANCE)){
			Intent i = new Intent(VehicleDriveHighPerformance.INTENT_ACTION);
			i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
			c.startActivity(i);
		} else if(VehiclesContract.TRACK_TYPES_STREET.contains(track)){
			Intent i = new Intent(VehicleDriveStreet.INTENT_ACTION);
			i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
			c.startActivity(i);
		} else if(VehiclesContract.TRACK_TYPES_WALKUP.contains(track) ) {
			// Some other track
			Intent i = new Intent(VehicleDriveWalkUp.INTENT_ACTION);
			i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
			c.startActivity(i);
		} else if(bucket.equals(VehiclesContract.BUCKET_DISPLAY_TYPE)){
			Intent i = new Intent(VehicleStoryActivity.INTENT_ACTION);
			i.putExtra(VehicleIntentContract.INTENT_EXTRA_VIN, vin);
			c.startActivity(i);
		}

	}

/*
	public static void startWalkingNavigationForDriveLocation(Context context, String VIN) {

		Cursor c = context.getContentResolver().query( Uri.withAppendedPath(VehiclesProvider.CONTENT_URI_BY_VIN, VIN), 
				VehiclesProvider.getDefaultProjection(), null, null, null
				);
		if(c.getCount() < 1){
			Toast.makeText(context, "Vehicle not found", Toast.LENGTH_SHORT).show();
			return;
		}

		c.moveToPosition(0);
		String track = c.getString(c.getColumnIndex(VehiclesContract.TRACK_FIELD));
		String bucket = c.getString(c.getColumnIndex(VehiclesContract.BUCKET_FIELD));
		if(track == null){
			Toast.makeText(context, "Coordinates not found", Toast.LENGTH_SHORT).show();
			return;
		}

		// if perf drive
		// then shuttle
		// if street drive
		// street drive location
		// otherwise, get it from the vehicle
		
		// This logic needs to be updated
		// it's wrong!
		
		
		
		double latitude = 0;
		double longitude = 0;
		if(track.equals(VehiclesContract.TRACK_TYPE_HIGH_PERFORMANCE)){
			Coordinate coordinate = getCoordinate(Coordinates.COORINDATE_PERFORMANCE_TRACK_SHUTTLE_PICKUP);
			if(coordinate != null){
				latitude = coordinate.latitude;
				longitude = coordinate.longitude;
			}
		} else if(VehiclesContract.TRACK_TYPES_STREET.contains(track)){
			Coordinate coordinate = getCoordinate(Coordinates.COORINDATE_STREET_DRIVE_PICKUP);
			if(coordinate != null){
				latitude = coordinate.latitude;
				longitude = coordinate.longitude;
			}
		} else if(VehiclesContract.TRACK_TYPES_WALKUP.contains(track)
				||( bucket != null &&  bucket.equals(VehiclesContract.BUCKET_DISPLAY_TYPE ))){
			latitude = c.getDouble(c.getColumnIndex(VehiclesContract.LATITUDE_FIELD));
			longitude = c.getDouble(c.getColumnIndex(VehiclesContract.LONGITUDE_FIELD));
		}

		if(latitude == 0 || longitude == 0){
			Toast.makeText(context, "Coordinates not found", Toast.LENGTH_SHORT).show();
			return;
		}
		Intent i = new Intent(context, DriveLocationMap.class);
		i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE, latitude);
		i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE, longitude);
		context.startActivity(i);		
	}

*/

	public static Coordinate getCoordinate(String key){
		try {
			JSONDatabase configDatabase = Nissan360.getConfigDatabase();
			SearchFields search = SearchFields.Where("Type", Coordinates.COORDINATES_TYPE);
			Collection<JSONEntity> entities =  configDatabase.fetchByFields(search);
			Iterator<JSONEntity> i = entities.iterator();
			if(i.hasNext()){
				JSONEntity e = i.next();
				Coordinates c = (Coordinates) e.asClass(Coordinates.class);
				Coordinate coordinate = c.points.get(key);
				if(coordinate != null){
					return coordinate;
				}

			}

		} catch (InitJSONDatabaseExcepiton e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	
}

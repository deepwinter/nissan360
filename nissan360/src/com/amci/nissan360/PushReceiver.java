package com.amci.nissan360;

import com.amci.nissan360.activities.drive.DriveReminder;
import com.amci.nissan360.activities.messages.Message;
import com.amci.nissan360.state.ApplicationState;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

public class PushReceiver extends BroadcastReceiver {
	static final String TAG = "GCMDemo";
	public static final int NOTIFICATION_ID = 1;
	private static final String PUSH_BUNDLE_MESSAGE = "message";
	private static final String PUSH_BUNDLE_TYPE = "type";
	private static final String PUSH_BUNDLE_LATITUDE = "latitude";
	private static final String PUSH_BUNDLE_LONGITUDE = "longitude";
	private static final String PUSH_BUNDLE_MINUTES_UNTIL_RESERVATION = "minutes_until_reservation";
	private static final String PUSH_BUNDLE_RESERVATION_ID = "reservation_id";


	private static final String PUSH_TYPE_MESSAGE = "message";
	private static final String PUSH_TYPE_ALERT = "alert";
	private static final String PUSH_TYPE_RESERVATION_AVAILABLE = "reservation_available";
	private static final String PUSH_TYPE_RESERVATION_MODIFIED = "reservation_modified";
	private static final String PUSH_TYPE_RESERVATION_CANCELLED = "reservation_cancelled";
	private static final int UNSPECIFIED_INTEGER_VALUE = 98989898;


	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	Context ctx;

	@Override
	public void onReceive(Context context, Intent intent) {
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		ctx = context;
		String messageType = gcm.getMessageType(intent);
		

		
		if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
			sendNotification("Send error: " + intent.getExtras().toString());
		} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
			sendNotification("Deleted messages on server: " +
					intent.getExtras().toString());
		} else {
			handleMessage(intent.getExtras());
		}
		setResultCode(Activity.RESULT_OK);
	}

	private void handleMessage(Bundle bundle){

		// Switch on the message
		String type = bundle.getString(PushReceiver.PUSH_BUNDLE_TYPE);
		if(type == null){
			return;
		}

		Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);

		// 1. Vibrate for 1000 milliseconds
		long milliseconds = 1000;
		if(v != null){
			v.vibrate(milliseconds);
		}

		try {
			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(ctx, notification);
			r.play();
		} catch (Exception e) {}


		if(type.equals(PUSH_TYPE_MESSAGE)) {

			String msg = bundle.getString(PushReceiver.PUSH_BUNDLE_MESSAGE);
			Intent i = new Intent(ctx, Message.class);
			i.putExtra(Message.MESSAGE_EXTRA, msg);
			sendIntentNotification(msg, i);         

		} else if (type.equals(PUSH_TYPE_ALERT)){
			String msg = bundle.getString(PushReceiver.PUSH_BUNDLE_MESSAGE);
			Intent i = new Intent(ctx, Message.class);
			i.putExtra(Message.MESSAGE_EXTRA, msg);
			i.putExtra(Message.TYPE_EXTRA, Message.ALERT_TYPE);
			sendIntentNotification(msg, i);         

		} else if(type.equals(PUSH_TYPE_RESERVATION_AVAILABLE)){

			double latitude = 0;
			double longitude = 0;
			int minutesUntilReservation = UNSPECIFIED_INTEGER_VALUE;
			String reservationId = "";
			String message = "";

			if(bundle.containsKey(PUSH_BUNDLE_LATITUDE)){
				String latitudeString = bundle.getString(PUSH_BUNDLE_LATITUDE);
				if(latitudeString != null){
					latitude = Double.parseDouble(latitudeString);
				}
			}

			if(bundle.containsKey(PUSH_BUNDLE_LONGITUDE)){
				String longitudeString = bundle.getString(PUSH_BUNDLE_LONGITUDE);
				if(longitudeString != null){
					longitude = Double.parseDouble(longitudeString);
				}
			}

			if(bundle.containsKey(PUSH_BUNDLE_MINUTES_UNTIL_RESERVATION)){
				String minutesUntilReservationString = bundle.getString(PUSH_BUNDLE_MINUTES_UNTIL_RESERVATION);
				if(minutesUntilReservationString != null){
					minutesUntilReservation = Integer.parseInt(minutesUntilReservationString);
				}
			}

			if(bundle.containsKey(PUSH_BUNDLE_RESERVATION_ID)){
				reservationId = bundle.getString(PUSH_BUNDLE_RESERVATION_ID);
			}

			if(bundle.containsKey(PUSH_BUNDLE_MESSAGE)){
				message = bundle.getString(PUSH_BUNDLE_MESSAGE);
			}

			// Launch the reservation available activity.
			Intent i = new Intent(ctx, DriveReminder.class);
			//i.putExtra(Message.MESSAGE_EXTRA, msg);
			i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LATITUDE, latitude);
			i.putExtra(MapIntentContract.INTENT_EXTRA_LOCATION_LONGITUDE, longitude);
			if(minutesUntilReservation != UNSPECIFIED_INTEGER_VALUE){
				i.putExtra(DriveReminder.INTENT_EXTRA_MINUTES_UNTIL_RESERVATION, minutesUntilReservation);
			}
			i.putExtra(DriveReminder.INTENT_EXTRA_RESERVATION_ID, reservationId);
			i.putExtra(DriveReminder.INTENT_EXTRA_MESSAGE, message);

			ApplicationState.moveToReservationPendingMode(ctx, Integer.valueOf(reservationId) );
			sendIntentNotification("Reservation is available", i);  

		} else if(type.equals(PUSH_TYPE_RESERVATION_MODIFIED)){

			try {
				Nissan360.application.getNissanApiService().refreshReservations(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String msg = bundle.getString(PushReceiver.PUSH_BUNDLE_MESSAGE);
			Intent i = new Intent(ctx, Message.class);
			i.putExtra(Message.MESSAGE_EXTRA, msg);
			i.putExtra(Message.TYPE_EXTRA, Message.ALERT_TYPE);
			sendIntentNotification(msg, i); 

		} else if(type.equals(PUSH_TYPE_RESERVATION_CANCELLED)){

			try {
				Nissan360.application.getNissanApiService().refreshReservations(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String msg = bundle.getString(PushReceiver.PUSH_BUNDLE_MESSAGE);
			Intent i = new Intent(ctx, Message.class);
			i.putExtra(Message.MESSAGE_EXTRA, msg);
			i.putExtra(Message.TYPE_EXTRA, Message.ALERT_TYPE);
			sendIntentNotification(msg, i); 

		}
	}

	// Put the GCM message into a notification and post it.
	private void sendIntentNotification(String msg, Intent i) {
		mNotificationManager = (NotificationManager)
				ctx.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
				i, 0);

		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(ctx)
		.setSmallIcon(android.R.drawable.btn_default)
		.setContentTitle("GCM Notification")
		.setStyle(new NotificationCompat.BigTextStyle()
		.bigText(msg))
		.setContentText(msg);

		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

		if(ApplicationState.isInDriveMode(ctx)){
			return;
		}
		
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(i);
	}

	// Put the GCM message into a notification and post it.
	private void sendNotification(String msg) {
		mNotificationManager = (NotificationManager)
				ctx.getSystemService(Context.NOTIFICATION_SERVICE);



		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(ctx)
		.setSmallIcon(android.R.drawable.btn_default)
		.setContentTitle("GCM Notification")
		.setStyle(new NotificationCompat.BigTextStyle()
		.bigText(msg))
		.setContentText(msg);

		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


	}
}

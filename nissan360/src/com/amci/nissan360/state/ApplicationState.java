package com.amci.nissan360.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.activities.drive.VehicleExperienceController;
import com.amci.nissan360.activities.vehicleselector.VehicleStoryActivity;

import net.osmand.data.LatLon;
import net.osmand.plus.activities.MapActivity;
import net.osmand.plus.routing.RoutingHelper;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ApplicationState {


	public static final String APPLICATION_STATE_DRIVE_STATUS_TYPE = "APPLICATION_STATE_DRIVE_TYPE";
	public static final String APPLICATION_STATE_DRIVE_UNDERWAY = "APPLICATION_STATE_DRIVE_UNDERWAY";
	public static final String APPLICATION_STATE_NOT_DRIVING = "APPLICATION_STATE_NOT_DRIVING";
	public static final String APPLICATION_STATE_RESERVATION_PENDING = "APPLICATION_STATE_RESERVATION_REMINDER";
	public static final String APPLICATION_STATE_CHECKED_IN = "APPLICATION_STATE_CHECKED_IN";

	public static final String APPLICATION_STATE_DRIVE_LAUNCHES_COUNTER_TYPE = "APPLICATION_STATE_DRIVE_STATUS_TYPE";
	private static final String APPLICATION_STATE_ROUTE_NAME_PARAM = "APPLICATION_STATE_ROUTE_NAME_PARAM";

	private static final ArrayList<Integer> pendingReservations;
	
	static {
		pendingReservations = new ArrayList<Integer>();
	}

	public static boolean isInDriveMode(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		if(settings.getString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_NOT_DRIVING).equals(APPLICATION_STATE_DRIVE_UNDERWAY) ){
			return true;
		}
		return false;
	}

	public static boolean isReservationPending(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		if(settings.getString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_NOT_DRIVING).equals(APPLICATION_STATE_RESERVATION_PENDING) ){
			return true;
		}
		return false;
	}

	public static boolean isCheckedIn(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		if(settings.getString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_NOT_DRIVING).equals(APPLICATION_STATE_CHECKED_IN) ){
			return true;
		}
		return false;
	}

	public static void moveToReservationPendingMode(Context c, int pendingReservationId){
		pendingReservations.add(pendingReservationId);
		setDriveStateReservationPending(c);
	}
	
	public static boolean reservationIsPending(int reservationId){
		if(pendingReservations.contains(reservationId)){
			return true;
		} else {
			return false;
		}
	}

	public static void moveToCheckedInMode(Context c){
		setDriveStateCheckedIn(c);
	}

	public static void moveToDriveMode(Context c, String routeName){
		setDriveStateDriveUnderway(c);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(APPLICATION_STATE_ROUTE_NAME_PARAM, routeName);
		startRootActivityForCurrentState(c, params);
	}

	public static void leaveDriveMode(Context c){
		// Clear OSMAnd Routing
		RoutingHelper routingHelper = Nissan360.application.getRoutingHelper();
		if(routingHelper != null){
			routingHelper.setFinalAndCurrentLocation(null, new ArrayList<LatLon>(), null,
					routingHelper.getCurrentGPXRoute());
		}
		//
		setDriveStateNotDriving(c);
		startRootActivityForCurrentState(c);
	}

	public static void resetApplicationState(Context c){
		setDriveStateNotDriving(c);
	}

	private static void setDriveStateNotDriving(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		settings.edit().putString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_NOT_DRIVING).commit();
		settings.edit().putInt(APPLICATION_STATE_DRIVE_LAUNCHES_COUNTER_TYPE, 0).commit();

	}

	private static void setDriveStateDriveUnderway(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		settings.edit().putString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_DRIVE_UNDERWAY).commit();
	}

	private static void setDriveStateReservationPending(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		settings.edit().putString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_RESERVATION_PENDING).commit();
	}

	private static void setDriveStateCheckedIn(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		settings.edit().putString(APPLICATION_STATE_DRIVE_STATUS_TYPE, APPLICATION_STATE_CHECKED_IN).commit();		
	}

	public static void startRootActivityForCurrentState(Context c) {
		startRootActivityForCurrentState(c, null);
	}

	public static void startRootActivityForCurrentState(Context c, Map<String, Object> params) {

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		String driveState = settings.getString(APPLICATION_STATE_DRIVE_STATUS_TYPE,APPLICATION_STATE_NOT_DRIVING);

		if(driveState.equals(APPLICATION_STATE_NOT_DRIVING) 
				|| driveState.equals(APPLICATION_STATE_RESERVATION_PENDING)) {
			startDashboard(c);

		} else if(driveState.equals(APPLICATION_STATE_CHECKED_IN)){
			Intent i = new Intent(c, VehicleExperienceController.class);
			c.startActivity(i);
		} else {
			
			// We are driving
			int launchesCounter = settings.getInt(APPLICATION_STATE_DRIVE_LAUNCHES_COUNTER_TYPE, 0);

			if(launchesCounter == 0){

				if(params == null || !params.containsKey(APPLICATION_STATE_ROUTE_NAME_PARAM)){
					Toast.makeText(c, "No Paramters Found", Toast.LENGTH_SHORT).show();
					startDashboard(c);
					return;
				}
				String routeName = (String) params.get(APPLICATION_STATE_ROUTE_NAME_PARAM);
				if(routeName == null){
					Toast.makeText(c, "Route Name not Set", Toast.LENGTH_SHORT).show();
					startDashboard(c);
					return;
				}

				// First launch, use the selected route
				Intent i = new Intent("com.amci.nissan360.action.TURN_BY_TURN");
				i.putExtra(MapActivity.GPX_EXTRA, routeName);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				c.startActivity(i);

			} else {
				// Relaunch, application crashed or something
				// Don't send an extra, navigation should resume
				// or ask to resume
				// This needs to be tested.
				Intent i = new Intent("com.amci.nissan360.action.TURN_BY_TURN");
				c.startActivity(i);
			}
			settings.edit().putInt(APPLICATION_STATE_DRIVE_LAUNCHES_COUNTER_TYPE, launchesCounter+1).commit();

		}
	}

	private static void startDashboard(Context c){
		Intent i = new Intent("com.amci.nissan360.action.DASHBOARD");
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		c.startActivity(i);
	}


}

package com.amci.nissan360.state;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ReservationState {
	private static final String RESERVATION_STATE_PREFERENCE_VIN = "RESERVATION_STATE_PREFERENCE_VIN";
	private static final String RESERVATION_STATE_PREFERENCE_RESERVATION_ID = "RESERVATION_STATE_PREFERENCE_RESERVATION_ID";
	
	public static void setActiveReservation(Context c, String vin, int reservationId){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		settings.edit().putString(RESERVATION_STATE_PREFERENCE_VIN, vin).commit();
		settings.edit().putInt(RESERVATION_STATE_PREFERENCE_RESERVATION_ID, reservationId).commit();
	}
	
	public static void clearActiveReservation(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		settings.edit().putString(RESERVATION_STATE_PREFERENCE_VIN, null).commit();
		settings.edit().putString(RESERVATION_STATE_PREFERENCE_RESERVATION_ID, null).commit();
	}
	
	public static String getVin(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		return settings.getString(RESERVATION_STATE_PREFERENCE_VIN, "");
	}
	
	public static int getReservationId(Context c){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
		return settings.getInt(RESERVATION_STATE_PREFERENCE_RESERVATION_ID, 0);
	}
}

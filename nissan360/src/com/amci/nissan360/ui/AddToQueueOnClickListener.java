package com.amci.nissan360.ui;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CancellationSignal.OnCancelListener;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.agenda.Agenda;
import com.amci.nissan360.activities.base.RobospiceActivity;
import com.amci.nissan360.api.ApiError;
import com.amci.nissan360.api.Nissan360ApiErrorException;
import com.amci.nissan360.api.reservation.ReservationDescriptor;
import com.amci.nissan360.api.reservation.ReservationOptions;
import com.amci.nissan360.api.reservation.StreetDriveQueueRequest;
import com.amci.nissan360.api.reservation.StreetDriveQueueResponse;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.ReservationsQueueProvider;
import com.amci.nissan360.state.ApplicationState;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.walnutlabs.android.ProgressHUD;

public class AddToQueueOnClickListener implements OnClickListener, android.content.DialogInterface.OnCancelListener {

	private static final int NOTHING_SELECTED = 9999;

	RobospiceActivity activity;
	String vin;
	public ProgressHUD mProgressHUD;
	int selectedIndex = NOTHING_SELECTED;


	public AddToQueueOnClickListener(RobospiceActivity activity, String vin) {
		super();
		this.activity = activity;
		this.vin = vin;
	}



	@Override
	public void onClick(View v) {

		final Dialog dialog = new Dialog(activity);
		dialog.setContentView(R.layout.view_add_to_queue_dialog);
		dialog.setTitle("Add to Queue");

		ListView coursesListView = (ListView) dialog.findViewById(R.id.queue_choose_course_list);
		final CourseSelectionArrayAdapter<String> coursesAdapter = new CourseSelectionArrayAdapter<String>(activity, android.R.layout.simple_list_item_1);
		coursesAdapter.add(VehiclesContract.getStreetTrackDisplayName(VehiclesContract.TRACK_TYPES_STREET.get(0)));
		coursesAdapter.add(VehiclesContract.getStreetTrackDisplayName(VehiclesContract.TRACK_TYPES_STREET.get(1)));
		coursesListView.setAdapter(coursesAdapter);
		coursesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
				view.setSelected(true);
				selectedIndex = position;
			}

		});

		// Add the buttons
		Button yesButton = (Button) dialog.findViewById(R.id.queue_yes_button);
		yesButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if(selectedIndex == NOTHING_SELECTED){
					Toast.makeText(activity, "You must select a course", Toast.LENGTH_LONG).show();
					return;
				}

				dialog.dismiss();

				//Add to queue
				final ReservationOptions reservationOptions;
				try {
					reservationOptions = new ReservationOptions(
							Nissan360.getLoggedInUser().LoginId, vin, VehiclesContract.TRACK_TYPES_STREET.get(selectedIndex) );
				} catch (Exception e) {
					// User isn't logged in somehow.. pop to login page?
					e.printStackTrace();
					return;
				} 

				//Let user know it's done
				StreetDriveQueueRequest request = new StreetDriveQueueRequest(reservationOptions);
				class StreetDriveQueueRequestListener implements RequestListener< StreetDriveQueueResponse > {

					@Override
					public void onRequestFailure(SpiceException exception) {

						mProgressHUD.dismiss();

						if(exception instanceof NoNetworkException){
							// Handle as a network problem

							QueueFailedMessageDialog reservationMessageDialog = new QueueFailedMessageDialog("Network not Available", 
									"The network was not available.  Your request has been logged and will be uploaded as soon as possible");
							reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");

							// And add to the reservations queue
							activity.getContentResolver().insert(ReservationsQueueProvider.CONTENT_URI, reservationOptions.getContentValues());

						} else if (exception.getCause() instanceof HttpClientErrorException) {
							HttpClientErrorException clientException = (HttpClientErrorException)exception.getCause();
							if (clientException.getStatusCode().equals(HttpStatus.BAD_REQUEST) ) 
							{
								String body = clientException.getResponseBodyAsString();
								try {
									ApiError error = Nissan360.objectMapper.readValue(body, ApiError.class);

									String dialogMessage = error.getErrorText();

									QueueFailedMessageDialog reservationMessageDialog;
									reservationMessageDialog = new QueueFailedMessageDialog("Request Failed", dialogMessage);
									reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");

								} catch (JsonParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (JsonMappingException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

							}

						} else {

							String dialogMessage = "There was a problem with the request.  Please see an ambassador. ";

							String message = exception.getMessage();
							if(exception.getCause() instanceof HttpClientErrorException)
							{
								HttpClientErrorException e = (HttpClientErrorException)exception.getCause();
								String response = e.getResponseBodyAsString();
								dialogMessage += response;
							} else {
								dialogMessage += message;
							}

							QueueFailedMessageDialog reservationMessageDialog;	
							reservationMessageDialog = new QueueFailedMessageDialog("Request Failed", dialogMessage);
							reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");
						}

					}

					@Override
					public void onRequestSuccess(
							StreetDriveQueueResponse response) {


						String waitTimeMessageV2;
						if(ApplicationState.isReservationPending(activity)){
							waitTimeMessageV2 = "Within 15 Minutes";
						} else {
							int number = 45 * response.QueuePos;
							if(number  > 60){
								int hours = (int) (number / 60);
								int minutes = number % 60;
								waitTimeMessageV2 = "Within " + String.valueOf(hours) + " Hours, " + String.valueOf(minutes) + " Minutes";
							} else {
								waitTimeMessageV2 = "Within " + String.valueOf(number) + " Minutes";
							}
						}
						
						
						mProgressHUD.dismiss();
						QueueAddedMessageDialog reservationMessageDialog = new QueueAddedMessageDialog(response.Vehicle, waitTimeMessageV2);
						reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");	

						try {
							Nissan360.application.getNissanApiService().refreshReservations(null);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}

				mProgressHUD = ProgressHUD.show(activity,"Queuing...", true,true,AddToQueueOnClickListener.this);
				activity.spiceManager.execute( request, null, DurationInMillis.NEVER, new StreetDriveQueueRequestListener() );

			}

		});

		Button noButton = (Button) dialog.findViewById(R.id.queue_no_button);
		noButton.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();				


	}

	@Override
	public void onCancel(DialogInterface dialog) {
		mProgressHUD.dismiss();

	}


}



class QueueAddedMessageDialog  extends DialogFragment {

	String message;
	String model;



	public QueueAddedMessageDialog(String model, String message) {
		super();
		this.model = model;
		this.message = message;
	}



	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.added_to_queue).setMessage("The Nissan " + model + " has been added to your Queue. Your vehicle is estimated to be available within " + message);

		// Add the buttons
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				QueueAddedMessageDialog.this.dismiss();
				Intent i = new Intent(getActivity(), Agenda.class);
				i.putExtra(Agenda.INTENT_EXTRA_CURRENT_TAB, Agenda.INTENT_EXTRA_QUEUE_TAB);
				startActivity(i);
				getActivity().finish();

			}
		});



		AlertDialog dialog = builder.create();
		return dialog;
	}
}

class QueueFailedMessageDialog  extends DialogFragment {

	String title;
	String message;

	public QueueFailedMessageDialog(String title, String message) {
		this.title = title;
		this.message = message;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(message)
		.setTitle(title);

		// Add the buttons
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				QueueFailedMessageDialog.this.dismiss();

			}
		});



		AlertDialog dialog = builder.create();
		return dialog;
	}
}

package com.amci.nissan360.ui;

import java.io.IOException;
import java.util.ArrayList;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CancellationSignal.OnCancelListener;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.amci.nissan360.Nissan360;
import com.amci.nissan360.R;
import com.amci.nissan360.activities.base.RobospiceActivity;
import com.amci.nissan360.api.ApiError;
import com.amci.nissan360.api.reservation.PerformanceDriveReservationOptions;
import com.amci.nissan360.api.reservation.PerformanceDriveReservationRequest;
import com.amci.nissan360.api.reservation.PerformanceDriveReservationResponse;
import com.amci.nissan360.api.reservation.PerformanceTimesRequest;
import com.amci.nissan360.api.reservation.PerformanceTimesResponse;
import com.amci.nissan360.api.reservation.ReservationOptions;
import com.amci.nissan360.api.reservation.StreetDriveQueueRequest;
import com.amci.nissan360.api.reservation.StreetDriveQueueResponse;
import com.amci.nissan360.database.VehiclesContract;
import com.amci.nissan360.provider.ReservationsQueueProvider;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.walnutlabs.android.ProgressHUD;

public class SelectReservationOnClickListener implements OnClickListener, OnCancelListener {

	public ProgressHUD mProgressHUD;

	RobospiceActivity activity;
	DateTime[] options;
	String vin;

	public SelectReservationOnClickListener(RobospiceActivity setActivity, String vin) {
		super();
		this.activity = setActivity;
		this.vin = vin;
	}


	@Override
	public void onClick(View v) {

		//Let user know it's done
		PerformanceTimesRequest request = new PerformanceTimesRequest();
		class PerformanceTimesRequestListener implements RequestListener< PerformanceTimesResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				mProgressHUD.dismiss();
				ReservationFailedMessageDialog reservationMessageDialog = new ReservationFailedMessageDialog(e.getMessage());
				reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");										
			}

			@Override
			public void onRequestSuccess(
					PerformanceTimesResponse arg0) {
				mProgressHUD.dismiss();
				options = new DateTime[arg0.size()];
				for(int i=0; i<arg0.size(); i++){
					String timeString = arg0.get(i);
					DateTime dateTime = new DateTime(timeString);
					options[i] = dateTime.toDateTime();
				}
				showStartTimeSelection();

			}


		}

		mProgressHUD = ProgressHUD.show(activity,"Getting available times...", true,true, null);
		activity.spiceManager.execute( request, null, DurationInMillis.NEVER, new PerformanceTimesRequestListener() );

	}

	@Override
	public void onCancel() {
		mProgressHUD.dismiss();

	}

	int selectedIndex;
	private static final int NOTHING_SELECTED = 9999;


	private void showStartTimeSelection(){

		final Dialog dialog = new Dialog(activity);
		dialog.setContentView(R.layout.view_choose_track_time_dialog);
		dialog.setTitle("Select Reservation Time Below");

		ListView coursesListView = (ListView) dialog.findViewById(R.id.track_choose_time_list);
		String[] textOptions;
		textOptions = new String[options.length];
		DateTimeFormatter fmt = new DateTimeFormatterBuilder()
		.appendMonthOfYearShortText()
		.appendLiteral(" ")
		.appendDayOfMonth(2)
		.appendLiteral(" ")
		.appendHourOfHalfday(2)
		.appendLiteral(":")
		.appendMinuteOfHour(2)
		.toFormatter();

		final CourseSelectionArrayAdapter<String> coursesAdapter = new CourseSelectionArrayAdapter<String>(activity, android.R.layout.simple_list_item_1);

		int i=0;
		for(DateTime dateTime : options){
			coursesAdapter.add(dateTime.toString(fmt));
			i++;
		}

		coursesListView.setAdapter(coursesAdapter);
		coursesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
				view.setSelected(true);
				selectedIndex = position;
			}

		});

		final DateTime[] optionStartTimes = options;


		// Add the buttons
		Button yesButton = (Button) dialog.findViewById(R.id.reserve_yes_button);
		yesButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if(selectedIndex == NOTHING_SELECTED){
					Toast.makeText(activity, "You must select a time", Toast.LENGTH_LONG).show();
					return;
				}

				dialog.dismiss();

				String testingPerformanceDriveVin = vin;
				PerformanceDriveReservationOptions options;
				try {
					options = new PerformanceDriveReservationOptions(
							Nissan360.getLoggedInUser().LoginId, testingPerformanceDriveVin, optionStartTimes[selectedIndex]);
				} catch (Exception e) {
					// TODO: User isn't logged in, pop to login page
					e.printStackTrace();
					return;
				} 

				//Let user know it's done
				PerformanceDriveReservationRequest request = new PerformanceDriveReservationRequest(options);
				class PerformanceDriveReservationRequestListener implements RequestListener< PerformanceDriveReservationResponse > {

					@Override
					public void onRequestFailure(SpiceException exception) {
						mProgressHUD.dismiss();

						String dialogMessage = exception.getMessage();

						if(exception instanceof NoNetworkException){
							dialogMessage = "Network connection unavailable, please try again";

						} else if (exception.getCause() instanceof HttpClientErrorException) {
							HttpClientErrorException clientException = (HttpClientErrorException)exception.getCause();
							if (clientException.getStatusCode().equals(HttpStatus.BAD_REQUEST) ) 
							{
								String body = clientException.getResponseBodyAsString();
								try {
									ApiError error = Nissan360.objectMapper.readValue(body, ApiError.class);

									dialogMessage = error.getErrorText();
									if(dialogMessage.equals(ApiError.ERROR_DUPLICATE_RESERVATION)){
										dialogMessage = ApiError.ERROR_DUPLICATE_RESERVATION_HIGH_PERFORMANCE;
									}

								} catch (JsonParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (JsonMappingException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}

						ReservationFailedMessageDialog reservationMessageDialog = new ReservationFailedMessageDialog(dialogMessage);
						reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");										
					}

					@Override
					public void onRequestSuccess(
							PerformanceDriveReservationResponse arg0) {
						mProgressHUD.dismiss();
						ReservationAddedMessageDialog reservationMessageDialog = new ReservationAddedMessageDialog();
						reservationMessageDialog.show(activity.getSupportFragmentManager(), "reservation_added");	

					}

				}

				mProgressHUD = ProgressHUD.show(activity,"Reserving Car...", true,true,null);
				activity.spiceManager.execute( request, null, DurationInMillis.NEVER, new PerformanceDriveReservationRequestListener() );

			}
		});


		Button noButton = (Button) dialog.findViewById(R.id.reserve_no_button);
		noButton.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();	

	}

}





class ReservationAddedMessageDialog  extends DialogFragment {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(R.string.reservation_added).setTitle("￼Reservation Added");

		// Add the buttons
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				ReservationAddedMessageDialog.this.dismiss();

			}
		});



		AlertDialog dialog = builder.create();
		return dialog;
	}
}

class ReservationFailedMessageDialog  extends DialogFragment {

	String message;

	public ReservationFailedMessageDialog(String message2) {
		super();
		message = message2;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(message).setTitle("Reservation Failed.  Please try again!");

		// Add the buttons
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				ReservationFailedMessageDialog.this.dismiss();

			}
		});



		AlertDialog dialog = builder.create();
		return dialog;
	}
}

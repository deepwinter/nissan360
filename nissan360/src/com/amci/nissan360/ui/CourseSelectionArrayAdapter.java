package com.amci.nissan360.ui;

import com.amci.nissan360.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class CourseSelectionArrayAdapter<T> extends ArrayAdapter<T> {

	public CourseSelectionArrayAdapter(Context context, int resource) {
		super(context, resource);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view =  super.getView(position, convertView, parent);
		
		/*
		if(position % 2 == 0){
			view.setBackgroundResource(R.color.list_item_dark);
		} else {
			view.setBackgroundResource(R.color.list_item_light);
		}
		*/
		return view;
	}
	
	

}

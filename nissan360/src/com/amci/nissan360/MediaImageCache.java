package com.amci.nissan360;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Debug;

public class MediaImageCache extends ImageCache {

	private static final long MAX_IMAGE_FILE_SIZE = 800; // kB

	public Bitmap getBitmap(String imagePath){

		Bitmap bitmap = getBitmapFromMemCache(imagePath);
		if(bitmap != null){
			return bitmap;
		} else {
			File file= new File( imagePath);
			if (file.exists() && file.length() / 1024 <= MAX_IMAGE_FILE_SIZE) {

				try {
					// Check for memory problem here	
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(imagePath, options);
					int imageHeight = options.outHeight;
					int imageWidth = options.outWidth;
					String imageType = options.outMimeType;

					// Let's make sure garbage has been collected
					// System.gc(); // I think doing this every time really slows down the system.

					// Decode file
					bitmap = BitmapFactory.decodeFile(imagePath);
					if(bitmap == null){
						return null;
					}
					addBitmapToMemoryCache(imagePath, bitmap);
					return bitmap;
				} catch  (OutOfMemoryError ome){
					return null;
				}
			} else {
				return null;
			}
		}
	}

	public static boolean checkBitmapFitsInMemory(long bmpwidth,long bmpheight, int bmpdensity ){
		long reqsize=bmpwidth*bmpheight*bmpdensity;
		long allocNativeHeap = Debug.getNativeHeapAllocatedSize();

		final long heapPad=(long) Math.max(4*1024*1024,Runtime.getRuntime().maxMemory()*0.1);
		if ((reqsize + allocNativeHeap + heapPad) >= Runtime.getRuntime().maxMemory())
		{
			return false;
		}
		return true;

	}


}

